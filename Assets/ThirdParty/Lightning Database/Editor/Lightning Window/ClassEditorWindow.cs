﻿using UnityEngine;
using UnityEngine.Events;
using UnityEditor;
using UnityEditorInternal;

using System;
using System.Collections.Generic;
using System.Linq;


namespace RutCreate.LightningDatabase
{
	public class AddClassEditorWindow : BaseClassEditorWindow
	{
		protected override string Title { get { return "Add Class"; } }

		protected override string SubmitButtonTitle { get { return "Create New Class"; } }

		protected override bool OnCanRemoveColumnCallback(ReorderableList list)
		{
			EditorField field = m_Form.Columns[list.index];
			return (field.Name != "ID" && field.Name != "Name") || field.IsNew;
		}
	}

	public class EditClassEditorWindow : BaseClassEditorWindow
	{
		protected override string Title { get { return "Edit Class"; } }

		protected override string SubmitButtonTitle { get { return "Apply Changes"; } }

		protected override bool OnCanRemoveColumnCallback(ReorderableList list)
		{
			EditorField field = m_Form.Columns[list.index];
			return field.Name != "ID" && field.Name != "Name";
		}

		protected override void ResetForm()
		{
			base.ResetForm();

			m_Form.Columns.Clear();
			if (LightningWindow.Instance != null && LightningWindow.Instance.CurrentClass != null)
			{
				foreach (Field field in LightningWindow.Instance.CurrentClass.Fields)
				{
					m_Form.Columns.Add(new EditorField(field.Name, field.Type, field.IsList, field.IsClassField, false));
				}
				m_Form.Name = LightningWindow.Instance.CurrentClass.Name;
			}
		}

		protected override void DrawFieldName()
		{
			bool guiEnabled = GUI.enabled;
			GUI.enabled = false;
			base.DrawFieldName();
			GUI.enabled = guiEnabled;
		}

		protected override bool ValidateFieldName(FormData data)
		{
			return true;
		}
	}

	public abstract class BaseClassEditorWindow : EditorWindow
	{
		protected const float WindowWidth = 500f;
		protected const float WindowHeight = 400f;
		protected const float ToggleColumnWidth = 40f;
		protected const float ColumnSpacing = 4f;

		public static T Open<T>(UnityAction<FormData> onSubmitListener) where T : BaseClassEditorWindow
		{
			T window = GetWindowWithRect<T>(new Rect(0f, 0f, WindowWidth, WindowHeight), true, "Lightning", true);
			window.onSubmit.RemoveAllListeners();
			window.onSubmit.AddListener(onSubmitListener);
			window.ResetForm();
			return window;
		}

		public OnSubmit onSubmit = new OnSubmit();

		protected FormData m_Form = new FormData();

		private Vector2 m_ScrollPosition;

		protected ReorderableList m_ColumnList;

		protected bool m_Changed = false;

		protected string m_NameErrorMessage = string.Empty;
		protected string m_FieldErrorMessage = string.Empty;

		private List<GUIContent> m_FieldTypeDisplayOptions;
		private List<Type> m_FieldTypeValues;

		protected virtual void OnGUI()
		{
			if (m_Form.Columns.Count == 0)
			{
				Close();
			}
			else
			{
				DrawTitle();
				DrawBody();
			}
		}

		abstract protected string Title { get; }

		abstract protected string SubmitButtonTitle { get; }

		protected virtual void DrawTitle()
		{
			EditorGUILayout.BeginHorizontal(RCGUIStyles.ProjectTopBarBg);
			EditorGUILayout.LabelField(Title, RCGUIStyles.ClassesTopic);
			EditorGUILayout.EndHorizontal();
		}

		protected virtual void DrawBody()
		{
			DrawForm();
		}

		protected virtual void DrawFieldName()
		{
			if (!string.IsNullOrEmpty(m_NameErrorMessage))
			{
				EditorGUILayout.HelpBox(m_NameErrorMessage, MessageType.Error);
			}

			m_Form.Name = EditorGUILayout.TextField("Name", m_Form.Name);
		}

		protected virtual void DrawFieldColumns()
		{
			if (m_ColumnList == null)
			{
				m_ColumnList = new ReorderableList(m_Form.Columns, typeof(EditorField), true, false, true, true);
				m_ColumnList.drawHeaderCallback = DrawFieldHeaderCallback;
				m_ColumnList.onAddCallback = OnAddColumnCallback;
				m_ColumnList.onRemoveCallback = OnRemoveColumnCallback;
				m_ColumnList.onCanRemoveCallback = OnCanRemoveColumnCallback;
				m_ColumnList.onReorderCallback = OnReorderColumnCallback;
				m_ColumnList.drawElementCallback = DrawColumnElementCallback;
			}

			if (!string.IsNullOrEmpty(m_FieldErrorMessage))
			{
				EditorGUILayout.HelpBox(m_FieldErrorMessage, MessageType.Error);
			}

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Columns");
			EditorGUILayout.BeginVertical();
			m_ColumnList.DoLayoutList();
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndHorizontal();
		}

		protected virtual void DrawForm()
		{
			EditorGUI.BeginChangeCheck();

			float labelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = 80f;

			m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition);
			DrawFieldName();
			DrawFieldColumns();
			EditorGUILayout.EndScrollView();

			if (EditorGUI.EndChangeCheck())
			{
				SetFormDirty();
			}

			GUI.enabled = m_Changed;
			if (GUILayout.Button(SubmitButtonTitle, "LargeButton"))
			{
				if (Validate(m_Form))
				{
					onSubmit.Invoke(m_Form);
					m_Changed = false;
					Close();
				}

				m_ScrollPosition.y = 0f;
			}
			GUI.enabled = true;
			EditorGUIUtility.labelWidth = labelWidth;
		}

		protected virtual void DrawFieldHeaderCallback(Rect rect)
		{
			Rect position = rect;
			position.x += 14f;
			position.width = (rect.width - ColumnSpacing * 2 - ToggleColumnWidth - 14f) / 2f;
			position.height = EditorGUIUtility.singleLineHeight;
			EditorGUI.LabelField(position, "Name", EditorStyles.boldLabel);

			position.x += position.width + ColumnSpacing;
			EditorGUI.LabelField(position, "Type", EditorStyles.boldLabel);

			position.x += position.width + ColumnSpacing;
			position.width = ToggleColumnWidth;
			EditorGUI.LabelField(position, "List", EditorStyles.boldLabel);
		}

		protected virtual void OnAddColumnCallback(ReorderableList list)
		{
			m_Form.Columns.Add(new EditorField("", typeof(StringField)));
			SetFormDirty();
		}

		protected virtual void OnRemoveColumnCallback(ReorderableList list)
		{
			m_Form.Columns.RemoveAt(list.index);
			SetFormDirty();
		}

		abstract protected bool OnCanRemoveColumnCallback(ReorderableList list);

		protected virtual void OnReorderColumnCallback(ReorderableList list)
		{
			SetFormDirty();
		}

		protected virtual void DrawColumnElementCallback(Rect rect, int index, bool isActive, bool isFocused)
		{
			EditorField column = m_Form.Columns[index];

			Rect position = rect;
			position.y += 2f;
			position.width = (rect.width - ColumnSpacing * 2 - ToggleColumnWidth) / 2f;
			position.height = EditorGUIUtility.singleLineHeight;

			if (column.IsNew)
			{
				column.Name = EditorGUI.TextField(position, column.Name);
			}
			else
			{
				EditorGUI.LabelField(position, column.Name);
			}

			position.x += position.width + ColumnSpacing;
			if (column.IsNew)
			{
				int selectedIndex = m_FieldTypeValues.IndexOf(column.Type);
				int newSelectedIndex = EditorGUI.Popup(position, selectedIndex, m_FieldTypeDisplayOptions.ToArray());
				if (newSelectedIndex != selectedIndex)
				{
					column.Type = m_FieldTypeValues[newSelectedIndex];
				}
			}
			else
			{
				EditorGUI.LabelField(position, EditorUtil.GetFieldTypeName(column.Type));
			}

			position.x += position.width + ColumnSpacing + (ToggleColumnWidth - 16f) / 2f;
			position.width = 16f;
			GUI.enabled = column.IsNew;
			column.IsList = EditorGUI.Toggle(position, column.IsList);
			GUI.enabled = true;

			if (GUI.changed)
			{
				if (column.Type.IsSubclassOf(typeof(FieldType)))
				{
					FieldType fieldType = (FieldType)System.Activator.CreateInstance(column.Type);
					column.IsClassField = fieldType.IsClassField;
				}
			}
		}

		protected virtual void ResetForm()
		{
			m_Form.Columns.Clear();
			m_Form.Columns.Add(new EditorField("ID", typeof(IntField), false, false, false));
			m_Form.Columns.Add(new EditorField("Name", typeof(StringField), false, false, false));

			m_FieldTypeDisplayOptions = new List<GUIContent>();
			m_FieldTypeValues = new List<Type>();
			List<Type> menuTypes = Util.FindDerivedTypes(typeof(FieldType)).ToList();
			foreach (Type type in menuTypes)
			{
				// Ignore self reference.
				FieldType instance = (FieldType)System.Activator.CreateInstance(type);
				if (instance != null && LightningWindow.Instance.CurrentClass != null &&
				    instance.IsClassField && instance.ClassFieldName == LightningWindow.Instance.CurrentClass.Name) continue;
				
				object[] attributes = type.GetCustomAttributes(false);
				foreach (var attribute in attributes)
				{
					FieldInfoAttribute typeAttr = attribute as FieldInfoAttribute;
					if (typeAttr != null)
					{
						GUIContent menuTitle = new GUIContent(string.Format("{0}/{1}", typeAttr.Category, typeAttr.Name));
						m_FieldTypeDisplayOptions.Add(menuTitle);
						m_FieldTypeValues.Add(type);
					}
				}
			}
		}

		protected virtual bool ValidateFieldName(FormData data)
		{
			if (string.IsNullOrEmpty(m_Form.Name))
			{
				m_NameErrorMessage = "Class name cannot be empty.";
				return false;
			}
			else if (!EditorUtil.IsValidClassName(m_Form.Name))
			{
				m_NameErrorMessage = "Class name is invalid format.\n* Must be starts with alphabet letter.\n* Allows only a-z, A-z, 0-9 and underscore.";
				return false;
			}
			else
			{
				// Class name.
				string baseModelPath = string.Format("{0}.Base{1}", LightningWindow.DatabaseNamespace, data.Name);
				string modelPath = string.Format("{0}.{1}", LightningWindow.DatabaseNamespace, data.Name);
				string baseDatabasePath = string.Format("{0}.Base{1}Database", LightningWindow.DatabaseNamespace, data.Name);
				string databasePath = string.Format("{0}.{1}Database", LightningWindow.DatabaseNamespace, data.Name);

				if (!IsClassNameAvailable(baseModelPath) ||
				    !IsClassNameAvailable(modelPath) ||
				    !IsClassNameAvailable(baseDatabasePath) ||
				    !IsClassNameAvailable(databasePath))
				{
					m_NameErrorMessage = string.Format("Class name\"{0}\" is already exists . Please use another class name.", m_Form.Name);
					return false;
				}
			}

			return true;
		}

		protected virtual bool ValidateFieldFields(FormData data)
		{
			bool valid = true;

			List<string> columnNames = new List<string>();
			List<string> existingColumns = new List<string>();
			List<string> fieldErrorMessages = new List<string>();
			foreach (EditorField field in data.Columns)
			{
				if (columnNames.IndexOf(field.Name) >= 0)
				{
					if (existingColumns.IndexOf(field.Name) == -1)
					{
						existingColumns.Add(field.Name);
						fieldErrorMessages.Add(string.Format("  - Field \"{0}\" is already exists.", field.Name));
						valid = false;
					}
				}
				else
				{
					columnNames.Add(field.Name);
					if (!EditorUtil.IsValidClassName(field.Name))
					{
						fieldErrorMessages.Add(string.Format("  - Field \"{0}\" is invalid format.", field.Name));
						valid = false;
					}
				}
			}

			if (fieldErrorMessages.Count > 0)
			{
				string fieldErrorMessage = string.Join("\n", fieldErrorMessages.ToArray());
				m_FieldErrorMessage = string.Format("Fields error(s)\n{0}\n\n* Must be starts with alphabet letter.\n* Allows only a-z, A-z, 0-9 and underscore.", fieldErrorMessage);
			}

			return valid;
		}

		protected virtual bool Validate(FormData data)
		{
			m_NameErrorMessage = string.Empty;
			m_FieldErrorMessage = string.Empty;

			bool valid = ValidateFieldName(data) && ValidateFieldFields(data);
			return valid;
		}

		protected bool IsClassNameAvailable(string fullName)
		{
			Type type = Util.GetTypeFromReferencedAssemblies(fullName);
			bool valid = (type == null);
			return valid;
		}

		protected virtual void SetFormDirty()
		{
			m_Changed = true;
		}

		public class OnSubmit : UnityEvent<FormData>
		{

		}

		public class FormData
		{
			public string Name;

			public List<EditorField> Columns;

			public FormData()
			{
				Columns = new List<EditorField>();
			}
		}
	}
}
