﻿using UnityEngine;
using UnityEditor;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public partial class LightningWindow : EditorWindow
	{

		private const float InitialSidebarWidth = 200f;
		private const string ConfigurationDirectoryName = "Configuration";
		private const string ConfigurationFileName = "Configuration.asset";
		public const string DatabaseNamespace = "RutCreate.LightningDatabase";

		private const float CompilingMessageWidth = 250f;
		private const float CompilingMessageHeight = 40f;

		private Vector2 m_ClassesScrollPosition;

		private Vector2 m_BrowserScrollPosition = Vector2.zero;

		private Configuration m_Configuration;

		private int m_SelectedClassIndex = -1;

		private bool m_IsCompiling = false;

		private DataManager m_DataManager;

		private bool m_Editable = true;

		private static LightningWindow m_Instance = null;

		public static LightningWindow Instance
		{
			get
			{
				return m_Instance;
			}
		}

		public Project CurrentProject
		{
			get
			{
				if (m_Configuration != null)
				{
					if (m_Configuration.Projects.Count == 0)
					{
						m_Configuration.Projects.Add(new Project("Lightning"));
					}
					return m_Configuration.Projects[0];
				}
				return null;
			}
		}

		public Class CurrentClass
		{
			get
			{
				if (CurrentProject != null)
				{
					if (m_SelectedClassIndex >= 0 && m_SelectedClassIndex < CurrentProject.Classes.Count)
					{
						return CurrentProject.Classes[m_SelectedClassIndex];
					}
				}
				return null;
			}
		}

		private void LoadConfiguration()
		{
			if (m_Configuration == null)
			{
				string filepath = GetConfigurationDirectory() + ConfigurationFileName;
				m_Configuration = AssetDatabase.LoadAssetAtPath<Configuration>(filepath);
				if (m_Configuration == null)
				{
					m_Configuration = ScriptableObject.CreateInstance<Configuration>();
					AssetDatabase.CreateAsset(m_Configuration, filepath);
				}
			}
		}

		private string GetConfigurationDirectory()
		{
			// TODO It mays has other way to do this (better than stack trace).
			string projectPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar;
			string filepath = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
			string directory = Path.GetDirectoryName(filepath.Substring(projectPath.Length));
			directory = Path.GetDirectoryName(directory);
			directory = Path.Combine(directory, ConfigurationDirectoryName) + Path.DirectorySeparatorChar;
			if (!Directory.Exists(directory))
			{
				Directory.CreateDirectory(directory);
			}
			return directory;
		}

		public void AddNewClass(Class clazz)
		{
			Class newClass = new Class(clazz);
			CurrentProject.Classes.Add(newClass);
			ClassBuilder.Build(newClass);

			SetConfigurationDirty();
		}

		private void OnEnable()
		{
			m_Instance = this;
			Undo.undoRedoPerformed += OnUndoRedoPerformed;
		}

		private void OnDisable()
		{
			Undo.undoRedoPerformed -= OnUndoRedoPerformed;
		}

		private void OnGUI()
		{
			LoadConfiguration();
			if (m_Configuration == null) return;

			if (EditorApplication.isCompiling)
			{
				m_IsCompiling = true;
			}

			m_Editable = !EditorApplication.isCompiling;
			GUI.enabled = m_Editable;

			if (m_DataManager == null && m_SelectedClassIndex >= 0)
			{
				SetSelectedClassIndex(m_SelectedClassIndex);
			}

			DrawMainUI();

			if (m_IsCompiling)
			{
				Rect messagePosition = new Rect();
				messagePosition.x = position.width * 0.5f - CompilingMessageWidth * 0.5f;
				messagePosition.y = position.height * 0.5f - CompilingMessageHeight * 0.5f;
				messagePosition.width = CompilingMessageWidth;
				messagePosition.height = CompilingMessageHeight;

				GUI.enabled = true;
				EditorGUI.HelpBox(messagePosition, "Unity scripts are compiling. Please wait for a moment.", MessageType.Info);
				GUI.enabled = m_Editable;
			}
		}

		private void Update()
		{
			if (m_Configuration != null && !EditorApplication.isCompiling && m_IsCompiling)
			{
				m_IsCompiling = false;

				if (m_SelectedClassIndex >= 0 && m_SelectedClassIndex < CurrentProject.Classes.Count)
				{
					SetSelectedClassIndex(m_SelectedClassIndex);
				}

				Repaint();
			}

			if (m_DataManager != null)
			{
				m_DataManager.Update();
			}
		}

		private void DrawMainUI()
		{
			EditorGUILayout.BeginHorizontal();
			DrawSidebar();
			DrawContent();
			EditorGUILayout.EndHorizontal();
		}

		private void DrawSidebar()
		{
			EditorGUILayout.BeginVertical(GUILayout.Width(InitialSidebarWidth));

			EditorGUILayout.BeginHorizontal(RCGUIStyles.ProjectTopBarBg);
			EditorGUILayout.LabelField("Classes", RCGUIStyles.ClassesTopic);
			EditorGUILayout.EndHorizontal();

			m_ClassesScrollPosition = EditorGUILayout.BeginScrollView(m_ClassesScrollPosition);
			for (int i = 0; i < CurrentProject.Classes.Count; i++)
			{
				Class klass = CurrentProject.Classes[i];
				bool isSelected = (m_SelectedClassIndex == i);
				bool result = GUILayout.Toggle(isSelected, klass.Name, RCGUIStyles.ClassItem);
				if (isSelected == false && result == true)
				{
					SetSelectedClassIndex(i);
				}
			}
			EditorGUILayout.EndScrollView();

			if (GUILayout.Button("Add new class", EditorStyles.miniButton, GUILayout.MinWidth(100f)))
			{
				AddClassEditorWindow.Open<AddClassEditorWindow>(OnAddClassFormSubmit);
			}

			GUILayout.Space(10f);

			EditorGUILayout.EndVertical();

			DrawSplitter();
		}

		private void OnAddClassFormSubmit(BaseClassEditorWindow.FormData data)
		{
			Class newClass = new Class();
			newClass.Name = data.Name;
			foreach (EditorField column in data.Columns)
			{
				newClass.Fields.Add(new Field(column.Name, column.Type, column.IsList));
			}
			AddNewClass(newClass);

			Focus();
			Repaint();
		}

		private void OnManageColumnFormSubmit(BaseClassEditorWindow.FormData data)
		{
			if (CurrentClass == null) return;

			Dictionary<string, float> m_ColumnWidths = new Dictionary<string, float>();
			foreach (Field field in CurrentClass.Fields)
			{
				m_ColumnWidths.Add(field.Name, field.ColumnWidth);
			}

			CurrentClass.Fields.Clear();
			foreach (EditorField column in data.Columns)
			{
				Field field = new Field(column.Name, column.Type, column.IsList, column.IsClassField);
				if (m_ColumnWidths.ContainsKey(field.Name))
				{
					field.ColumnWidth = m_ColumnWidths[field.Name];
				}
				CurrentClass.Fields.Add(field);
			}

			ClassBuilder.Build(CurrentClass);

			SetConfigurationDirty();

			Focus();
			Repaint();
		}

		private void DrawSplitter()
		{
			Rect rect = GUILayoutUtility.GetLastRect();
			rect.x += rect.width - 1f;
			rect.y = rect.yMin;
			rect.width = 1f;
			GUI.Box(rect, "", RCGUIStyles.Splitter);
		}

		private void DrawContent()
		{
			EditorGUILayout.BeginVertical(RCGUIStyles.ContentBackground, GUILayout.ExpandWidth(true));

			DrawBrowserStatusBar();
			DrawBrowserTable();

			EditorGUILayout.EndVertical();
		}

		private void DrawBrowserStatusBar()
		{
			EditorGUILayout.BeginHorizontal(RCGUIStyles.ProjectTopBarBg);
			if (CurrentClass != null)
			{
				EditorGUILayout.LabelField(CurrentClass.Name, RCGUIStyles.BrowserStatusBarTitle);
				if (m_DataManager != null)
				{
					EditorGUILayout.LabelField(string.Format("{0} objects", m_DataManager.Count), RCGUIStyles.BrowserStatusBarSubtitle);
				}
				GUILayout.FlexibleSpace();

				if (GUILayout.Button("Delete Class", RCGUIStyles.BrowserStatusBarButton))
				{
					// Is class used as field by another class.
					Field usingField = null;
					foreach (Class klass in CurrentProject.Classes)
					{
						if (klass.Name == CurrentClass.Name) continue;
						foreach (Field field in klass.Fields)
						{
							if (field.IsClassField && field.ClassFieldName == CurrentClass.Name)
							{
								usingField = field;
								break;
							}
						}
					}

					if (usingField != null)
					{
						EditorUtility.DisplayDialog(
							"Delete Class",
							string.Format("Sorry! \"{0}\" could not be deleted because \"{1}\" is using it. You may need to delete field \"{2}\" from class \"{3}\" first.",
								CurrentClass.Name, usingField.ClassFieldName, usingField.Name, usingField.ClassFieldName),
							"I got it");
					}
					else
					{
						if (EditorUtility.DisplayDialog(
							    "Delete Class", 
							    string.Format("All scripts and database will be deleted. Are you sure you want to delete class \"{0}\"?", CurrentClass.Name),
							    "Yes, delete", "No, keep it"))
						{

							// Clear class editor variables.
							EditorPrefs.DeleteKey(CurrentClass.Name + "_IPP");

							// Delete scripts.
							ClassBuilder.Delete(CurrentClass);

							// Delete database asset.
							string assetPath = AssetDatabase.GetAssetPath(m_DataManager.Database);
							if (!string.IsNullOrEmpty(assetPath))
							{
								AssetDatabase.DeleteAsset(assetPath);
							}

							// Remove from configuration.
							CurrentProject.Classes.RemoveAt(m_SelectedClassIndex);

							SetConfigurationDirty();

							m_SelectedClassIndex = -1;
							m_DataManager = null;
						}
					}
				}
				if (GUILayout.Button("Edit Class", RCGUIStyles.BrowserStatusBarButton))
				{
					EditClassEditorWindow.Open<EditClassEditorWindow>(OnManageColumnFormSubmit);
				}
			}
			EditorGUILayout.EndHorizontal();
		}

		private void SetSelectedClassIndex(int index)
		{
			m_SelectedClassIndex = index;
			try
			{
				string classPath = string.Format("{0}.{1}Database", DatabaseNamespace, CurrentClass.Name);
				Type classType = Util.GetTypeFromReferencedAssemblies(classPath);
				if (classType != null)
				{
					int itemsPerPage = EditorPrefs.GetInt(CurrentClass.Name + "_IPP", 10);
					m_DataManager = new DataManager(classType, CurrentClass, itemsPerPage);
				}

				ClearSelection();
			}
			catch (NullReferenceException ex)
			{
				m_SelectedClassIndex = -1;
				Debug.LogWarning(ex.Message);
			}
		}

		private void OnUndoRedoPerformed()
		{
			Repaint();
		}

		public void SetConfigurationDirty(bool saveImmediate = true)
		{
			EditorUtility.SetDirty(m_Configuration);
			if (saveImmediate)
			{
				AssetDatabase.SaveAssets();
			}
		}

		[MenuItem("Window/Lightning Database", false, -99)]
		public static void Open()
		{
			GetWindow<LightningWindow>(false, "Lightning", true);
		}
	}
}
