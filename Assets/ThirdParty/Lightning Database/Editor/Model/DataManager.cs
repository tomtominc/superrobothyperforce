﻿using UnityEngine;
using UnityEditor;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public class DataManager
	{
		private MethodInfo m_AddMethodInfo;

		private MethodInfo m_RemoveMethodInfo;

		private FieldInfo m_ItemsFieldInfo;

		private object m_Field;

		private ScriptableObject m_Database;

		public ScriptableObject Database
		{
			get { return m_Database; }
		}

		private Class m_Class;

		public Class Klass
		{
			get { return m_Class; }
		}

		private Type m_DatabaseType;

		public Type DatabaseType
		{
			get { return m_DatabaseType; }
		}

		private Type m_ModelType;

		public Type ModelType
		{
			get { return m_ModelType; }
		}

		private IList m_List;

		public IList List
		{
			get { return m_List; }
		}

		private int m_TotalPage;

		public int TotalPage
		{
			get { return m_TotalPage; }
		}

		private int m_ItemsPerPage;

		public int ItemsPerPage
		{
			get { return m_ItemsPerPage; }
			set
			{
				m_ItemsPerPage = Mathf.Clamp(value, 1, int.MaxValue);
				CalculateTotalPage();
				Page = m_Page;
			}
		}

		private int m_Page;

		public int Page
		{
			get { return m_Page; }
			set
			{
				m_Page = Mathf.Clamp(value, 1, m_TotalPage);
				CalculateItemIndex();
			}
		}

		private int m_NumRows;

		public int NumRows
		{
			get { return m_NumRows; }
		}

		private int m_StartItemIndex;

		public int StartItemIndex
		{
			get { return m_StartItemIndex; }
		}

		private int m_EndItemIndex;

		public int EndItemIndex
		{
			get { return m_EndItemIndex; }
		}

		private bool m_Dirty = false;

		public bool IsDirty
		{
			get { return m_Dirty; }
		}

		private float m_NextSaveTime = -1f;

		public DataManager(Type databaseType, Class klass, int itemsPerPage = 10, int page = 1)
		{
			m_DatabaseType = databaseType;
			m_Class = klass;
			m_ItemsPerPage = itemsPerPage;
			m_Page = page;

			LoadDatabase();
			LoadFieldsData();
			PrepareMethods();
			CalculateTotalPage();
			CalculateItemIndex();
		}

		private void LoadDatabase()
		{
			string assetPath = Path.Combine(ClassBuilder.OutputDatabasePath, string.Format("{0}Database.asset", Klass.Name));
			m_Database = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);
			if (m_Database == null)
			{
				m_Database = ScriptableObject.CreateInstance(DatabaseType);
				AssetDatabase.CreateAsset(m_Database, assetPath);
				AssetDatabase.Refresh();
			}

			LoadReferencedDatabases();
		}

		private void LoadReferencedDatabases()
		{
			FieldInfo[] fieldInfos = m_Database.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);
			foreach (FieldInfo fieldInfo in fieldInfos)
			{
				Type baseType = fieldInfo.FieldType.BaseType;
				if (baseType.IsGenericType)
				{
					Type[] genericArguments = baseType.GetGenericArguments();
					if (genericArguments.Length == 1 && genericArguments[0].IsSubclassOf(typeof(BaseClass)))
					{
						object value = fieldInfo.GetValue(m_Database);
						if (value == null)
						{
							string databasePath = ClassBuilder.OutputDatabasePath + "/" + genericArguments[0].Name + "Database.asset";
							UnityEngine.Object database = AssetDatabase.LoadAssetAtPath(databasePath, fieldInfo.FieldType);
							if (database != null)
							{
								fieldInfo.SetValue(m_Database, database);
							}
							else
							{
								Debug.LogWarningFormat("Auto assign reference class failed. Could not find database at {0}", databasePath);
							}
						}
					}
				}
			}
		}

		private void LoadFieldsData()
		{
			List<string> fieldNames = new List<string>();
			foreach (Field field in m_Class.Fields)
			{
				FieldType fieldType = (FieldType)System.Activator.CreateInstance(field.Type);
				if (!fieldNames.Contains(fieldType.ClassFieldName))
				{
					fieldType.ReloadData();
					fieldNames.Add(fieldType.ClassFieldName);
				}
			}
		}

		private void PrepareMethods()
		{
			m_ItemsFieldInfo = m_Database.GetType().GetField("m_Items", BindingFlags.NonPublic | BindingFlags.Instance);
			m_Field = m_ItemsFieldInfo.GetValue(m_Database);
			m_ModelType = m_Field.GetType().GetGenericArguments()[0];
			m_AddMethodInfo = m_DatabaseType.GetMethod("Add");
			m_RemoveMethodInfo = m_DatabaseType.GetMethod("RemoveAt");
			m_List = m_Field as IList;
		}

		public void AddItem()
		{
			object item = System.Activator.CreateInstance(m_ModelType);
			m_AddMethodInfo.Invoke(m_Database, new object[] { item });
			CalculateTotalPage();
		}

		public void Remove(int id)
		{
			m_RemoveMethodInfo.Invoke(m_Database, new object[] { id });
		}

		public void NextPage()
		{
			Page++;
		}

		public void PreviousPage()
		{
			Page--;
		}

		public void FirstPage()
		{
			Page = 1;
		}

		public void LastPage()
		{
			Page = TotalPage;
		}

		public int Count
		{
			get { return m_List.Count; }
		}

		public void SetDirty(bool saveImmediate = false)
		{
			if (m_Database != null)
			{
				EditorUtility.SetDirty(m_Database);

				if (saveImmediate)
				{
					AssetDatabase.SaveAssets();
				}
				else
				{	
					m_Dirty = true;
					m_NextSaveTime = Time.realtimeSinceStartup + Preferences.AutoSaveInterval;
				}
			}
		}

		public void StartRecordUndo()
		{
			if (m_Database != null) Undo.RecordObject(m_Database, "Database " + m_Database.name);
		}

		public void StopRecordUndo()
		{
			if (GUI.changed) SetDirty();
		}

		private void CalculateTotalPage()
		{
			m_TotalPage = Mathf.CeilToInt(Count / (float)m_ItemsPerPage);
		}

		private void CalculateItemIndex()
		{
			m_StartItemIndex = Mathf.Clamp((m_Page - 1) * m_ItemsPerPage, 0, int.MaxValue);
			m_EndItemIndex = Mathf.Clamp(m_StartItemIndex + m_ItemsPerPage - 1, 0, Count - 1);
			m_NumRows = m_EndItemIndex - m_StartItemIndex + 1;
		}

		public void Update()
		{
			if (m_Dirty && Time.realtimeSinceStartup >= m_NextSaveTime)
			{
				AssetDatabase.SaveAssets();
				m_Dirty = false;
			}
		}
	}
}
