﻿using UnityEngine;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Class : DotLiquid.Drop
	{
		[SerializeField]
		protected string m_Name;

		public string Name
		{
			get { return m_Name; }
			set { m_Name = value; }
		}

		[SerializeField]
		protected List<Field> m_Fields;

		public List<Field> Fields
		{
			get { return m_Fields; }
			set { m_Fields = value; }
		}

		public Class()
		{
			m_Fields = new List<Field>();
		}

		public Class(string name)
		{
			m_Name = name;
			m_Fields = new List<Field>();
		}

		public Class(Class source)
		{
			m_Name = source.Name;
			m_Fields = new List<Field>();
			foreach (Field field in source.Fields)
			{
				m_Fields.Add(new Field(field));
			}
		}
	}
}
