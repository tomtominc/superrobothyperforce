﻿using UnityEngine;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class Project
	{
		public string Name;

		public List<Class> Classes;

		public Project()
		{
			Classes = new List<Class>();
		}

		public Project(string name)
		{
			Name = name;
			Classes = new List<Class>();
		}

		public Project(Project source)
		{
			Name = source.Name;
			Classes = new List<Class>();
			foreach (Class clazz in source.Classes)
			{
				Classes.Add(new Class(clazz));
			}
		}
	}
}
