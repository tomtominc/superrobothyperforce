﻿using System;


namespace RutCreate.LightningDatabase
{
	public class EditorField : Field
	{
		public bool IsNew;

		public EditorField(string name, Type type, bool isList = false, bool isClassField = false, bool isNew = true) : base(name, type, isList, isClassField)
		{
			IsNew = isNew;
		}

		public EditorField(EditorField source) : base(source)
		{
			IsNew = source.IsNew;
		}
	}
}
