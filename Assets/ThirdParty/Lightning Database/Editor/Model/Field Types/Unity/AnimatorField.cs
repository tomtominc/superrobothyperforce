﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Animator", "Unity", typeof(Animator), "Animator", "null")]
	public class AnimatorField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (Animator)item, typeof(Animator), false);
			return item;
		}
	}
}
