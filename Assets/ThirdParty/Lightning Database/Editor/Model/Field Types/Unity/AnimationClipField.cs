﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("AnimationClip", "Unity", typeof(AnimationClip), "AnimationClip", "null")]
	public class AnimationClipField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.ObjectField((item == null) ? null : (AnimationClip)item, typeof(AnimationClip), false);
			return item;
		}
	}
}
