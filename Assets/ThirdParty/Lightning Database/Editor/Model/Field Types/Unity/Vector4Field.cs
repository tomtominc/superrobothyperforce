﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Vector4", "Unity", typeof(Vector4), "Vector4", "Vector4.zero")]
	public class Vector4Field : FieldType
	{
		public override object DrawField(object item)
		{
			EditorUtil.SetLabelWidth(14f);
			EditorGUILayout.BeginHorizontal();
			Vector4 value = (item == null) ? Vector4.zero : (Vector4)item;
			value.x = EditorGUILayout.FloatField("X", value.x, GUILayout.MinWidth(0f));
			value.y = EditorGUILayout.FloatField("Y", value.y, GUILayout.MinWidth(0f));
			value.z = EditorGUILayout.FloatField("Z", value.z, GUILayout.MinWidth(0f));
			value.w = EditorGUILayout.FloatField("W", value.w, GUILayout.MinWidth(0f));
			EditorGUILayout.EndHorizontal();
			EditorUtil.ResetLabelWidth();
			return value;
		}

		public override object GetDefaultValue()
		{
			return Vector4.zero;
		}
	}
}
