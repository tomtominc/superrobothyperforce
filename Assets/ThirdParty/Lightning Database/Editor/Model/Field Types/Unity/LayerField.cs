﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Layer", "Unity", typeof(int), "int", "0")]
	public class LayerField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.LayerField((item == null) ? 0 : (int)item);
			return item;
		}

		public override object GetDefaultValue()
		{
			return 0;
		}
	}
}
