using UnityEngine;
using UnityEditor;

using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("ProjectileBlueprint", "Lightning", typeof(int), "int", "-1")]
	public class ProjectileBlueprintField : FieldType
	{
		private static string[] m_DisplayedOptions = null;
		private static int[] m_OptionValues = null;

		private static ProjectileBlueprintDatabase m_Database;

		protected static ProjectileBlueprintDatabase Database
		{
			get
			{
				if (m_Database == null)
				{
					string databasePath = "Assets/Lightning Database/Databases/Database/ProjectileBlueprintDatabase.asset";
					m_Database = AssetDatabase.LoadAssetAtPath<ProjectileBlueprintDatabase>(databasePath);
				}
				return m_Database;
			}
		}

		public override void ReloadData()
		{
			base.ReloadData();

			List<string> displayedOptions = new List<string>();
			List<int> optionValues = new List<int>();

			displayedOptions.Add("- None -");
			optionValues.Add(-1);

			if (Database != null)
			{
				List<ProjectileBlueprint> items = Database.FindAll();
				foreach (ProjectileBlueprint item in items)
				{
					displayedOptions.Add(string.Format("[{0}] {1}", item.ID, item.Name));
					optionValues.Add(item.ID);
				}
			}

			m_DisplayedOptions = displayedOptions.ToArray();
			m_OptionValues = optionValues.ToArray();
		}

		public override object DrawField(object item)
		{
			item = EditorGUILayout.IntPopup((int)item, m_DisplayedOptions, m_OptionValues);
			return item;
		}

		public override object GetDefaultValue()
		{
			return -1;
		}

		public override bool IsClassField
		{
			get { return true; }
		}

		public override string ClassFieldName
		{
			get { return "ProjectileBlueprint"; }
		}
	}
}
