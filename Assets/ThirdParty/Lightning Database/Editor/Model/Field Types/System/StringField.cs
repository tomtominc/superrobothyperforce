﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("String", "System", typeof(string), "string", "string.Empty")]
	public class StringField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.TextField((item == null) ? string.Empty : item.ToString(), GUILayout.MinWidth(0f));
			return item;
		}
	}
}
