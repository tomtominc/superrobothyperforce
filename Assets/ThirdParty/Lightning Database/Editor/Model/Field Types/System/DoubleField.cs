﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Double", "System", typeof(double), "double", "0.0")]
	public class DoubleField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.DoubleField((item == null) ? 0 : (double)item, GUILayout.MinWidth(0f));
			return item;
		}

		public override object GetDefaultValue()
		{
			return 0.0;
		}
	}
}
