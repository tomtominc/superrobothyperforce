﻿using UnityEngine;
using UnityEditor;


namespace RutCreate.LightningDatabase
{
	[FieldInfo("Long", "System", typeof(long), "long", "0")]
	public class LongField : FieldType
	{
		public override object DrawField(object item)
		{
			item = EditorGUILayout.LongField((item == null) ? 0 : (long)item, GUILayout.MinWidth(0f));
			return item;
		}

		public override object GetDefaultValue()
		{
			return 0;
		}
	}
}
