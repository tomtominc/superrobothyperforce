﻿using UnityEngine;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace RutCreate.LightningDatabase
{
	public static class Util
	{

		public static bool IsValidCSharpVariableName(string name)
		{
			CodeDomProvider provider = CodeDomProvider.CreateProvider("C#");
			bool valid = provider.IsValidIdentifier(name);
			return valid;
		}

		public static Type GetTypeFromReferencedAssemblies(string name)
		{
			Assembly executingAssembly = Assembly.GetExecutingAssembly();
			AssemblyName[] referencedAssemblies = executingAssembly.GetReferencedAssemblies();
			foreach (AssemblyName assemblyName in referencedAssemblies)
			{
				Assembly assembly = Assembly.Load(assemblyName);
				if (assembly != null)
				{
					Type type = assembly.GetType(name);
					if (type != null)
					{
						return type;
					}
				}
			}

			return null;
		}

		/// <summary>
		/// FindDerivedTypesFromAssembly allows a user to query all of types derived from a
		/// particular Type at runtime.
		/// Example usage:
		///     foreach (System.Type st in EditorUtility.FindDerivedTypesFromAssembly(System.Reflection.Assembly.GetAssembly(typeof(BaseTimelineEvent)), typeof(BaseTimelineEvent), true))
		/// </summary>
		/// <param name="assembly">The assembly to search in</param>
		/// <param name="baseType">The base Type from which all returned Types derive</param>
		/// <param name="classOnly">If true, only class Types will be returned</param>
		/// <returns></returns>
		public static Type[] FindDerivedTypesFromAssembly(this Assembly assembly, Type baseType, bool classOnly = true)
		{
			if (assembly == null)
			{
				Debug.LogError("Assembly must be defined");
			}
			if (baseType == null)
			{
				Debug.LogError("Base type must be defined");
			}

			// Iterate through all available types in the assembly
			var types = assembly.GetTypes().Where(type =>
			{
				if (classOnly && !type.IsClass)
					return false;

				if (baseType.IsInterface)
				{
					Type it = type.GetInterface(baseType.FullName);

					if (it != null)
						return true;
				}
				else if (type.IsSubclassOf(baseType))
				{
					return true;
				}

				return false;
			}
			            );

			return types.ToArray();
		}

		/// <summary>
		/// A convenient method for calling the above.
		/// Example usage:
		///     List<System.Type> subTypes = EditorUtility.FindDerivedTypes(typeof(BaseTimelineEvent)).ToList();
		/// </summary>
		/// <param name="baseType"></param>
		/// <param name="classOnly"></param>
		/// <returns></returns>
		public static Type[] FindDerivedTypes(Type baseType, bool classOnly = true)
		{
			return FindDerivedTypesFromAssembly(Assembly.GetAssembly(baseType), baseType, classOnly);
		}

		public static bool IsList(object o)
		{
			if (o == null) return false;
			return o is IList &&
			o.GetType().IsGenericType &&
			o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
		}

		public static bool IsDictionary(object o)
		{
			if (o == null) return false;
			return o is IDictionary &&
			o.GetType().IsGenericType &&
			o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
		}
	}
}
