﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;
using System.IO;
using System.Text;

using DotLiquid;


namespace RutCreate.LightningDatabase
{
	public class ClassBuilder
	{
		public const string TemplateDirectory = "Template";
		public const string TemplateDirectoryDefault = "Default";

		public const string TemplateFileModel = "Model.cs.template";
		public const string TemplateFileBaseModel = "BaseModel.cs.template";
		public const string TemplateFileDatabase = "Database.cs.template";
		public const string TemplateFileBaseDatabase = "BaseDatabase.cs.template";
		public const string TemplateFileClassField = "ClassField.cs.template";
		public const string TemplateFilePlayMakerAction = "PlayMakerAction.cs.template";

		public const string OutputDirectory = "Databases";
		public const string OutputDatabaseDirectory = "Database";
		public const string OutputScriptDirectory = "Scripts";
		public const string OutputPlayMakerDirectory = "PlayMaker/Actions";
		public const string LightningFieldScriptPath = "Editor/Model/Field Types/Lightning";

		private string m_ModelScriptContent;
		private string m_BaseModelScriptContent;
		private string m_DatabaseScriptContent;
		private string m_BaseDatabaseScriptContent;
		private string m_ClassFieldScriptContent;
		private Dictionary<string, string> m_PlayMakerScriptContents = new Dictionary<string, string>();

		private Class m_Class;

		public static void Build(Class klass)
		{
			ClassBuilder builder = new ClassBuilder(klass);
			builder.Build();
		}

		public static void Delete(Class klass)
		{
			ClassBuilder builder = new ClassBuilder(klass);
			builder.Delete();
		}

		public static void Rebuild(Class klass)
		{
			ClassBuilder builder = new ClassBuilder(klass);
			builder.Delete();
			builder.Build();
		}

		public ClassBuilder(Class klass)
		{
			m_Class = klass;
			PrepareTemplate();
		}

		public void Build()
		{
			PrepareDirectories();
			CreateScripts();

			AssetDatabase.Refresh();
		}

		public void Delete()
		{
			string baseModelPath = Path.Combine(OutputScriptPath, string.Format("Base{0}.cs", m_Class.Name));
			if (File.Exists(baseModelPath))
			{
				File.Delete(baseModelPath);
			}

			string baseDatabasePath = Path.Combine(OutputScriptPath, string.Format("Base{0}Database.cs", m_Class.Name));
			if (File.Exists(baseDatabasePath))
			{
				File.Delete(baseDatabasePath);
			}

			string modelPath = Path.Combine(OutputScriptPath, string.Format("{0}.cs", m_Class.Name));
			if (File.Exists(modelPath))
			{
				File.Delete(modelPath);
			}

			string databasePath = Path.Combine(OutputScriptPath, string.Format("{0}Database.cs", m_Class.Name));
			if (File.Exists(databasePath))
			{
				File.Delete(databasePath);
			}

			string classFieldPath = Path.Combine(OutputClassFieldPath, string.Format("{0}Field.cs", m_Class.Name));
			if (File.Exists(classFieldPath))
			{
				File.Delete(classFieldPath);
			}

			// Delete all actions.
			foreach (Field field in m_Class.Fields)
			{
				string actionClassName = string.Format("{0}Get{1}.cs", m_Class.Name, field.CName);
				string actionClassPath = Path.Combine(OutputPlayMakerPath, actionClassName);
				if (File.Exists(actionClassPath))
				{
					File.Delete(actionClassPath);
				}
			}

			AssetDatabase.Refresh();
		}

		private void CreateScripts()
		{
			File.WriteAllText(Path.Combine(OutputScriptPath, string.Format("Base{0}.cs", m_Class.Name)), m_BaseModelScriptContent);
			File.WriteAllText(Path.Combine(OutputScriptPath, string.Format("Base{0}Database.cs", m_Class.Name)), m_BaseDatabaseScriptContent);

			string modelPath = Path.Combine(OutputScriptPath, string.Format("{0}.cs", m_Class.Name));
			if (!File.Exists(modelPath))
			{
				File.WriteAllText(modelPath, m_ModelScriptContent);
			}

			string databasePath = Path.Combine(OutputScriptPath, string.Format("{0}Database.cs", m_Class.Name));
			if (!File.Exists(databasePath))
			{
				File.WriteAllText(databasePath, m_DatabaseScriptContent);
			}

			File.WriteAllText(Path.Combine(OutputClassFieldPath, string.Format("{0}Field.cs", m_Class.Name)), m_ClassFieldScriptContent);

			// List all PlayMaker action files.
			Dictionary<string, string> playMakerScriptsToDelete = new Dictionary<string, string>();
			foreach (string filepath in Directory.GetFiles(OutputPlayMakerPath))
			{
				string filename = Path.GetFileName(filepath);
				if (filename.StartsWith(m_Class.Name + "Get") && !filename.StartsWith(m_Class.Name + "GetGet"))
				{
					playMakerScriptsToDelete.Add(Path.GetFileName(filepath), filepath);
				}
			}

			foreach (KeyValuePair<string, string> playMakerScript in m_PlayMakerScriptContents)
			{
				File.WriteAllText(Path.Combine(OutputPlayMakerPath, playMakerScript.Key), playMakerScript.Value);

				// Remove from delete list.
				if (playMakerScriptsToDelete.ContainsKey(playMakerScript.Key))
				{
					playMakerScriptsToDelete.Remove(playMakerScript.Key);
					playMakerScriptsToDelete.Remove(playMakerScript.Key + ".meta");
				}
			}

			foreach (KeyValuePair<string, string> playMakerFilepath in playMakerScriptsToDelete)
			{
				File.Delete(playMakerFilepath.Value);
			}
		}

		private void PrepareTemplate()
		{
			PrepareTemplateModel();
			PrepareTemplateBaseModel();
			PrepareTemplateDatabase();
			PrepareTemplateBaseDatabase();
			PrepareTemplateClassField();
			PrepareTemplatePlayMakerActions();
		}

		private void PrepareTemplateModel()
		{
			m_ModelScriptContent = GetGeneratedFormattedTemplate(TemplateFileModel, new {
				Class = m_Class
			});
		}

		private void PrepareTemplateBaseModel()
		{
			List<Dictionary<string, object>> fieldsToken = new List<Dictionary<string, object>>();
			StringBuilder requiredNamespaces = new StringBuilder();
			foreach (Field field in m_Class.Fields)
			{
				if (field.Name == "ID" || field.Name == "Name") continue;

				Dictionary<string, object> fieldToken = new Dictionary<string, object> {
					{ "Name", field.Name },
					{ "Type", field.FormattedType },
					{ "DefaultValue", field.DefaultValue },
					{ "CName", field.CName }
				};
				fieldsToken.Add(fieldToken);

				string requiredNamespace = EditorUtil.GetFieldTypeRequiredNamespace(field.Type);
				if (!string.IsNullOrEmpty(requiredNamespace))
				{
					requiredNamespaces.AppendFormat("using {0};\n", requiredNamespace);
				}
			}

			m_BaseModelScriptContent = GetGeneratedFormattedTemplate(TemplateFileBaseModel, new {
				Class = m_Class,
				RequiredNamespaces = requiredNamespaces.ToString()
			});
		}

		private void PrepareTemplateDatabase()
		{
			m_DatabaseScriptContent = GetGeneratedFormattedTemplate(TemplateFileDatabase, new {
				Class = m_Class
			});
		}

		private void PrepareTemplateBaseDatabase()
		{
			List<string> referenceDatabases = new List<string>();
			foreach (Field field in m_Class.Fields)
			{
				if (field.IsClassField && !referenceDatabases.Contains(field.ClassFieldName) && field.ClassFieldName != m_Class.Name)
				{
					referenceDatabases.Add(field.ClassFieldName);
				}
			}

			m_BaseDatabaseScriptContent = GetGeneratedFormattedTemplate(TemplateFileBaseDatabase, new {
				Class = m_Class,
				References = referenceDatabases.ToArray(),
				HasClassField = referenceDatabases.Count > 0
			});
		}

		private void PrepareTemplateClassField()
		{
			m_ClassFieldScriptContent = GetGeneratedFormattedTemplate(TemplateFileClassField, new {
				Class = m_Class
			});
		}

		private void PrepareTemplatePlayMakerActions()
		{
			m_PlayMakerScriptContents.Clear();
			foreach (Field field in m_Class.Fields)
			{
				if (field.Name == "ID") continue;

				string playMakerType = string.Empty;
				if (field.Type == typeof(FloatField))
				{
					playMakerType = "FsmFloat";
				}
				else if (field.Type == typeof(IntField))
				{
					playMakerType = "FsmInt";
				}
				else if (field.Type == typeof(BooleanField))
				{
					playMakerType = "FsmBool";
				}
				else if (field.Type == typeof(GameObjectField))
				{
					playMakerType = "FsmGameObject";
				}
				else if (field.Type == typeof(StringField))
				{
					playMakerType = "FsmString";
				}
				else if (field.Type == typeof(Vector2Field))
				{
					playMakerType = "FsmVector2";
				}
				else if (field.Type == typeof(Vector3Field))
				{
					playMakerType = "FsmVector3";
				}
				else if (field.Type == typeof(ColorField))
				{
					playMakerType = "FsmColor";
				}
				else if (field.Type == typeof(RectField))
				{
					playMakerType = "FsmRect";
				}
				else if (field.Type == typeof(MaterialField))
				{
					playMakerType = "FsmMaterial";
				}
				else if (field.Type == typeof(TextureField))
				{
					playMakerType = "FsmTexture";
				}
				else
				{
					continue;
				}

				string scriptContent = GetGeneratedFormattedTemplate(TemplateFilePlayMakerAction, new {
					Class = m_Class,
					Field = field,
					PlayMakerType = playMakerType
				});

				m_PlayMakerScriptContents.Add(string.Format("{0}Get{1}.cs", m_Class.Name, field.CName), scriptContent);
			}
		}

		private string GetGeneratedFormattedTemplate(string filename, object tokens)
		{
			string templatePath = Path.Combine(Path.GetDirectoryName(WorkingDirectory), TemplateDirectory);
			templatePath = Path.Combine(templatePath, TemplateDirectoryDefault);
			templatePath = Path.Combine(templatePath, filename);

			if (!File.Exists(templatePath))
			{
				Debug.LogErrorFormat("Could file template name: {0}", filename);
				return string.Empty;
			}

			string source = File.ReadAllText(templatePath);
			Template template = Template.Parse(source);
			string content = template.Render(Hash.FromAnonymousObject(tokens));
			return content;
		}

		private void PrepareDirectories()
		{
			if (!Directory.Exists(OutputPath))
			{
				Directory.CreateDirectory(OutputPath);
			}

			if (!Directory.Exists(OutputDatabasePath))
			{
				Directory.CreateDirectory(OutputDatabasePath);
			}

			if (!Directory.Exists(OutputScriptPath))
			{
				Directory.CreateDirectory(OutputScriptPath);
			}

			if (!Directory.Exists(LightningFieldScriptPath))
			{
				Directory.CreateDirectory(LightningFieldScriptPath);
			}

			if (!Directory.Exists(OutputPlayMakerPath))
			{
				Directory.CreateDirectory(OutputPlayMakerPath);
			}
		}

		public static string WorkingDirectory
		{
			get
			{
				string projectPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar;
				string filepath = new System.Diagnostics.StackTrace(true).GetFrame(0).GetFileName();
				string directory = Path.GetDirectoryName(filepath.Substring(projectPath.Length));
				return directory;
			}
		}

		public static string RootAssetDirectory
		{
			get
			{
				string rootDirectory = Directory.GetParent(Directory.GetParent(WorkingDirectory).ToString()).ToString();
				return rootDirectory;
			}
		}

		public static string OutputPath
		{
			get
			{
				string outputPath = Path.Combine(RootAssetDirectory, OutputDirectory);
				return outputPath;
			}
		}

		public static string OutputDatabasePath
		{
			get
			{
				string outputPath = Path.Combine(OutputPath, OutputDatabaseDirectory);
				return outputPath;
			}
		}

		public static string OutputScriptPath
		{
			get
			{
				string outputPath = Path.Combine(OutputPath, OutputScriptDirectory);
				return outputPath;
			}
		}

		public static string OutputClassFieldPath
		{
			get
			{
				string outputPath = Path.Combine(RootAssetDirectory, LightningFieldScriptPath);
				return outputPath;
			}
		}

		public static string OutputPlayMakerPath
		{
			get
			{
				string outputPath = Path.Combine(RootAssetDirectory, OutputPlayMakerDirectory);
				return outputPath;
			}
		}
	}
}
