using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class ProjectileBlueprint : BaseProjectileBlueprint
	{
		public ProjectileBlueprint() {}

		public ProjectileBlueprint(ProjectileBlueprint source) : base(source) {}
	}
}
