using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class AutonomousBlueprint : BaseAutonomousBlueprint
	{
		public AutonomousBlueprint() {}

		public AutonomousBlueprint(AutonomousBlueprint source) : base(source) {}
	}
}
