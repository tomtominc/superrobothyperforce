using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class BusterBlueprint : BaseBusterBlueprint
	{
		public BusterBlueprint() {}

		public BusterBlueprint(BusterBlueprint source) : base(source) {}
	}
}
