using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseProjectileBlueprint : BaseClass
	{
		public BaseProjectileBlueprint() {}

		public BaseProjectileBlueprint(BaseProjectileBlueprint source) : base(source)
		{
			
			
			m_Icon = source.Icon;
			
		}

		[SerializeField]
		protected Sprite m_Icon = null;

		public virtual Sprite Icon
		{
			get { return m_Icon; }
			set { m_Icon = value; }
		}
		
	}
}
