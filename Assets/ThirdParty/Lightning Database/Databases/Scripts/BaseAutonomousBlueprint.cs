using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseAutonomousBlueprint : BaseClass
	{
		public BaseAutonomousBlueprint() {}

		public BaseAutonomousBlueprint(BaseAutonomousBlueprint source) : base(source)
		{
			
			
			m_Description = source.Description;
			m_BaseHealth = source.BaseHealth;
			m_BasePower = source.BasePower;
			m_Element = source.Element;
			
		}

		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}
		[SerializeField]
		protected int m_BaseHealth = 0;

		public virtual int BaseHealth
		{
			get { return m_BaseHealth; }
			set { m_BaseHealth = value; }
		}
		[SerializeField]
		protected int m_BasePower = 0;

		public virtual int BasePower
		{
			get { return m_BasePower; }
			set { m_BasePower = value; }
		}
		[SerializeField]
		protected Element m_Element = Element.Normal;

		public virtual Element Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}
		
	}
}
