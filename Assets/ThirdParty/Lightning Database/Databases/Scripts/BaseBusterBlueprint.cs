using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseBusterBlueprint : BaseClass
	{
		public BaseBusterBlueprint() {}

		public BaseBusterBlueprint(BaseBusterBlueprint source) : base(source)
		{
			
			
			m_Description = source.Description;
			m_Rarity = source.Rarity;
			m_Element = source.Element;
			m_BasePower = source.BasePower;
			m_BaseSpeed = source.BaseSpeed;
			m_Icon = source.Icon;
			
		}

		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}
		[SerializeField]
		protected Rarity m_Rarity = Rarity.Common;

		public virtual Rarity Rarity
		{
			get { return m_Rarity; }
			set { m_Rarity = value; }
		}
		[SerializeField]
		protected Element m_Element = Element.Normal;

		public virtual Element Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}
		[SerializeField]
		protected int m_BasePower = 0;

		public virtual int BasePower
		{
			get { return m_BasePower; }
			set { m_BasePower = value; }
		}
		[SerializeField]
		protected int m_BaseSpeed = 0;

		public virtual int BaseSpeed
		{
			get { return m_BaseSpeed; }
			set { m_BaseSpeed = value; }
		}
		[SerializeField]
		protected Sprite m_Icon = null;

		public virtual Sprite Icon
		{
			get { return m_Icon; }
			set { m_Icon = value; }
		}
		
	}
}
