using UnityEngine;


namespace RutCreate.LightningDatabase
{
	[System.Serializable]
	public class CardBlueprint : BaseCardBlueprint
	{
		public CardBlueprint() {}

		public CardBlueprint(CardBlueprint source) : base(source) {}
	}
}
