using UnityEngine;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
	public abstract class BaseCardBlueprint : BaseClass
	{
		public BaseCardBlueprint() {}

		public BaseCardBlueprint(BaseCardBlueprint source) : base(source)
		{
			
			
			m_Description = source.Description;
			m_Rarity = source.Rarity;
			m_Element = source.Element;
			m_BasePower = source.BasePower;
			
		}

		[SerializeField]
		protected string m_Description = string.Empty;

		public virtual string Description
		{
			get { return m_Description; }
			set { m_Description = value; }
		}
		[SerializeField]
		protected Rarity m_Rarity = Rarity.Common;

		public virtual Rarity Rarity
		{
			get { return m_Rarity; }
			set { m_Rarity = value; }
		}
		[SerializeField]
		protected Element m_Element = Element.Normal;

		public virtual Element Element
		{
			get { return m_Element; }
			set { m_Element = value; }
		}
		[SerializeField]
		protected int m_BasePower = 0;

		public virtual int BasePower
		{
			get { return m_BasePower; }
			set { m_BasePower = value; }
		}
		
	}
}
