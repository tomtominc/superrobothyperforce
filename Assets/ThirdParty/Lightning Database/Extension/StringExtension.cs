﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace RutCreate
{
	public static class StringExtension
	{
		public static int ToInt(this string s)
		{
			int result = 0;
			int.TryParse(s, out result);
			return result;
		}

		public static float ToFloat(this string s)
		{
			float result = 0;
			float.TryParse(s, out result);
			return result;
		}

		public static bool ToBool(this string s)
		{
			return (string.Compare(s, "True", System.StringComparison.OrdinalIgnoreCase) == 0);
		}

		public static string ToUppercaseFirst(this string s)
		{
			if (String.IsNullOrEmpty(s))
				throw new ArgumentException("ARGH!");
			return s.First().ToString().ToUpper() + s.Substring(1);
		}

		/// <summary>
		/// Betters the format.
		/// See http://stackoverflow.com/a/4077118/1597295.
		/// </summary>
		/// <returns>The format.</returns>
		/// <param name="format">Format.</param>
		/// <param name="source">Source.</param>
		public static string BetterFormat(this string format, object source)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}

			string result = format;
			foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(source))
			{
				result = result.Replace("{{" + prop.Name + "}}", (prop.GetValue(source) ?? "(null)").ToString());
			}

			return result;
		}

		public static string BetterFormat(this string format, Dictionary<string, object> source)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}

			string result = format;
			foreach (KeyValuePair<string, object> entry in source)
			{
				result = result.Replace("{{" + entry.Key + "}}", entry.Value.ToString());
			}

			return result;
		}

		private static readonly IList<string> Unpluralizables = new List<string> {
			"equipment",
			"information",
			"rice",
			"money",
			"species",
			"series",
			"fish",
			"sheep",
			"deer"
		};

		private static readonly IDictionary<string, string> Pluralizations = new Dictionary<string, string> {
			// Start with the rarest cases, and move to the most common
			{ "person", "people" },
			{ "ox", "oxen" },
			{ "child", "children" },
			{ "foot", "feet" },
			{ "tooth", "teeth" },
			{ "goose", "geese" },
			// And now the more standard rules.
			{ "(.*)fe?", "$1ves" },         // ie, wolf, wife
			{ "(.*)man$", "$1men" },
			{ "(.+[aeiou]y)$", "$1s" },
			{ "(.+[^aeiou])y$", "$1ies" },
			{ "(.+z)$", "$1zes" },
			{ "([m|l])ouse$", "$1ice" },
			{ "(.+)(e|i)x$", @"$1ices" },    // ie, Matrix, Index
			{ "(octop|vir)us$", "$1i" },
			{ "(.+(s|x|sh|ch))$", @"$1es" },
			{ "(.+)", @"$1s" }
		};

		public static string Pluralize(this string singular)
		{
			if (Unpluralizables.Contains(singular))
				return singular;

			foreach (var pluralization in Pluralizations)
			{
				if (Regex.IsMatch(singular, pluralization.Key))
				{
					return Regex.Replace(singular, pluralization.Key, pluralization.Value);
				}
			}

			return singular;
		}

	}
}
