﻿using UnityEngine;
using System;
using System.Collections.Generic;


namespace RutCreate.LightningDatabase
{
    [System.Serializable]
    public class BaseClass
    {
        public BaseClass()
        {
			
        }

        public BaseClass(BaseClass source)
        {
            m_ID = source.ID;
            m_Name = source.Name;
        }

        #region Fields

        [SerializeField]
        protected int m_ID;

        public virtual int ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        [SerializeField]
        protected string m_Name;

        public virtual string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[{0}]: ID={1}, Name={2}", GetType().Name, ID, Name);
        }
    }

}
