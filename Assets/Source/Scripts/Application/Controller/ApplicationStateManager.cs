﻿using UnityEngine;
using System.Collections;
using StateManager;
using TaskManager;
using Framework.Core;
using System.Collections.Generic;

public enum GameEvent
{
    OnMainMenuOpen
}

// main menu
// for customize do a "garage menu"
//
public enum GameState
{
    MainMenu,
    CustomizeMenu,
    OpenPacksMenu,
    BattleScreen

}

public class ApplicationStateManager : Loader
{
    
    [Header("Game States")]
    [SerializeField] private GameState _currentGameState;
    [SerializeField] private MainMenuState _mainMenuState;
    [SerializeField] private CustomizeMenuState _customizeMenuState;
    [SerializeField] private BattleScreenState _battleScreenState;

    protected internal StateManager < ApplicationStateManager > _stateManager;
    protected internal TaskManager < ApplicationStateManager > _taskManager;

    private static ApplicationStateManager _instance;

    public static ApplicationStateManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType < ApplicationStateManager >();

            return _instance;
        }
    }

    public override IEnumerator Load()
    {
        //PhotonNetwork.ConnectUsingSettings("v1.0");

        _taskManager = new TaskManager<ApplicationStateManager>(this, gameObject);
        _stateManager = new StateManager< ApplicationStateManager >(this);

        _stateManager.AddState(_mainMenuState);
        _stateManager.AddState(_customizeMenuState);
        _stateManager.AddState(_battleScreenState);

        ChangeState(_currentGameState);

        yield break;
    }

    private void Update()
    {
        if (_stateManager == null)
            return;
        
        _stateManager.Update(Time.deltaTime);
    }

    public void ChangeState(GameState state)
    {
        _currentGameState = state;

        switch (_currentGameState)
        {
            case GameState.MainMenu:
                _stateManager.ChangeState < MainMenuState >();
                break;
            case GameState.CustomizeMenu:
                _stateManager.ChangeState<CustomizeMenuState>();
                break;
            case GameState.BattleScreen:
                _stateManager.ChangeState<BattleScreenState>();
                break;
        }
    }

    public void ChangeState(string state)
    {
        ChangeState(Enum<GameState>.Parse(state));
    }
}
