﻿using UnityEngine;
using StateManager;
using DoozyUI;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;


[System.Serializable]
public class CustomizeMenuState : State<ApplicationStateManager>
{
    public enum ToggleState
    {
        DeckToggle,
        HeroToggle,
        CraftToggle
    }

    public enum HeroSubState
    {
        BusterToggle,
        BodyToggle
    }

    [SerializeField] private WorldGrid world;
    [SerializeField] private BlueprintDisplay _blueprintDisplay;

    [SerializeField] private ToggleGroupUtility _mainGroupUtility;
    [SerializeField] private ToggleGroupUtility _heroGroupUtility;

    private ToggleState _currentState;

    private CharacterEntity characterEntity;

    public override void StateStart()
    {
        UIManager.ShowUiElement("CustomizeMenu");

        EntityData entityData = new EntityData("Character", EntityType.Character, new Vector2(1f, 1f));

        characterEntity = (CharacterEntity)world.SpawnEntity(entityData);

        CharacterData characterData = new CharacterData();

        _mainGroupUtility.ToggleChanged += HandleMainToggleChanged;
        _heroGroupUtility.ToggleChanged += HandleSubToggleChanged;
        _blueprintDisplay.ButtonClicked += HandleButtonClicked;

    }

    public override void StateUpdate(float deltaTime)
    {
        
    }

    public override void StateEnd()
    {
        _mainGroupUtility.ToggleChanged -= HandleMainToggleChanged;
        _heroGroupUtility.ToggleChanged -= HandleSubToggleChanged;

        UIManager.HideUiElement("CustomizeMenu");
    }

    private void HandleMainToggleChanged(object sender, ToggleEventArgs args)
    {
        Toggle toggle = args.ActiveToggle;

        if (toggle.name.Equals("DeckToggle"))
        {
            _currentState = ToggleState.DeckToggle;
            UIManager.HideUiElement("CustomizeMenu_Hero");
        }
        else if (toggle.name.Equals("HeroToggle"))
        {
            _currentState = ToggleState.HeroToggle;
            UIManager.ShowUiElement("CustomizeMenu_Hero");
        }
        else if (toggle.name.Equals("CraftToggle"))
        {
            _currentState = ToggleState.CraftToggle;
            UIManager.HideUiElement("CustomizeMenu_Hero");
        }
    }

    private void HandleSubToggleChanged(object sender, ToggleEventArgs args)
    {
        Toggle toggle = args.ActiveToggle;

        if (toggle.name.Equals("BusterToggle"))
        {
            _blueprintDisplay.SetDisplay(PartType.Buster);
        }
        else if (toggle.name.Equals("BodyToggle"))
        {
            _blueprintDisplay.SetDisplay(PartType.Body);
        }
    }

    private void HandleButtonClicked(object sender, ActionEventArgs e)
    {
        Debug.LogFormat("Property: {0} Action: {1}", e.Property, e.Action);
    }
  
}
