﻿using UnityEngine;
using System.Collections;
using StateManager;
using UnityEngine.UI;
using Framework.Core.EventSystem;
using DoozyUI;

[System.Serializable]
public class MainMenuState : State<ApplicationStateManager>
{
    [SerializeField] private UIEnergyBar _levelBar;
    [SerializeField] private UIEnergyBar _rankBar;
    [SerializeField] private UISpriteText _currencyLabel;
    [SerializeField] private UISpriteText _premiumCurrencyLabel;

    public override void StateStart()
    {
        // set up all the currency stuff by grabbing it from the persistent manager, who grabs off database

        UIManager.ShowUiElement("MainMenu");
        EventManager.Publish(this, GameEvent.OnMainMenuOpen, null);

    }

    public override void StateEnd()
    {
        UIManager.HideUiElement("MainMenu");

    }

}
