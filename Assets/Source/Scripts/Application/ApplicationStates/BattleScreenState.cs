﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using Framework.Core.EventSystem;
using DG.Tweening;
using DoozyUI;
using Framework.Core;

[System.Serializable]
public class BattleScreenState : State<ApplicationStateManager>
{
    public enum Character
    {
        ReineBleu
    }

    [SerializeField] private WorldGrid _world;
    [SerializeField] private FormationDatabase _formationDatabase;
    [SerializeField] private Character _startingCharacter;

    private BattleData _currentBattleData;
    private float _unPauseDelay = 3f;
    private bool _battlePaused = false;
    private bool _isUnpausing = false;


    public override void StateStart()
    {
        _currentBattleData = new BattleData(Theme.Plains, 0);

        EventManager.Subscribe(BattleEvent.OnBattlePreLoad, OnBattlePreLoad);
        EventManager.Subscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Subscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Subscribe(BattleEvent.OnBattleTogglePause, OnBattleTogglePause);
        EventManager.Subscribe(UIMenuEvent.TogglePause, OnTogglePause);

        EventManager.Publish(this, BattleEvent.OnBattlePreLoad, null, -1);
    }

    public override void StateEnd()
    {
        EventManager.Unsubscribe(BattleEvent.OnBattlePreLoad, OnBattlePreLoad);
        EventManager.Unsubscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Unsubscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Unsubscribe(BattleEvent.OnBattleTogglePause, OnBattleTogglePause);
        EventManager.Unsubscribe(UIMenuEvent.TogglePause, OnTogglePause);
    }

    private void OnTogglePause(IMessage message)
    {
        if (_isUnpausing)
            return;

        _battlePaused = !_battlePaused;

        if (_battlePaused)
        {
            EventManager.Publish(this, BattleEvent.OnBattleTogglePause, _battlePaused);
        }
        else if (!_isUnpausing)
        {
            _isUnpausing = true;

            EventManager.Publish(this, BattleEvent.OnBattleUnpausing, _unPauseDelay);

            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(_unPauseDelay);
            sequence.AppendCallback(() =>
                {
                    _isUnpausing = false;
                    EventManager.Publish(this, BattleEvent.OnBattleTogglePause, _battlePaused);
                });

            sequence.SetUpdate(UpdateType.Normal, true);

            sequence.Play();
        }
    }

    private void OnBattleTogglePause(IMessage message)
    {
        if (!_battlePaused)
        {
            UIManager.TogglePause();
        }
    }

    private void OnBattlePreLoad(IMessage message)
    {
        BattleData battleData = _currentBattleData;

        List<Formation> themedList = _formationDatabase.GetThemedList(battleData.theme);

        Selector selector = new Selector();

        Formation formation = selector.Single(themedList).Clone();

        if (battleData.score <= 0)
        {
            // formation.Add("Character", EntityType.Character, new Vector2(1f, 1f));

            // EventManager.Publish(this, WorldAction.SpawnEntity, new EntityData(_startingCharacter.ToString(), EntityType.Character, new Vector2(1f, 1f)));

            EntityData entityData = new EntityData(_startingCharacter.ToString(), EntityType.Character, new Vector2(1f, 1f));
            CharacterEntity characterEntity = (CharacterEntity)_world.SpawnEntity(entityData);
            CharacterData characterData = new CharacterData(CharacterData.State.Battle, UnitEntity.AnimatorState.OnIdle);

            characterEntity.SetCustomData(characterData);
        }

        foreach (EntityData entityData in formation.data)
        {
            EventManager.Publish(this, WorldAction.SpawnEntity, entityData);
        }


        EventManager.Publish(this, BattleEvent.OnBattleStart, null, -1);

    }

    private void OnCharacterDestroyed(IMessage message)
    {
        EventManager.Publish(this, BattleEvent.OnBattleFinish, null);
    }

    private void OnFormationDestroyed(IMessage message)
    {
        _currentBattleData.score++;
        EventManager.Publish(this, BattleEvent.OnBattleFinish, _currentBattleData);
    }

}
