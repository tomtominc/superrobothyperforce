﻿using UnityEngine;
using System.Collections;
using PathologicalGames;
using Framework.Core.Animation;

public class EffectManager : MonoBehaviour
{
    public static void DoEffect(string name, Vector2 point)
    {
        Transform effectTransform = PoolManager.Pools["Entity"].Spawn("Effect");
        effectTransform.position = point;
        effectTransform.name = name;

        SpriteAnimation animation = effectTransform.GetComponent < SpriteAnimation >();
        animation.PlayWithCallBack(name, (x, y) =>
            {
                PoolManager.Pools["Entity"].Despawn(effectTransform);
            }); 
    }

    public static Transform GetEffectPrefab()
    {
        return PoolManager.Pools["Entity"].Spawn("Effect");
    }
}
