﻿using UnityEngine;
using System.Collections;
using Framework.Utilities;
using System.Collections.Generic;
using System.Linq;
using Framework.Core.EventSystem;

public class FactoryManager : Singleton<FactoryManager>
{
    private static List<Factory> factories = new List<Factory>();

    public void OnEnable()
    {
        EventManager.Subscribe(BattleEvent.OnBattleFinish, OnBattleFinish);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(BattleEvent.OnBattleFinish, OnBattleFinish);
    }

    private void OnBattleFinish(IMessage message)
    {
        factories.ForEach(x => x.RemoveFromWorld());
        factories.Clear();
    }

    public static void AddToUpdater(Factory factory)
    {
        factories.Add(factory);
    }

    public static void RemoveFactory(Factory factory)
    {
        factories.Remove(factory);
    }

    private void Update()
    {
        factories.ForEach(x => x.Update(Time.deltaTime));
    }
}
