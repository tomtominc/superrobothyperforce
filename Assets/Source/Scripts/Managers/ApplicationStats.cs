﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;

public class ApplicationStats :  Loader
{
    public override IEnumerator Load()
    {
        EventManager.Subscribe(EntityEvent.OnSpawned, OnEntitySpawned);
        EventManager.Subscribe(EntityEvent.OnDeath, OnEntityDeath);
        EventManager.Subscribe(EntityEvent.OnChangedHealth, OnEntityChangedHealth);
        EventManager.Subscribe(EntityEvent.OnUpdateStatus, OnEntityUpdatedStatus);
        EventManager.Subscribe(ApplicationEvent.OnChangedApplicationState, OnChangedApplicationState);

        yield break;
    }

    private void OnDisable()
    {
        EventManager.Subscribe(EntityEvent.OnSpawned, OnEntitySpawned);
        EventManager.Subscribe(EntityEvent.OnDeath, OnEntityDeath);
        EventManager.Subscribe(EntityEvent.OnChangedHealth, OnEntityChangedHealth);
        EventManager.Subscribe(EntityEvent.OnUpdateStatus, OnEntityUpdatedStatus);
        EventManager.Subscribe(ApplicationEvent.OnChangedApplicationState, OnChangedApplicationState);
    }

    private void OnEntitySpawned(IMessage message)
    {
        
    }

    private void OnChangedApplicationState(IMessage message)
    {
        
    }

    private void OnEntityDeath(IMessage message)
    {
        
    }

    private void OnEntityChangedHealth(IMessage message)
    {
        
    }

    private void OnEntityUpdatedStatus(IMessage message)
    {
        
    }


}
