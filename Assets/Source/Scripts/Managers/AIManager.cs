﻿using UnityEngine;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using Framework.Core;

public enum AutonomousEvent
{
    Register,
    Deregister
}

public class AIManager : MonoBehaviour
{
    private Dictionary < string, List < AutonomousBehaviour > > _agents;
    private Dictionary < string , AutonomousBehaviour > _activeAgents;

    private void Awake()
    {
        _agents = new Dictionary<string, List < AutonomousBehaviour > >();
        _activeAgents = new Dictionary<string,AutonomousBehaviour >();

        EventManager.Subscribe(AutonomousEvent.Register, Register); 
        EventManager.Subscribe(AutonomousEvent.Deregister, Deregister);
        EventManager.Subscribe(BattleEvent.OnBattleStart, OnBattleStart);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(AutonomousEvent.Register, Register); 
        EventManager.Unsubscribe(AutonomousEvent.Deregister, Deregister);
        EventManager.Unsubscribe(BattleEvent.OnBattleStart, OnBattleStart);
    }

    private void Register(IMessage message)
    {
        AutonomousBehaviour agent = (AutonomousBehaviour)message.Data;

        if (_agents.ContainsKey(agent.name))
        {
            _agents[agent.name].Add(agent);
        }
        else
        {
            _agents.Add(agent.name, new List<AutonomousBehaviour>() { agent });
        }

    }

    private void Deregister(IMessage message)
    {
        AutonomousBehaviour agent = (AutonomousBehaviour)message.Data;

        AutonomousBehaviour current = GetCurrentAgent(agent.name);

        if (current == null)
            return;

        if (agent == current)
        {
            OnAgentFinished(agent);
        }

        RemoveAgent(agent);
    }

    private void RemoveAgent(AutonomousBehaviour agent)
    {
        if (!_agents.ContainsKey(agent.name))
            return;

        List < AutonomousBehaviour > agentList = _agents[agent.name];

        if (agentList.Contains(agent))
            agentList.Remove(agent);

        if (IsAgentsEmpty())
        {
            EventManager.Publish(this, BattleEvent.OnFormationDestroyed, null);
        }
    }


    private bool IsAgentsEmpty()
    {
        bool empty = true;

        foreach (var list in _agents)
        {
            if (list.Value.Count > 0)
            {
                empty = false;
                break;
            }
        }

        return empty;
    }

    private void OnBattleStart(IMessage message)
    {
        StartAllAgents();
    }

    private void OnAgentFinished(AutonomousBehaviour agent)
    {
        agent.StopAgent();

        if (_agents.ContainsKey(agent.name))
        {
            int index = _agents[agent.name].IndexOf(agent);
            index = index.Move(1, _agents[agent.name].Count - 1);

            StartAgent(agent.name, index);
        }
    }

    public AutonomousBehaviour GetCurrentAgent(string key)
    {
        if (!_activeAgents.ContainsKey(key))
            return null;

        return _activeAgents[key];
    }

    private void StartAgent(string key, int index)
    {
        if (!_agents.ContainsKey(key))
            return;

        List < AutonomousBehaviour > agents = _agents[key];

        if (agents.Count <= index)
            return;

        if (!_activeAgents.ContainsKey(key))
        {
            _activeAgents.Add(key, agents[index]);
        }
        else
        {
            _activeAgents[key] = agents[index];
        }

        _activeAgents[key].StartAgent(OnAgentFinished);

    }

    private void StartAllAgents()
    {
        foreach (var list in _agents)
        {
            StartAgent(list.Key, 0);
        }
    }
}
