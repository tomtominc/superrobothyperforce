﻿using UnityEngine;
using System.Collections;
using Framework.Utilities;

public class LayerManager : Singleton<LayerManager>
{
    public LayerMask noLayer;
    public LayerMask characterLayer;
    public LayerMask autonomousLayer;
    public LayerMask bulletLayer;

    public static LayerMask CharacterLayer
    {
        get { return Instance.characterLayer; }
    }

    public static LayerMask AutonomousLayer
    {
        get { return Instance.autonomousLayer; }
    }

    public static LayerMask BulletLayer
    {
        get { return Instance.bulletLayer; }
    }

    public static LayerMask NoLayer
    {
        get { return Instance.noLayer;  }
    }
}
