﻿using UnityEngine;
using System.Collections;
using Gamelogic.Grids;
using System.Collections.Generic;
using System.Security.Cryptography;
using System;
using Framework.Core.Animation;
using DG.Tweening;

public class Cell : SpriteCell
{
    public SpriteRenderer overlay;

   
    private bool _occupied = false;
    private bool _isDamagable = false;
    private bool _isTargeted = false;

    private List < Transform > _entities;
    private SpriteAnimation _animator;

    private Tweener blinkTween;

    public bool Occupied
    {
        get { return _occupied; }
        set
        {
            _occupied = value;
        }
    }

    public bool IsDamagable
    {
        get { return _isDamagable; }

        set
        {
            _isDamagable = value;

            if (_isDamagable)
            {
                overlay.color = Color.white;
            }
            else
            {
                overlay.color = new Color(1f, 1f, 1f, 0f);
            }
        }
    }

    public bool IsTargeted
    {
        get { return _isTargeted; }

        set
        {
            _isTargeted = value;

            if (_isTargeted)
            {
                overlay.color = Color.white;

                blinkTween = overlay.DOFade(0f, 0.1f).SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                blinkTween.Kill();
                overlay.color = new Color(1f, 1f, 1f, 0f);
            }
        }
    }

    public void Initialize()
    {
        _entities = new List<Transform>();
        _animator = GetComponentInChildren < SpriteAnimation >(true);
    }

    public void SetSprite(string side, string type, int yValue)
    {
        _animator.PlayFormat("{0}_{1}_{2}", side, type, yValue);
    }

    public void AddToCell(Transform entity)
    {
        if (_entities.Contains(entity))
        {
            //Debug.LogWarningFormat("Same entity {0} trying to occupy the same cell {1}. This is not allowed.", entity, transform);
            return;
        }

        _entities.Add(entity);

        UpdateOccupiedStatus();
    }


    public void RemoveFromCell(Transform entity)
    {
        if (_entities.Contains(entity))
        {
            _entities.Remove(entity);
        }
        else
        {
            // Debug.LogWarningFormat ("Trying to remove {0} from cell {1} yet it's not there. This is not allowed.", entity , transform );
            return;
        }

        UpdateOccupiedStatus();
    }

    public bool Contains(Transform entity)
    {
        return _entities.Contains(entity);
    }

    protected void UpdateOccupiedStatus()
    {
        _entities.RemoveAll(x => x == null);

        if (_entities.Count > 0)
        {
            bool isOccupiedByDamagable = false;

            foreach (Transform child in _entities)
            {
                IEntity entity = child.GetComponent < IEntity >();
                if (entity == null)
                    continue;
                
                if (entity is AutonomousBehaviour && ((AutonomousBehaviour)entity).autonomousType == AutonomousBehaviour.AutonomousType.Projectile)
                {
                    isOccupiedByDamagable = true;
                }
            }

            IsDamagable = isOccupiedByDamagable;
            Occupied = true;
        }
        else
        {
            IsDamagable = false;
            Occupied = false;
        }

       
    }
}
