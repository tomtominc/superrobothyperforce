﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

[System.Serializable]
public class ProjectileKey
{
    public string value;

    public ProjectileBlueprint Value
    {
        get { return DatabaseManager.GetProjectileBlueprint(value); }
    }
}

