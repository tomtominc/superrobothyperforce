﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

[System.Serializable]
public class AutonomousKey
{
    public string value;

    public AutonomousBlueprint Value
    {
        get { return DatabaseManager.GetAutonomousBlueprint(value); }
    }
}
