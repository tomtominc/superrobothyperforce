﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using RutCreate.LightningDatabase;
using System.Collections.Generic;

public enum DatabaseType
{
    Autonomous,
    Buster,
    Cartridge
}


public class DatabaseManager : Loader
{
    private static DatabaseManager _instance;

    public static DatabaseManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType < DatabaseManager >();

            return _instance;
        }
    }

    public List < Sprite > ElementSprites;

    [SerializeField] private BusterBlueprintDatabase _busterBlueprintDatabase;
    [SerializeField] private ProjectileBlueprintDatabase _projectileBlueprintDatabase;
    [SerializeField] private AutonomousBlueprintDatabase _autonomousBlueprintDatabase;

    public override IEnumerator Load()
    {
        yield break;
    }

    public static List < BusterBlueprint > GetAllBusters()
    {
        return Instance._busterBlueprintDatabase.FindAll();
    }

    public static BusterBlueprint GetBusterBlueprint(string name)
    {
        return Instance._busterBlueprintDatabase.Find(name);
    }

    public static BusterBlueprint GetBusterBlueprint(int index)
    {
        return Instance._busterBlueprintDatabase.Find(index + 1);
    }

    public static BusterBlueprintDatabase GetBusterBlueprintDatabase()
    {
        return Instance._busterBlueprintDatabase;
    }

    public static Sprite GetElementSprite(Element element)
    {
        return Instance.ElementSprites.Find(x => x.name == element.ToString());
    }

    public static AutonomousBlueprint GetAutonomousBlueprint(string name)
    {
        return Instance._autonomousBlueprintDatabase.Find(name);
    }

    public static AutonomousBlueprint GetAutonomousBlueprint(int index)
    {
        return Instance._autonomousBlueprintDatabase.Find(index + 1);
    }

    public static AutonomousBlueprintDatabase GetAutonomousBlueprintDatabase()
    {
        return Instance._autonomousBlueprintDatabase;
    }

    public static ProjectileBlueprint GetProjectileBlueprint(string name)
    {
        return Instance._projectileBlueprintDatabase.Find(name);
    }

    public static ProjectileBlueprintDatabase GetProjectileBlueprintDatabase()
    {
        return Instance._projectileBlueprintDatabase;
    }

    public static ProjectileBlueprint GetProjectileBlueprint(int index)
    {
        return Instance._projectileBlueprintDatabase.Find(index + 1);
    }
   
}
