﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

[System.Serializable]
public class DatabaseKey
{
    public DatabaseType type;
    public int index;
    public bool showInEditor = false;

    private BaseClass _value;
    private int _lastIndex;
    private DatabaseType _lastType;

    public BaseClass Value
    {
        get
        {
            Refresh();
            return _value;  
        }
    }

    private void Refresh()
    {
        if (_lastType != type || index != _lastIndex || _value == null)
        {
            _lastIndex = index;
            _lastType = type;

            switch (type)
            {
                case DatabaseType.Autonomous:
                    _value = DatabaseManager.GetAutonomousBlueprint(index);
                    break;

                case DatabaseType.Buster:
                    _value = DatabaseManager.GetBusterBlueprint(index);
                    break;
            }
        }    
    }
}

