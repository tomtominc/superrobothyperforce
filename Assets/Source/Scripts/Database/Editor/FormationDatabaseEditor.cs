﻿using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core;

[CustomEditor(typeof(FormationDatabase))]
public class FormationDatabaseEditor : Editor
{
    private FormationDatabase _formationDatabase;
    private List<string> _enemyKeys;
    private const string NonValidKey = "~ NONE ~";

    private List<Formation> _formations
    {
        get { return _formationDatabase.formationsData; }
    }

    private List<Formation> GetFormations(Theme theme)
    {
        return _formationDatabase.formationsData.Where(x => x.theme == theme).ToList(); 
    }

    private int _currentIndex
    {
        get { return EditorPrefs.GetInt("CurrentIndex", 0); }
        set { EditorPrefs.SetInt("CurrentIndex", value); }
    }

    private void OnEnable()
    {
        _formationDatabase = (FormationDatabase)target;

        if (_formationDatabase.formationsData == null)
            _formationDatabase.formationsData = new List<Formation>();

        _enemyKeys = new List<string>() { NonValidKey };

        List <string> keys = DatabaseManager.GetAutonomousBlueprintDatabase().FindAll().Select(x => x.Name).ToList();

        _enemyKeys.AddRange(keys);
    }

    public override void OnInspectorGUI()
    {
        string[] themes = Enum<Theme>.ToList().ToArray();

        int themeIndex = EditorGUIExtensions.DrawSelectableHeader("FormationThemeHeader", themes);

        Theme theme = Enum<Theme>.Parse(themes[themeIndex]);

        EditorGUIExtensions.BeginContent(Color.white);
        DrawThemeHeader(theme);
        EditorGUIExtensions.EndContent();
    }

    private void DrawThemeHeader(Theme theme)
    {
        var themedList = GetFormations(theme);

        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(string.Format("Current: [{0}]", _currentIndex));

        if (GUILayout.Button("<"))
            _currentIndex = _currentIndex.Wrap(-1, themedList.Count);

        if (GUILayout.Button(">"))
            _currentIndex = _currentIndex.Wrap(1, themedList.Count);

        if (GUILayout.Button("Add"))
        {
            _formations.Add(new Formation() { theme = theme, data = new List<EntityData>() });
            _currentIndex = _formations.Count - 1;
        }

        if (GUILayout.Button("Remove"))
        {
            if (themedList.Count > _currentIndex)
            {
                Formation formationRemoval = themedList[_currentIndex];
                _formations.Remove(formationRemoval);
            }
          
        }

        EditorGUILayout.EndHorizontal();

        if (themedList.Count <= _currentIndex)
        {
            EditorGUILayout.HelpBox("No Formations in this theme, try adding one to the database!", MessageType.Warning);
            return;
        }

        Formation formation = themedList[_currentIndex];

            
        DrawFormation(formation);

    }

    private void DrawFormation(Formation formation)
    {
        int width = 3;
        int height = 0;
        int maxHeight = 2;
        int maxWidth = 3;
            
        for (int i = maxHeight; i >= height; i--)
        {
            EditorGUILayout.BeginHorizontal();
            for (int j = maxWidth; j < width + maxWidth; j++)
            {
                //int index = (i + j) + (width * i);
                string key = GetEntityInFormation(formation, j, i);

                int selectedIndex = _enemyKeys.IndexOf(key);
                string displayName = string.Format("[{0},{1}]", j, i);
                EditorGUILayout.LabelField(displayName, GUILayout.Width(32f));
                selectedIndex = EditorGUILayout.Popup(selectedIndex, _enemyKeys.ToArray());

                if (selectedIndex > 0)
                {
                    AddEntityToFormation(formation, 
                        new EntityData(_enemyKeys[selectedIndex], 
                            EntityType.Projectile, new Vector2(j, i)));
                }
                else
                {
                    RemoveEntityFromFormation(formation, j, i);
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        formation.selectionPercentage = EditorGUILayout.Slider("Selection Percentage (%)", formation.selectionPercentage, 0.1f, 100f);
    }

    private void AddEntityToFormation(Formation formation, EntityData data)
    {
        EntityData entity = formation.data.Where(x => x.point == new Vector2(data.point.x, data.point.y)).FirstOrDefault();

        if (entity != null && entity.key != data.key)
        {
            RemoveEntityFromFormation(formation, (int)data.point.x, (int)data.point.y);
            formation.Add(data); 
            return;
        }

        if (entity == null)
        {
            formation.Add(data);
        }
    }

    private void RemoveEntityFromFormation(Formation formation, int i, int j)
    {
        EntityData entity = formation.data.Where(x => x.point == new Vector2(i, j)).FirstOrDefault();

        if (entity != null)
            formation.data.Remove(entity);
    }

    private string GetEntityInFormation(Formation formation, int i, int j)
    {
        EntityData entity = formation.data.Where(x => x.point == new Vector2(i, j)).FirstOrDefault();

        if (entity != null)
        {
            return entity.key;
        }

        return NonValidKey;
    }

    //    public static void CreateFormationDatabase()
    //    {
    //        ScriptableObjectUtility.CreateAsset<FormationDatabase>();
    //    }
}
