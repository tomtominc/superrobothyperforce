﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using RutCreate.LightningDatabase;

[CustomPropertyDrawer(typeof(DatabaseKey))]
public class DatabaseKeyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int lines = 1;
        SerializedProperty showInEditor = property.FindPropertyRelative("showInEditor");

        if (showInEditor.boolValue)
        {
            SerializedProperty typeEnum = property.FindPropertyRelative("type");
            int value = typeEnum.enumValueIndex;

            switch (value)
            {
                case 0:
                    lines = 5;
                    break;
            }
        }

        return base.GetPropertyHeight(property, label) * lines;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        float height = 16f;
        float enumWidth = 0.7f;
        float buttonWidth = 0.1f;
        float indexWidth = 1f - (enumWidth + buttonWidth);

        SerializedProperty typeEnum = property.FindPropertyRelative("type");
        int value = typeEnum.enumValueIndex;

        EditorGUI.PropertyField(new Rect(position.x + position.width * 0, position.y, position.width * enumWidth, height), property.FindPropertyRelative("type"), label);

        List<string> keyNames = new List<string>();

        switch (value)
        {
            case 0:
                keyNames = DatabaseManager.GetAutonomousBlueprintDatabase().FindAll().Select(x => x.Name).ToList();
                break;

            case 1: 
                keyNames = DatabaseManager.GetBusterBlueprintDatabase().FindAll().Select(x => x.Name).ToList();
                break;
                
        }

        SerializedProperty indexValue = property.FindPropertyRelative("index");

        if (indexValue.intValue >= keyNames.Count)
            indexValue.intValue = 0;

        indexValue.intValue = EditorGUI.Popup(new Rect(position.x + position.width * enumWidth, position.y, position.width * indexWidth, height), indexValue.intValue, keyNames.ToArray());

        SerializedProperty showInEditor = property.FindPropertyRelative("showInEditor");

        Color buttonColor = showInEditor.boolValue ? Color.green : Color.white;

        GUI.color = buttonColor;
        if (GUI.Button(new Rect(position.x + position.width * (indexWidth + enumWidth), position.y, position.width * buttonWidth, height), "EDIT", EditorStyles.miniButton))
        {
            showInEditor.boolValue = !showInEditor.boolValue;
        }
        GUI.color = Color.white;

        if (showInEditor.boolValue)
        {
            EditorGUI.indentLevel++;

            switch (value)
            {
                case 0: 
                    DrawAutonomousKey(position, DatabaseManager.GetAutonomousBlueprint(indexValue.intValue));
                    break;

                default:

                    break;
            }
            EditorGUI.indentLevel--;
        }

        EditorGUI.EndProperty();
    }

    public void DrawAutonomousKey(Rect position, AutonomousBlueprint blueprint)
    {
        if (blueprint == null)
            return;
        
        float height = 16f;

        blueprint.Name = EditorGUI.TextField(new Rect(position.x, position.y + height * 1, position.width, height), "Name", blueprint.Name);
        blueprint.Description = EditorGUI.TextField(new Rect(position.x, position.y + height * 2, position.width, height), "Description", blueprint.Description);
        blueprint.BaseHealth = EditorGUI.IntField(new Rect(position.x, position.y + height * 3, position.width, height), "Base Health", blueprint.BaseHealth);
        blueprint.Element = (Element)EditorGUI.EnumPopup(new Rect(position.x, position.y + height * 4, position.width, height), "Element", blueprint.Element);
    }
}

