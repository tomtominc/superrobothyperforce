﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using RutCreate.LightningDatabase;

[CustomPropertyDrawer(typeof(AutonomousKey))]
public class AutonomousKeyDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var value = property.FindPropertyRelative("value");

        List < string > keyNames = DatabaseManager.GetAutonomousBlueprintDatabase().FindAll().Select(x => x.Name).ToList();

        int index = 0;

        if (!string.IsNullOrEmpty(value.stringValue))
        {
            index = keyNames.IndexOf(value.stringValue);   
        }

        index = EditorGUI.Popup(position, label.text, index, keyNames.ToArray());

        if (index >= keyNames.Count)
            index = 0;

        if (keyNames.Count > 0)
            value.stringValue = keyNames[index];


        EditorGUI.EndProperty();
    }
}
