﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class FormationDatabase : ScriptableObject
{
    [SerializeField]
    public List < Formation > formationsData;

    public List<Formation> GetThemedList(Theme theme)
    {
        return formationsData.Where(x => x.theme == theme).ToList();
    }
}
