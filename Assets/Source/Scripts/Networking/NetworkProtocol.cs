﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Core
{
    public enum NetworkProtocol
    {
        RegisterPlayer = 1000,
        StartGame = 1001,
        EndGame = 1002,
        StartTurn = 1003,
        EndTurn = 1004,
        StopTurn = 1005,
        UpdatePlayerAttributes = 1006,
        UpdateOpponentAttributes = 1007,
        PlayCard = 1008,
        SelectTargetPlayer = 1009,
        SelectTargetCard = 1010,
        TargetPlayerSelected = 1011,
        TargetCardSelected = 1012,
        AttackingCardSelected = 1013,
        AttackingCardUnselected = 1014,
        AttackedPlayerSelected = 1015,
        AttackedCardSelected = 1016,
        DrawCards = 1017,
        DiscardCards = 1018,
        CardsToDiscardSelected = 1019,
        CardsAutoDiscarded = 1020,
        SendChatTextMessage = 1021,
        BroadcastChatTextMessage = 1022,
        KilledCard = 1023,
        MoveCards = 1024,
    }

    public class NetworkMessage
    {
        
    }

    public class DrawCardMessage : NetworkMessage
    {
        public int PlayerId;
    }

    public class EnterZoneMessage : NetworkMessage
    {
        public int OwnerId;

        public int GameZoneId;

        public CardState EnteringCard;
    }

    public class OtherEnterZoneMessage : NetworkMessage
    {
        public int OtherOwnerId;

        public int OtherGameZoneId;

        public CardState OtherCard;
    }

    public class PlayCardMessage : NetworkMessage
    {
        /// <summary>
        /// The owner identifier.
        /// </summary>
        public int OwnerId;

        /// <summary>
        /// The card identifier.
        /// </summary>
        public CardState CartridgeState;
    }
}

