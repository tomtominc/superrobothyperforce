﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core;

/// <summary>
/// The state of the cartridge on the server
/// this is jsonified and sent over to update 
/// the game.
/// </summary>
public class CardState
{
    /// <summary>
    /// The net identifier.
    /// </summary>
    public int GameId;

    /// <summary>
    /// The id that this state represents on the network    
    /// </summary>
    public int CardId;

    /// <summary>
    /// Which player owns this card
    /// </summary>
    public int OwnerId;

    /// <summary>
    /// currently where the cell is on the board.
    /// </summary>
    public int BoardId;

    /// <summary>
    /// The status of this cartridage or entity, (e.g. burned cartridges will lose health every turn.)
    /// </summary>
    public StatusEffect CurrentStatus = StatusEffect.Normal;

    /// <summary>
    /// The type.
    /// </summary>
    public CardType Type;

    /// <summary>
    /// The type of the tile.
    /// </summary>
    public TileType TileType;

    /// <summary>
    /// The modified health.
    /// </summary>
    public int DamageCounters = 0;

    /// <summary>
    /// The given health.
    /// </summary>
    public int GivenHealth = 0;

    /// <summary>
    /// The given power.
    /// </summary>
    public int GivenPower = 0;

    /// <summary>
    /// Does this card have actions left?
    /// If so we allow it to use those
    /// actions when conditions are met.
    /// </summary>
    public bool HasActions = false;

    /// <summary>
    /// The attached weapon.
    /// Only applicable to hero card types.
    /// </summary>
    public int weaponAttachmentId = -1;

    /// <summary>
    /// The attached body.
    /// Only applicable to hero card types.
    /// </summary>
    public int bodyAttachmentId = -1;

    /// <summary>
    /// The attached cannon.
    /// Only applicable to hero card types.
    /// </summary>
    public int cannonAttachmentId = -1;

    /// <summary>
    /// What gamezone is this card in?
    /// </summary>
    public GameZone currentGameZone;

    public CardState()
    {
        
    }

    public CardState(string cardName, int gameId, int ownerId)
    {
        Card card = CollectionManager.Instance.GetCard(cardName);

        if (card == null)
        {
            Debug.LogFormat("Couldn't find card with name: {0}", cardName);
            return;
        }

        CardId = card.Id;
        GameId = gameId;
        OwnerId = ownerId;
        Type = card.CardType;
    }

    /// <summary>
    /// Gets the equipped weapon, if no weapon is 
    /// equipped, returns this cards gameId. 
    /// </summary>
    /// <returns>The weapon.</returns>
    public int GetWeapon()
    {
        return weaponAttachmentId;
    }

    /// <summary>
    /// Damage the specified amount.
    /// </summary>
    /// <param name="amount">Amount.</param>
    public void Damage(DamageInfo info)
    {
        DamageCounters += info.amount;
    }

    /// <summary>
    /// Gets the cartridge from the collection manager
    /// </summary>
    /// <returns>The cartridge.</returns>
    public Card GetCard()
    {
        return CollectionManager.Instance.GetCard(CardId);
    }

    /// <summary>
    /// Gets the health.
    /// </summary>
    /// <returns>The health.</returns>
    public int GetHealth()
    {
        Card cartridge = GetCard();
        int health = cartridge.Health;
        return (health + GivenHealth) - DamageCounters;
    }

    /// <summary>
    /// The description of the cartridge.
    /// </summary>
    /// <returns>The description.</returns>
    public string GetDescription()
    {
        return GetCard().Description;
    }

    /// <summary>
    /// The name of the cartridge.
    /// </summary>
    /// <returns>The name.</returns>
    public string GetName()
    {
        return GetCard().Name;
    }

    /// <summary>
    /// Gets the power.
    /// </summary>
    /// <returns>The power.</returns>
    public int GetPower()
    {
        return GetCard().Power;
    }

    public Element GetElement()
    {
        return GetCard().Element;
    }

    /// <summary>
    /// Gets the type of weapon, pets and buildings
    /// don't have weapons equpped but have a weaopn
    /// type. 
    /// Heros are the only things that can equip weapons.
    /// If no weapon is attached this will return none.
    /// </summary>
    /// <returns>The weapon type.</returns>
    public WeaponType GetWeaponType()
    {
        return GetCard().WeaponType;
    }

    public void SetTileType(TileType tileType)
    {
        TileType = tileType;
    }

    public TileType GetTileType()
    {
        return TileType;
    }

}
