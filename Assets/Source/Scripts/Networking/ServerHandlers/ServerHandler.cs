﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;

namespace Framework.Core
{
    [System.Serializable]
    public class ServerHandler : State < Server >
    {
        /// <summary>
        /// Access to the master server
        /// </summary>
        protected Server _server;

        public ServerHandler(Server server)
        {
            _server = server;
        }
    }
}
