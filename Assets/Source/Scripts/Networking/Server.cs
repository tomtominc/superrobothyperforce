﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using TaskManager;
using Newtonsoft.Json;

namespace Framework.Core
{
    public class Server //: Photon.PunBehaviour
    {

        public string VERSION = "v0.0.1";
        /// <summary>
        /// The State Manager is used to update the server state
        /// it runs specific tasks in order to handle different senarios
        /// like drawing cards / playing cards / moving entities etc..
        /// </summary>
        protected internal StateManager < Server > _stateManager;

        /// <summary>
        /// The task manager is used to help the state manager 
        /// manage subtasks within a state. tasks are usually like waiting 
        /// and moving of a specific entity.
        /// </summary>
        protected internal TaskManager < Server > _taskManager;

        /// <summary>
        /// Gets the max players, always returns 2.
        /// </summary>
        /// <value>The max players.</value>
        public int MaxPlayers { get { return 2; } }

        /// <summary>
        /// Gets the duration of the turn. This changes based on 
        /// if the player has already had their turn ended.
        /// </summary>
        /// <value>The duration of the turn.</value>
        public float TurnDuration { get; private set; }

        /// <summary>
        /// Gets the size of the max hand, always returns 10.
        /// </summary>
        /// <value>The size of the max hand.</value>
        public int MaxHandSize { get { return 10; } }

        /// <summary>
        /// Current player (the player whos turn it is)
        /// </summary>
        /// <value>The current player.</value>
        public PlayerState CurrentPlayer { get; private set; }

        /// <summary>
        /// Gets the current opponent.
        /// </summary>
        /// <value>The current opponent.</value>
        public PlayerState CurrentOpponent { get; private set; }

        /// <summary>
        /// Gets the client player.
        /// </summary>
        /// <value>The client player.</value>
        public PlayerState ClientPlayer { get; private set; }

        /// <summary>
        /// The current turn.
        /// </summary>
        protected int _currentTurn;

        /// <summary>
        /// True if the game is finished; false otherwise.
        /// </summary>
        protected bool _gameFinished;

        public void Start()
        {
            //PhotonNetwork.ConnectUsingSettings(VERSION);
        }

        public virtual void PlayCartridge(CardState cartridgeState)
        {
            // change the state of the game to play cartridge
            // the play cartridge state will then try and spawn a specific 
            // cartridge on a specific point on the board
            // (at this point all the cool effects happen)
            // after the play cartridge state will change the 
            // state to the on spawned state passing the just spawned cartridge
            // the on spawned handler will check the cartridges effects 
            // and what's on the board, in hand, and in graveyard.
            // the effect will then react to anything that meets certain conditions.

            PlayCardMessage objectMessage = new PlayCardMessage();

            objectMessage.OwnerId = ClientPlayer.NetId;
            objectMessage.CartridgeState = cartridgeState;

            string message = JsonConvert.SerializeObject(objectMessage);

            //photonView.RPC("PlayCartridge_RPC", PhotonTargets.Others, message);
        }

        //        [PunRPC]
        //        public void PlayCartridge_RPC(string message)
        //        {
        //
        //        }

        public virtual Entity2D CreateEntityCartridgeObject(Card cartridge)
        {
            return null;
        }
    }
}
