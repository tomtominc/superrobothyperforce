﻿using UnityEngine;
using UnityEditor;

namespace RutCreate.LightningDatabase
{
    [FieldInfo("Element", "Custom", typeof(Element), "Element", "Element.Normal")]
    public class ElementDrawer : FieldType
    {
        public override object DrawField(object item)
        {
            Element value = (item == null) ? Element.Normal : (Element)item;
            value = (Element)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return Element.Normal;
        }
    }

    [FieldInfo("Rarity", "Custom", typeof(Rarity), "Rarity", "Rarity.Common")]
    public class RarityDrawer : FieldType
    {
        public override object DrawField(object item)
        {
            Rarity value = (item == null) ? Rarity.Common : (Rarity)item;
            value = (Rarity)EditorGUILayout.EnumPopup(value);
            return value;
        }

        public override object GetDefaultValue()
        {
            return Rarity.Common;
        }
    }
}