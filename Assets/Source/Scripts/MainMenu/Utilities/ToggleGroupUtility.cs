
using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class ToggleGroupUtility : MonoBehaviour
{
    private Toggle _currentlyActiveToggle;
    private ToggleGroup _toggleGroup;

    public event EventHandler<ToggleEventArgs> ToggleChanged;

    private void Start()
    {
        _toggleGroup = GetComponent<ToggleGroup>();

    }

    private void Update()
    {
        Toggle changed;

        if (IsToggleChanged(_currentlyActiveToggle, _toggleGroup, out changed))
        {
            _currentlyActiveToggle = changed;

            if (ToggleChanged != null)
                ToggleChanged.Invoke(this, new ToggleEventArgs(changed, _toggleGroup));
        }
    }

    private bool IsToggleChanged(Toggle toggle, ToggleGroup toggleGroup, out Toggle current)
    {
        if (toggleGroup == null)
        {
            current = null;
            return false;
        }

        Toggle lastToggle = toggle;

        toggle = GetActiveToggle(toggleGroup);

        current = toggle;

        if (lastToggle != toggle)
        {
            return true;
        }

        return false;
    }

    private Toggle GetActiveToggle(ToggleGroup toggleGroup)
    {
        Toggle toggle = null;

        if (toggleGroup.AnyTogglesOn())
        {
            List<Toggle> _active = toggleGroup.ActiveToggles().ToList();

            if (_active.Count > 0)
            {
                toggle = _active[0];
            }
        }

        return toggle;
    }


}

public class ToggleEventArgs : EventArgs
{
    public Toggle ActiveToggle { get; private set; }

    public ToggleGroup ToggleGroup { get; private set; }

    public ToggleEventArgs(Toggle ActiveToggle, ToggleGroup ToggleGroup)
    {
        this.ActiveToggle = ActiveToggle;
        this.ToggleGroup = ToggleGroup;
    }
}