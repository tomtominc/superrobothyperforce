﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;
using DoozyUI;

public class BattleGUIManager : Loader
{
    public override IEnumerator Load()
    {
        EventManager.Subscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Subscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Subscribe(BattleEvent.OnBattleUnpausing, OnBattleUnpausing);
        
        yield break;
    }

    private void OnCharacterDestroyed(IMessage message)
    {
        
    }

    private void OnBattleUnpausing(IMessage message)
    {
        float delay = (float)message.Data;

        UIManager.ShowUiElement("UnpauseBattleMenu");

        this.DoActionIndependentTimeScale(delay, () =>
            {
                UIManager.HideUiElement("UnpauseBattleMenu");
            });
    }

    private void OnFormationDestroyed(IMessage message)
    {
        UIManager.ShowUiElement("BattleCompleteMenu"); 

        this.DoActionIndependentTimeScale(2f, NextFormationButtonAction);

    }

    public void NextFormationButtonAction()
    {
        UIManager.HideUiElement("BattleCompleteMenu");
        EventManager.Publish(this, BattleEvent.OnBattlePreLoad, null, -1);
    }


}
