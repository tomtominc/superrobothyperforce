﻿using UnityEngine;
using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using DoozyUI;
using DG.Tweening;

public enum BattleEvent
{
    OnBattlePreLoad,
    OnBattleInitialize,
    OnBattleStart,
    OnFormationDestroyed,
    OnCharacterDestroyed,
    OnBattleFinish,
    OnBattleUnpausing,
    OnBattleTogglePause
}

public class BattleManager : Loader
{
    public FormationDatabase formationDatabase;
    public BattleData currentBattleData;

    private float _unPauseDelay = 3f;
    private bool _battlePaused = false;
    private bool _isUnpausing = false;

    public override IEnumerator Load()
    {
        currentBattleData = new BattleData(Theme.Plains, 0);

        EventManager.Subscribe(BattleEvent.OnBattlePreLoad, OnBattlePreLoad);
        EventManager.Subscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Subscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Subscribe(BattleEvent.OnBattleTogglePause, OnBattleTogglePause);
        EventManager.Subscribe(UIMenuEvent.TogglePause, OnTogglePause);

        //EventManager.Publish(this, BattleEvent.OnBattlePreLoad, null, -1);

        yield return null;
    }

    private void OnTogglePause(IMessage message)
    {
        if (_isUnpausing)
            return;
        
        _battlePaused = !_battlePaused;

        if (_battlePaused)
        {
            EventManager.Publish(this, BattleEvent.OnBattleTogglePause, _battlePaused);
        }
        else if (!_isUnpausing)
        {
            _isUnpausing = true;

            EventManager.Publish(this, BattleEvent.OnBattleUnpausing, _unPauseDelay);

            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(_unPauseDelay);
            sequence.AppendCallback(() =>
                {
                    _isUnpausing = false;
                    EventManager.Publish(this, BattleEvent.OnBattleTogglePause, _battlePaused);
                });

            sequence.SetUpdate(UpdateType.Normal, true);

            sequence.Play();
        }

       
    }

    private void OnBattleTogglePause(IMessage message)
    {
        if (!_battlePaused)
        {
            UIManager.TogglePause();
        }
           
    }

    private void OnBattlePreLoad(IMessage message)
    {
        BattleData battleData = currentBattleData;

        Debug.LogFormat("(OnBattlePreloaded) battle data: {0} Time: {1}", battleData, Time.time);

        List<Formation> themedList = formationDatabase.GetThemedList(battleData.theme);

        Selector selector = new Selector();

        Formation formation = selector.Single(themedList).Clone();

        formation.data.RemoveAll(x => x.key == "Character");

        if (battleData.score <= 0)
        {
            formation.Add("Character", EntityType.Character, new Vector2(1f, 1f));
        }
            

        EventManager.Publish(this, BattleEvent.OnBattleInitialize, formation);

    }

    private void OnCharacterDestroyed(IMessage message)
    {
        EventManager.Publish(this, BattleEvent.OnBattleFinish, null);
    }

    private void OnFormationDestroyed(IMessage message)
    {
        currentBattleData.score++;
        EventManager.Publish(this, BattleEvent.OnBattleFinish, currentBattleData);
    }

}

public class BattleData
{
    public Theme theme = Theme.Plains;
    public int score = 0;

    public BattleData(Theme theme, int score)
    {
        this.theme = theme;
        this.score = score;
    }
}