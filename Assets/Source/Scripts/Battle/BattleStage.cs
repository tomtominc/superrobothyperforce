﻿using System.Collections;
using Framework.Core;
using Framework.Core.EventSystem;
using UnityEngine;

public class BattleStage : Loader
{
    public override IEnumerator Load()
    {
        EventManager.Subscribe(BattleEvent.OnBattleInitialize, OnBattleInitialize);

        yield return null;
    }

    public void OnBattleInitialize(IMessage message)
    {
       
    }
}
