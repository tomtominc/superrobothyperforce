﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Framework.Core;

[System.Serializable]
public class EntityData
{
    public EntityData()
    {
    }

    public EntityData(string key, EntityType type, Vector2 point)
    {
        this.key = key;
        this.type = type;
        this.point = point;
    }

    public string key;
    public EntityType type;
    public Vector2 point;
}

public enum EntityType
{
    Character,
    Projectile,
    Bullet
}

public enum Theme
{
    Plains,
    Forest,
    Dungeon,
    Facility
}

[System.Serializable]
public class Formation : ISelectable
{
    [SerializeField]
    public Theme theme;

    [Range(0.1f, 100f)]
    public float selectionPercentage = 0.1f;

    public float GetSelectionWeight()
    {
        return selectionPercentage;
    }

    public float AdjustedSelectionWeight { get; set; }

    public List < EntityData > data;

    public Formation()
    {
        data = new List<EntityData>();
    }

    public Formation(Formation formation)
    {
        theme = formation.theme;
        selectionPercentage = formation.selectionPercentage;
        data = new List < EntityData >(formation.data);
    }

    public Formation Clone()
    {
        return new Formation(this);
    }

    public void Add(EntityData entity)
    {
        data.Add(entity);
    }

    public void Add(string key, EntityType type, Vector2 point)
    {
        EntityData entity = new EntityData(key, type, point);

        Add(entity);
    }
}
