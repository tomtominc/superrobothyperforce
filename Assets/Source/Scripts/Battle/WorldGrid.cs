﻿using UnityEngine;
using Gamelogic.Grids;
using Framework.Core.EventSystem;
using PathologicalGames;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;

public enum WorldAction
{
    SpawnEntity,
    RemoveEntity
}

[System.Serializable]
public class GridColorOptions
{
    public Rect gridSpace;
}

public class WorldGrid : GridBehaviour<RectPoint>, IWorld
{
    public Rect MaxGridSpace = new Rect(0f, 0f, 3f, 6f);

    public IGrid < Cell , RectPoint > _grid;

    public GridColorOptions characterBounds;
    public GridColorOptions enemyBounds;

    public Transform prefabAnchor;
    public List < Transform > prefabs;

    public Rect MaxGrid { get { return MaxGridSpace; } }

    protected Dictionary < string , Transform > _prefabs;

    private void Awake()
    {

        _prefabs = new Dictionary<string, Transform>();

        foreach (var prefab in prefabs)
            _prefabs.Add(prefab.name, prefab);

        EventManager.Subscribe(WorldAction.SpawnEntity, SpawnEntity);
    }

    public override void InitGrid()
    {
        _grid = Grid.CastValues<Cell,RectPoint>();

        _grid.Apply(x => x.Initialize());

        ResetBounds();
    }

    public RectPoint GetPoint(Vector2 point)
    {
        return new RectPoint((int)point.x, (int)point.y);
    }

    public Cell GetCellWorldPosition(Vector2 point)
    {
        RectPoint gridPoint = Map[point];
        return GetCell(gridPoint);
    }

    public Cell GetCell(RectPoint point)
    {
        if (_grid.Contains(point))
        {
            return _grid[point];
        }

        return null;
    }

    public Vector2 GetWorldPoint(Vector2 point)
    {
        Cell cell = GetCell(GetPoint(point));
        return cell == null ? Vector2.zero : (Vector2)cell.transform.position;
    }

    public Vector2 GetRandomWorldPoint(int min, int max)
    {
        RectPoint gridPoint = _grid.ToList()[UnityEngine.Random.Range(min, max)];
        return new Vector2(gridPoint.X, gridPoint.Y);
    }

    public Vector2 GetCoordinates(Vector2 point)
    {
        RectPoint gridPoint = Map[point];
        return new Vector2(gridPoint.X, gridPoint.Y);
    }

    public bool IsInWorldBounds(Vector2 point)
    {
        return (point.x >= MaxGridSpace.x && point.y >= MaxGridSpace.y && point.x < MaxGridSpace.width && point.y < MaxGridSpace.height);
    }

    public bool IsMoveValid(IEntity entity, Vector2 point)
    {
        Cell cell = GetCell(GetPoint(point));

        if (cell == null)
            return false;

        if (!cell.Occupied || cell.Contains(entity.Transform))
            return true;

        return false;
    }

    public Rect GetBounds(LayerMask layer)
    {
        if ((layer & LayerManager.CharacterLayer) != 0)
            return characterBounds.gridSpace;
        if ((layer & LayerManager.AutonomousLayer) != 0)
            return enemyBounds.gridSpace;

        return MaxGridSpace;
    }

    public void SetBounds(LayerMask layer, Rect rect)
    {
        if ((layer & LayerManager.CharacterLayer) != 0)
            characterBounds.gridSpace = rect;
        else
            enemyBounds.gridSpace = rect;

        ResetBounds();
    }

    public void ResetBounds()
    {
        foreach (var point in _grid)
        {
            Vector2 v2point = new Vector2(point.X, point.Y);

            if (characterBounds.gridSpace.Contains(v2point))
            {
                _grid[point].SetSprite("Character", "Normal", (int)v2point.y);
            }

            if (enemyBounds.gridSpace.Contains(v2point))
            {
                _grid[point].SetSprite("Enemy", "Normal", (int)v2point.y);
            }
           
        }
    }

    public bool Move(IEntity entity, Vector2 point, float duration, MoveTweenType moveTweenType,
                     Action<bool> onfinished = null)
    {
        Cell cell = GetCell(GetPoint(point));

        if (cell == null)
        {
            if (onfinished != null)
                onfinished.Invoke(false);
            
            return false;
        }

        if (duration <= 0f)
        {
            Cell lastCell = GetCellWorldPosition(entity.Transform.position);

            if (lastCell != null)
                lastCell.RemoveFromCell(entity.Transform);

            entity.SetPosition(cell.transform.position);
            cell.AddToCell(entity.Transform);

            if (onfinished != null)
                onfinished.Invoke(true);
        }
        else
        {
            TweenBehaviour tweenBehaviour = null;

            switch (moveTweenType)
            {
                case MoveTweenType.Linear: 
                    tweenBehaviour = new TweenLinearBehaviour(entity.Transform, cell.transform.position, duration, (currentPoint, lastPoint) =>
                        {
                            UpdateMovement(entity, moveTweenType, currentPoint, lastPoint);
                        }, 
                        () =>
                        {
                            CompleteMovement(entity, cell.transform.position, onfinished);
                        });
                    break;

                case MoveTweenType.SimulateHeight:

                    RemoveFromCell(entity, entity.Transform.position);

                    tweenBehaviour = new TweenSimulateHeightBehaviour(entity.Transform, cell.transform.position, duration, (currentPoint, lastPoint) =>
                        {
                            UpdateMovement(entity, moveTweenType, currentPoint, lastPoint);
                        }, 
                        () =>
                        {
                            CompleteMovement(entity, cell.transform.position, onfinished);
                        });
                    break;
                    
            }

            tweenBehaviour.DoStart();
        }

        return true;
    }

    private void UpdateMovement(IEntity entity, MoveTweenType moveTweenType, Vector3 position, Vector3 lastPosition)
    {
        if (moveTweenType == MoveTweenType.Linear)
            SetOccupiedCell(entity, position, lastPosition);
        
    }

    public void RemoveFromCell(IEntity entity, Vector3 point)
    {
        Cell cell = GetCellWorldPosition(point);
        if (cell)
            cell.RemoveFromCell(entity.Transform);
    }

    private void CompleteMovement(IEntity entity, Vector3 position, Action<bool> onfinished)
    {
        bool finished = entity.Transform.gameObject.activeSelf;

        if (!finished)
        {
            Cell lastCell = GetCellWorldPosition(entity.Transform.position);

            if (lastCell)
                lastCell.RemoveFromCell(entity.Transform);
        }
        else
        {
            entity.SetPosition(position);
        }

        onfinished.Invoke(finished);
    }

    public void SetTargetCell(Vector2 targetPoint, bool isTargeted)
    {
        Cell targetCell = GetCell(new RectPoint((int)targetPoint.x, (int)targetPoint.y));

        if (targetCell)
        {
            targetCell.IsTargeted = isTargeted;
        }
        else
        {
            Debug.LogWarningFormat("Could not find target cell at : {0}", targetPoint);
        }
            
    }

    public void SetDamagableCell(Vector2 targetPoint, bool isDamagable)
    {
        Cell targetCell = GetCellWorldPosition(targetPoint);

        if (targetCell)
            targetCell.IsDamagable = isDamagable;
    }

    public void SetOccupiedCell(IEntity entity, Vector2 targetPoint, Vector2 lastPoint)
    {
        Cell currentCell = GetCellWorldPosition(targetPoint);
        Cell lastCell = GetCellWorldPosition(lastPoint);

        if (currentCell == lastCell)
            return;

        if (lastCell != null)
            lastCell.RemoveFromCell(entity.Transform);
        
        if (currentCell != null)
            currentCell.AddToCell(entity.Transform);
    }

    public void RemoveFromWorld(Transform entity)
    {
        Cell lastCell = GetCellWorldPosition(entity.position);

        if (lastCell != null)
        {
            lastCell.RemoveFromCell(entity);
        }


        Destroy(entity.gameObject);
    }

    public IEntity AddToWorld(string type)
    {
        if (!_prefabs.ContainsKey(type))
        {
            Debug.LogWarningFormat("Prefabs do not contain key of type: {0}", type);
            return null;
        }

        Transform t = Instantiate < Transform >(_prefabs[type]);

        t.SetParent(prefabAnchor);

        return t.GetComponent < IEntity >();
    }

    private void SpawnEntity(IMessage message)
    {
        EntityData data = (EntityData)message.Data;

        SpawnEntity(data);
       
    }

    public IEntity SpawnEntity(EntityData data)
    {
        IEntity entity = AddToWorld(data.key);

        if (entity == null)
            return null;
        
        entity.Initialize(this, data.key);

        Move(entity, data.point, 0f, MoveTweenType.Linear);

        return entity;
    }
}
