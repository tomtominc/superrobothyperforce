﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    private static PrefabManager instance;

    public static PrefabManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < PrefabManager >();

            return instance;
        }
    }

    [SerializeField] private List < Transform > _prefabs;

    public Transform GetPrefab(string name)
    {
        return _prefabs.Find(x => x.name == name);
    }

    public CardView GetInstance(int gameId)
    {
        CardState cardState = GameManager.Instance.GetCardState(gameId);
        Transform cardPrefab = PrefabManager.Instance.GetPrefab(cardState.GetName());

        if (cardPrefab == null)
        {
            Debug.LogErrorFormat("Card Prefab '{0}' is null, please assign to 'GameController' Object.", cardState.GetName());
        }

        Transform cardInstance = Object.Instantiate < Transform >(cardPrefab);
        cardInstance.SetParent(transform, false);

        CardView cardView = cardInstance.GetComponent < CardView >();
        cardView.Initialize(cardState);

        GameManager.Instance.RegisterCardView(cardView);

        return cardView;
    }
}
