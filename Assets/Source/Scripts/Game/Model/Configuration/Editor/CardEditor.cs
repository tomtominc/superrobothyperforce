﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditorInternal;
using UnityEditor;
using Framework.Core;
using CCGKit;
using System.Linq;

partial class ConfigurationEditor
{
    // move these variables to card editor
    private ReorderableList currentCardList;
    private ReorderableList currentAbilities;

    private List < Card > currentlyFilteredList = new List < Card >();

    private Card currentCard;
    private Ability currentAbility;

    // all the search parameters
    private CardSet currentCardSet;
    private CardType currentCartridgeType;
    private Rarity currentCardRarity;
    private ClassSet currentClassSet;
    private Subset currentSubset;

    private Vector2 cardCollectionScrollPos;

    private AbilityEditor abilityEditor = new AbilityEditor();

    private void InitCardCollectionEditor()
    {
        currentlyFilteredList = DoFilteredSearch(config, currentCardSet, currentCardRarity, currentClassSet, currentSubset);

        currentCardList = EditorUtils.SetupReorderableList("Card", currentlyFilteredList, 
            ref currentCard, (rect, x) =>
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight), 
                    string.Format("{0} [{1}]", x.Name, x.CardType)); 
            },
            (x) =>
            {
                currentCard = x;
                abilityEditor.Clear();
                abilityEditor.CreateAbilityList(currentCard);
            },
            () =>
            {
                var menu = new GenericMenu();
                foreach (var cartridgeType in Enum<CardType>.ToValues ())
                    menu.AddItem(new GUIContent(cartridgeType.ToString()), false, 
                        CreateCardCallback, cartridgeType);
                menu.ShowAsContext();
            },
            (x) =>
            {
                currentCard = null;
                abilityEditor.Clear();
            }
        );
    }

    private void CreateCardCallback(object obj)
    {
        Card cartridge = new Card();

        if (config.CardCollection.Count > 0)
        {
            var higherIdCard = config.CardCollection.Aggregate((x, y) => x.Id > y.Id ? x : y);
            if (higherIdCard != null)
                cartridge.Id = higherIdCard.Id + 1;
        }
        else
        {
            cartridge.Id = 0;
        }

        cartridge.Name = "None";
        cartridge.CardSet = currentCardSet;
        cartridge.CardType = (CardType)obj;

        config.CardCollection.Add(cartridge);

        InitCardCollectionEditor();
    }


    private void DrawCardCollectionEditor()
    {
        var oldLabelWidth = EditorGUIUtility.labelWidth;

        EditorGUIUtility.labelWidth = 80;

        var oldCardSet = currentCardSet;
        var oldCardRarity = currentCardRarity;
        var oldClassSet = currentClassSet;
        var oldSubset = currentSubset;

        EditorGUIExtensions.DrawHeader("Filter Group", GUILayout.ExpandWidth(true));
        EditorGUIExtensions.BeginContent(Color.white);
        GUILayout.BeginHorizontal();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.PrefixLabel("Card Set");
        currentCardSet = (CardSet)EditorGUILayout.EnumPopup(currentCardSet);
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.PrefixLabel("Rarity");
        currentCardRarity = (Rarity)EditorGUILayout.EnumPopup(currentCardRarity);
        EditorGUILayout.EndVertical();


        EditorGUILayout.BeginVertical();
        EditorGUILayout.PrefixLabel("Class Set");
        currentClassSet = (ClassSet)EditorGUILayout.EnumPopup(currentClassSet);
        EditorGUILayout.EndVertical();


        EditorGUILayout.BeginVertical();
        EditorGUILayout.PrefixLabel("Subset");
        currentSubset = (Subset)EditorGUILayout.EnumPopup(currentSubset);
        EditorGUILayout.EndVertical();

        GUILayout.EndHorizontal();

        EditorGUIExtensions.EndContent();

        if (oldCardSet != currentCardSet)
            InitCardCollectionEditor();
        else if (oldCardRarity != currentCardRarity)
            InitCardCollectionEditor();
        else if (oldClassSet != currentClassSet)
            InitCardCollectionEditor();
        else if (currentSubset != currentSubset)
            InitCardCollectionEditor();

        EditorGUILayout.Space();

        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical(GUILayout.MaxWidth(150));

        if (currentCardList != null)
        {
            GUILayout.BeginVertical(GUILayout.MaxWidth(250));
            cardCollectionScrollPos = GUILayout.BeginScrollView(cardCollectionScrollPos, false, false, GUILayout.Width(250), GUILayout.Height(600));
            if (currentCardList != null)
                currentCardList.DoLayoutList();
            GUILayout.EndScrollView();
            GUILayout.EndVertical();
        }
        else
        {
            Debug.LogWarning("Current Card List is null");
        }

        GUILayout.EndVertical();

        EditorGUIUtility.labelWidth = oldLabelWidth;

        if (currentCard != null)
            DrawCard(currentCard);
       
        GUILayout.EndHorizontal();
       
    }

    public static Color CardColor(CardType type)
    {
        switch (type)
        {
            case CardType.Building:
                return EditorColor.orange;
            case CardType.Pet:
                return EditorColor.red;
            case CardType.Spell:
                return EditorColor.green;
            case CardType.Trick:
                return EditorColor.purple;
            case CardType.Weapon:
                return EditorColor.blue;
            case CardType.Hero:
                return EditorColor.bluelight;
            case CardType.Tile:
                return EditorColor.bluegreen;
            case CardType.Body:
                return EditorColor.yelloworange;
            case CardType.Cannon:
                return EditorColor.redlight;
            default:
                return Color.white;
        }
    }

    private void DrawCard(Card card)
    {
        GUILayout.BeginVertical();

        EditorGUIUtility.labelWidth = 60;

        EditorGUIExtensions.DrawHeader(string.Format("{0} [{1}]", card.Name, card.Id), CardColor(card.CardType), GUILayout.MaxWidth(210));

        EditorGUIExtensions.BeginContent(Color.white, GUILayout.MaxWidth(260));

        EditorGUILayout.Space();
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Name");
        card.Name = EditorGUILayout.TextField(card.Name, GUILayout.MaxWidth(150));
        EditorGUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Type");
        card.CardType = (CardType)EditorGUILayout.EnumPopup(card.CardType, GUILayout.MaxWidth(150));
        EditorGUILayout.EndHorizontal();

        if (card.CardType == CardType.Pet || card.CardType == CardType.Weapon || card.CardType == CardType.Building)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Weapon Type");
            card.WeaponType = (WeaponType)EditorGUILayout.EnumPopup(card.WeaponType, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();
        }

       
        if (card.CardType == CardType.Hero || card.CardType == CardType.Pet || card.CardType == CardType.Building || card.CardType == CardType.Weapon)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Health");
            card.Health = EditorGUILayout.IntField(card.Health, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Power");
            card.Power = EditorGUILayout.IntField(card.Power, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Element");
        card.Element = (Element)EditorGUILayout.EnumPopup(card.Element, GUILayout.MaxWidth(150));
        EditorGUILayout.EndHorizontal();

        if (card.CardType != CardType.Tile)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Rarity");
            card.Rarity = (Rarity)EditorGUILayout.EnumPopup(card.Rarity, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Card Set");
            card.CardSet = (CardSet)EditorGUILayout.EnumPopup(card.CardSet, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Class Set");
            card.ClassSet = (ClassSet)EditorGUILayout.EnumPopup(card.ClassSet, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Subset");
            card.Subset = (Subset)EditorGUILayout.EnumPopup(card.Subset, GUILayout.MaxWidth(150));
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Description");
        EditorStyles.textField.wordWrap = true;
        card.Description = EditorGUILayout.TextArea(card.Description, GUILayout.MaxWidth(200),
            GUILayout.MaxHeight(100));
        EditorGUILayout.EndHorizontal();

        EditorGUIExtensions.EndContent();

        DrawAbility(currentCard);

        GUILayout.EndVertical();


    }

    private void DrawAbility(Card cartridge)
    {
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical(GUILayout.MaxWidth(250));
        abilityEditor.DrawAbilityList();
        GUILayout.EndVertical();
        //GUILayout.BeginVertical(GUILayout.MaxWidth(380));
        abilityEditor.DrawCurrentAbility();
        //GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }

    private List < Card > DoFilteredSearch(Configuration configuration, CardSet cardSet, Rarity rarity, 
                                           ClassSet classSet, Subset subset)
    {
        return configuration.CardCollection.Where(x => x.CardSet == cardSet)
            .Where(x => x.Rarity == rarity || rarity == Rarity.All)
            .Where(x => x.ClassSet == classSet || classSet == ClassSet.All)
            .Where(x => x.Subset == subset || subset == Subset.All).ToList();
    }

}
