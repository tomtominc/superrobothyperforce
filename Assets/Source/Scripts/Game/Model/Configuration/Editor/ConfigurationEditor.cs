﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Framework.Core;
using UnityEditorInternal;
using System.Linq;
using CCGKit;

public partial class ConfigurationEditor : EditorWindow
{
    private Configuration config;
    private bool isLoading = false;
    private string loadMessage = string.Empty;

    [MenuItem("Window/Config Editor")]
    private static void Init()
    {
        var window = GetWindow(typeof(ConfigurationEditor));
        window.titleContent = new GUIContent("Config Editor");
    }

    private void OnEnable()
    {
        config = new Configuration();
        loadMessage = "Loading Configuration Settings";

        currentCardSet = CardSet.Classic;
        currentCardRarity = Rarity.All;
        currentClassSet = ClassSet.All;
        currentSubset = Subset.All;

        config.LoadGameConfiguration();

        InitCardCollectionEditor();
    }

    private void OnConfigurationLoaded()
    {
        isLoading = false;

        InitCardCollectionEditor();
    }

    private void OnGUI()
    {
        GUI.enabled = !isLoading;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Load", GUILayout.MaxWidth(60)))
        {
            config.LoadGameConfiguration();
        }


        if (GUILayout.Button("Save", GUILayout.MaxWidth(60)))
        {
            config.SaveGameConfiguration();
        }
       
        if (GUILayout.Button("Upload", GUILayout.MaxWidth(60)))
        {
            isLoading = true; 
            loadMessage = "Uploading Configuration Settings to Server";
            config.UploadGameConfiguration(null, OnConfigurationLoaded);
        }

        if (GUILayout.Button("Download", GUILayout.MaxWidth(60)))
        {
            isLoading = true; 
            loadMessage = "Downloading Configuration Settings from Server";
            config.DownloadGameConfiguration(null, OnConfigurationLoaded);
        }



        GUILayout.EndHorizontal();

        GUILayout.Space(5);

        if (config == null)
            return;

        DrawCardCollectionEditor();
    }

   
}
