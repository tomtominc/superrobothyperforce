﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Core
{
    public class PlayerState
    {
        public int Id;
        public int NetId;
        public int ConnectionId;
        public bool IsConnected;
        public string Name;
        public string CharacterName;
        public bool IsHuman;
        public int MaxHealth;
        public int Health;
        public int MaxOil = 10;
        public int Oil = 1;
        public Deck Deck;
        public int LastCardPlayedId = -1;

        public PlayerState(int id)
        {
            Id = id;
        }

        public void Draw()
        {
            //int cardId = Deck.DrawCard();
            Debug.LogFormat("Player {0} is drawing a card.", Id);
        }

        public void SetHealth(int health)
        {
            
        }

        public void SetOil(int oil)
        {
            
        }

        public void Heal(int amount)
        {
            
        }

        public void Damage(int amount)
        {
            
        }

        public void IncreaseMaxHealth(int amount)
        {
            
        }

        public void DecreaseMaxHealth(int amount)
        {
            
        }

        public void IncreaseOil(int amount)
        {
            
        }

        public void DecreaseOil(int amount)
        {
            
        }
    }
}

