﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Framework.Core
{
    public sealed class Deck
    {
        public int ownerId;

        public Card hero;
        public Card cannon;
        public Card body;

        public Dictionary < int , int > cards;

        public int Count()
        {
            int count = 0;

            foreach (var card in cards)
                count += card.Value;

            return count;
        }

        public int DrawCard()
        {
            int selectedKey = cards.Where(x => x.Value > 0).ElementAt(Random.Range(0, cards.Count)).Key;

            cards[selectedKey] -= 1;

            return selectedKey;
        }
    }
}
