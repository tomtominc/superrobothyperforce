﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AbilityUtils
{
    public static AbilityAction GetActionByType(AbilityActionType type)
    {
        switch (type)
        {
            case AbilityActionType.Damage: 
                return new DamageAbilityAction();

            case AbilityActionType.Destroy: 
                return new DestroyAbilityAction();

            case AbilityActionType.Draw: 
                return new DrawAbilityAction();

            case AbilityActionType.GainArmor: 
                return new GainArmorAbilityAction();

            case AbilityActionType.Enchant: 
                return new EnchantAction();

            case AbilityActionType.GiveHealthAndPower: 
                return new GiveHealthAndPowerAbilityAction();

            case AbilityActionType.Heal: 
                return new HealAbilityAction();

            case AbilityActionType.Recoil: 
                return new RecoilAbilityAction();

            case AbilityActionType.Taunt: 
                return new TauntAbilityAction();

            case AbilityActionType.Break:
                return new BreakAbilityAction();

            case AbilityActionType.Terraform:
                return new TerraformAbilityAction();
        }

        return new AbilityAction();
    }
}
