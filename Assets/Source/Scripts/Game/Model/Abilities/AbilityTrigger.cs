﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityTrigger
{
    WhenThisCardIsPlayed,
    WhenAnyCardPlayed,
    WhenThisCardDamaged,
    WhenAnyCardDamaged,
    WhenThisPlayerDraws,
    WhenAnyPlayerDraws,
    WhenThisCardMoves,
    WhenAnyCardMoves,
    WhenThisCardIsDestroyed,
    WhenAnyCardIsDestroyed,
    WhenAnyTileTerraformed,
    WhenThisCardAttacks,
    WhenAnyCardAttacks,
    None,
}
