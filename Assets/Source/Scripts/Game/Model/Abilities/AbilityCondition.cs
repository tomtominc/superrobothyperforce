﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityCondition
{
    public AbilityConditionType ConditionType;
}

public class HealthFullCondition : AbilityCondition
{
    public HealthFullCondition()
    {
        ConditionType = AbilityConditionType.HealthFull;
    }
}

public class HealthHigherThanCondition : AbilityCondition
{
    public int healthValue = 0;

    public HealthHigherThanCondition()
    {
        ConditionType = AbilityConditionType.HealthHigherThan;
    }
}

public class HealthLowerThanCondition : AbilityCondition
{
    public int healthValue = 0;

    public HealthLowerThanCondition()
    {
        ConditionType = AbilityConditionType.HealthLowerThan;
    }
}

public enum AbilityConditionType
{
    HealthFull,
    HealthLowerThan,
    HealthHigherThan
}
