﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AbilityAction
{
    /// <summary>
    /// The type of the action.
    /// </summary>
    public AbilityActionType ActionType;
}

public class HealAbilityAction : AbilityAction
{
    public int healAmount = 0;

    public HealAbilityAction()
    {
        healAmount = 20;
        ActionType = AbilityActionType.Heal;
    }
}

public class DestroyAbilityAction : AbilityAction
{

    public DestroyAbilityAction()
    {
        ActionType = AbilityActionType.Destroy;
    }
}

public class EnchantAction : AbilityAction
{
    public Ability ability;

    public EnchantAction()
    {
        ActionType = AbilityActionType.Enchant;
    }
}

public class TauntAbilityAction : AbilityAction
{

    public TauntAbilityAction()
    {
        ActionType = AbilityActionType.Taunt;
    }
}

public class DrawAbilityAction : AbilityAction
{
    public int numberOfCards;

    public DrawAbilityAction()
    {
        ActionType = AbilityActionType.Draw;
    }
}

public class DamageAbilityAction : AbilityAction
{
    public DamageInfo damageInfo;

    public DamageAbilityAction()
    {
        damageInfo = new DamageInfo();
        ActionType = AbilityActionType.Damage;
    }
}

public class GainArmorAbilityAction : AbilityAction
{
    public int armorAmount;

    public GainArmorAbilityAction()
    {
        ActionType = AbilityActionType.GainArmor;
    }
}

public class GiveHealthAndPowerAbilityAction : AbilityAction
{
    public int healthAmount;
    public int powerAmount;

    public GiveHealthAndPowerAbilityAction()
    {
        ActionType = AbilityActionType.GiveHealthAndPower;
    }
}

public class RecoilAbilityAction : AbilityAction
{
    public DamageInfo damageInfo;

    public RecoilAbilityAction()
    {
        damageInfo = new DamageInfo();
        ActionType = AbilityActionType.Recoil;
    }
}

public class BreakAbilityAction : AbilityAction
{
    public BreakAbilityAction()
    {
        ActionType = AbilityActionType.Break;
    }
}

public class TerraformAbilityAction : AbilityAction
{
    public TileType tileType;

    public TerraformAbilityAction()
    {
        ActionType = AbilityActionType.Terraform;
    }
}



public enum AbilityActionType
{
    Heal,
    Destroy,
    Enchant,
    Taunt,
    Draw,
    Damage,
    GainArmor,
    GiveHealthAndPower,
    Recoil,
    Break,
    Terraform
}
