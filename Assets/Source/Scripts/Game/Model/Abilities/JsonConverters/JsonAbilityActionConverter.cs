﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json.Linq;

public class JsonAbilityActionConverter : JsonCreationConverter<AbilityAction>
{
    protected override AbilityAction Create(Type objectType, JObject jsonObject)
    {
        AbilityActionType type = jsonObject["ActionType"].ToObject<AbilityActionType>();
        return AbilityUtils.GetActionByType(type);
    }
}
