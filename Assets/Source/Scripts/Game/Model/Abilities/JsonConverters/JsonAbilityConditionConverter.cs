﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public class JsonAbilityConditionConverter : JsonCreationConverter<AbilityCondition>
{
    protected override AbilityCondition Create(Type objectType, JObject jsonObject)
    {
        AbilityCondition condition = new AbilityCondition();

        AbilityConditionType type = jsonObject["ConditionType"].ToObject<AbilityConditionType>();

        switch (type)
        {
            case AbilityConditionType.HealthFull:
                condition = new HealthFullCondition();
                break;

            case AbilityConditionType.HealthHigherThan:
                condition = new HealthHigherThanCondition();
                ((HealthHigherThanCondition)condition).healthValue = jsonObject["healthValue"].ToObject<int>();

                break;

            case AbilityConditionType.HealthLowerThan:
                condition = new HealthLowerThanCondition();
                ((HealthLowerThanCondition)condition).healthValue = jsonObject["healthValue"].ToObject<int>();
                break;

        }

        return condition;
    }
}
