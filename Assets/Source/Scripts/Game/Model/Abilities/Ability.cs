﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability
{
    /// <summary>
    /// how long does the ability last?
    /// e.g. Taunt last permenently
    /// but Battlecry is a one and done.
    /// </summary>
    public AbilityType Type;

    /// <summary>
    /// How many turns to do ability
    /// if Type = AbilityType.ForTurns
    /// </summary>
    public int Turns = 1;

    /// <summary>
    /// The actions when 
    /// this ability is activated
    /// e.g. Destroy
    /// </summary>
    public AbilityAction Action;
    
    /// <summary>
    /// when does this ability trigger?
    /// e.g. battlecry triggers when the card enters
    /// the field.
    /// </summary>
    public AbilityTrigger Trigger;

    /// <summary>
    /// The target of this action. 
    /// e.g. your opponents weapon
    /// </summary>
    public TargetFilter TargetFilter;

    /// <summary>
    /// the trigger conditions
    /// </summary>
    public List < AbilityCondition > TriggerConditions;

    /// <summary>
    /// The target conditions.
    /// </summary>
    public List < AbilityCondition > TargetConditions;

}

public enum AbilityType
{
    PermanentEffect,
    TriggeredEffect,
}
