﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFilter
{
    public AbilityTargetType TargetType;
    public RandomType RandomType;
    public CardType CardType;
}

public enum RandomType
{
    None,
    Random,
    RandomSplit
}

public enum AbilityTargetType
{
    Any,
    Opponent,
    Self
}
