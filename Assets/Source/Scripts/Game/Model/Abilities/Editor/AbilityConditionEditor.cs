﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ability condition editor
using UnityEditorInternal;
using CCGKit;
using UnityEditor;
using Framework.Core;
using DotLiquid;

partial class AbilityEditor
{
    private ReorderableList triggerConditions;
    private ReorderableList targetConditions;

    private AbilityCondition currentTriggerCondition;
    private AbilityCondition currentTargetCondition;

    private void CreateTriggerConditions()
    {
        if (currentAbility.TriggerConditions == null)
            currentAbility.TriggerConditions = new List<AbilityCondition>();

        triggerConditions = EditorUtils.SetupReorderableList("Trigger Conditions", currentAbility.TriggerConditions,
            ref currentTriggerCondition,
            (rect, x) =>
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                    x.ConditionType.ToString().SplitCamelCase());
            },
            (x) =>
            {
                currentTriggerCondition = x;
            },
            () =>
            {
                var menu = new GenericMenu();
                foreach (var condition in Enum<AbilityConditionType>.ToValues ())
                    menu.AddItem(new GUIContent(condition.ToString()), false, CreateTriggerCallback, condition);
                menu.ShowAsContext();
            },
            (x) =>
            {
                currentTriggerCondition = null;
            }

        );
    }

    private void CreateTriggerCallback(object value)
    {
        AbilityConditionType type = (AbilityConditionType)value;

        currentAbility.TriggerConditions.Add(GetCondition(type));

    }

    private void DrawCurrentTriggerCondition()
    {
        // draw the properties from the trigger condition

        currentTriggerCondition = DrawCondition(currentTriggerCondition);
    }

    private void CreateTargetConditions()
    {
        if (currentAbility.TargetConditions == null)
            currentAbility.TargetConditions = new List<AbilityCondition>();
        
        targetConditions = EditorUtils.SetupReorderableList("Trigger Conditions", currentAbility.TargetConditions,

            ref currentTargetCondition,
            (rect, x) =>
            {
                EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                    x.ConditionType.ToString().SplitCamelCase());
            },
            (x) =>
            {
                currentTargetCondition = x;
            },
            () =>
            {
                var menu = new GenericMenu();
                foreach (var condition in Enum<AbilityConditionType>.ToValues ())
                    menu.AddItem(new GUIContent(condition.ToString()), false, CreateTargetCallback, condition);
                menu.ShowAsContext();
            },
            (x) =>
            {
                currentTargetCondition = null;
            }

        );
    }

    private void CreateTargetCallback(object value)
    {
        AbilityConditionType type = (AbilityConditionType)value;

        currentAbility.TargetConditions.Add(GetCondition(type));

    }

    private void DrawCurrentTargetCondition()
    {
        // draw the current target condition
        currentTargetCondition = DrawCondition(currentTargetCondition);
    }

    private AbilityCondition DrawCondition(AbilityCondition condition)
    {
        if (condition == null)
            return null;

        switch (condition.ConditionType)
        {
            case AbilityConditionType.HealthFull:
                break;

            case AbilityConditionType.HealthHigherThan:
                ((HealthHigherThanCondition)condition).healthValue = 
                    EditorGUILayout.IntField("Health Value", ((HealthHigherThanCondition)condition).healthValue);
                break;

            case AbilityConditionType.HealthLowerThan:
                ((HealthLowerThanCondition)condition).healthValue = 
                    EditorGUILayout.IntField("Health Value", ((HealthLowerThanCondition)condition).healthValue);
                break;
        }

        return condition;
    }

    private AbilityCondition GetCondition(AbilityConditionType type)
    {
        AbilityCondition condition = new HealthFullCondition();

        switch (type)
        {
            case AbilityConditionType.HealthFull: 
                condition = new HealthFullCondition();
                break;
            case AbilityConditionType.HealthHigherThan:
                condition = new HealthHigherThanCondition();
                break;

            case AbilityConditionType.HealthLowerThan:
                condition = new HealthLowerThanCondition();
                break;
        }

        return condition;
    }

    private void DrawAbilityTriggerConditions()
    {
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical(GUILayout.MaxWidth(380));
        if (triggerConditions != null)
            triggerConditions.DoLayoutList();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }

    private void DrawAbilityTargetConditions()
    {
        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical(GUILayout.MaxWidth(380));
        if (targetConditions != null)
            targetConditions.DoLayoutList();
        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
    }
}
