﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ability action editor
using UnityEditor;
using System;
using Framework.Core;

partial class AbilityEditor
{

    private void CreateActionCallback(object data)
    {
        AbilityActionType type = (AbilityActionType)data;
        Ability ability = new Ability();

        ability.Action = AbilityUtils.GetActionByType(type);

        currentCartridge.Abilities.Add(ability);
    }

    private void DrawActionProperties()
    {
        switch (currentAbility.Action.ActionType)
        {
            case AbilityActionType.Heal:
                ((HealAbilityAction)currentAbility.Action).healAmount = 
                    DrawInt("Heal Amount", ((HealAbilityAction)currentAbility.Action).healAmount);
                break;

            case AbilityActionType.Damage:
                ((DamageAbilityAction)currentAbility.Action).damageInfo = 
                    DrawDamageInfo(((DamageAbilityAction)currentAbility.Action).damageInfo);
                break;   

            case AbilityActionType.Draw:
                ((DrawAbilityAction)currentAbility.Action).numberOfCards = 
                    DrawInt("Number of Cards", ((DrawAbilityAction)currentAbility.Action).numberOfCards);
                break;

            case AbilityActionType.Terraform:
                ((TerraformAbilityAction)currentAbility.Action).tileType =
                    DrawEnum <TileType>("Tile Type", ((TerraformAbilityAction)currentAbility.Action).tileType);
                break;

            case AbilityActionType.Recoil:
                ((RecoilAbilityAction)currentAbility.Action).damageInfo =
                    DrawDamageInfo(((RecoilAbilityAction)currentAbility.Action).damageInfo);
                break;
        }
    }

    private int DrawInt(string label, int value)
    {
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel(label);
        value = EditorGUILayout.IntField(value, GUILayout.MaxWidth(150));
        GUILayout.EndHorizontal();

        return value;
    }

    private T DrawEnum<T>(string label, T value) where T : struct, IConvertible
    {
        int eValue = value.ToInt<T>();
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel(label);
        eValue = EditorGUILayout.Popup(eValue, EnumExtensions.ToList<T>().ToArray(), GUILayout.MaxWidth(150));
        GUILayout.EndHorizontal();
        value = EnumExtensions.ToValues < T >()[eValue];

        return value;
    }

    private DamageInfo DrawDamageInfo(DamageInfo info)
    {
        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Damage Amount");
        info.amount = EditorGUILayout.IntField(info.amount, GUILayout.MaxWidth(150));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Damage Element");
        info.element = (Element)EditorGUILayout.EnumPopup(info.element, GUILayout.MaxWidth(150));
        GUILayout.EndHorizontal();

        return info;
    }

   
}
