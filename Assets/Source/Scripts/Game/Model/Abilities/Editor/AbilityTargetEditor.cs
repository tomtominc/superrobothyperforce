﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ability target editor
using UnityEditor;


partial class AbilityEditor
{
    private void DrawAbilityTarget()
    {
        GUILayout.BeginHorizontal();

        EditorGUILayout.PrefixLabel("Target");

        if (currentAbility.TargetFilter == null)
            currentAbility.TargetFilter = new TargetFilter();

        currentAbility.TargetFilter.TargetType = (AbilityTargetType)EditorGUILayout.EnumPopup
            (currentAbility.TargetFilter.TargetType, GUILayout.MaxWidth(150));

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        EditorGUILayout.PrefixLabel("Random");

        currentAbility.TargetFilter.RandomType = (RandomType)EditorGUILayout.EnumPopup
            (currentAbility.TargetFilter.RandomType, GUILayout.MaxWidth(150));

        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();

        EditorGUILayout.PrefixLabel("Cartridge");

        currentAbility.TargetFilter.CardType = (CardType)EditorGUILayout.EnumPopup
            (currentAbility.TargetFilter.CardType, GUILayout.MaxWidth(150));

        GUILayout.EndHorizontal();
    }
}
