﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core;
using UnityEditorInternal;
using CCGKit;
using UnityEditor;


public partial class AbilityEditor
{
    private Configuration config;
    private ReorderableList abilityList;

    private Ability currentAbility;

    private List < string > cartridgeNames = new List < string >();

    private Card currentCartridge;

    public void LoadEditorData(Configuration config)
    {
        this.config = config;

        foreach (var cartridge in config.CardCollection)
        {
            cartridgeNames.Add(cartridge.Name);
        }
    }

    public void CreateAbilityList(Card cartridge)
    {
        currentCartridge = cartridge;

        abilityList = EditorUtils.SetupReorderableList("Abilities", cartridge.Abilities, ref currentAbility, (rect, x) =>
            {
                AbilityAction action = x.Action;

                EditorGUI.LabelField(new Rect(rect.x, rect.y, 200, EditorGUIUtility.singleLineHeight),
                    x.Action.ActionType.ToString().SplitCamelCase());
            },
            (x) =>
            {
                currentAbility = x;

                CreateTriggerConditions();
                CreateTargetConditions();
            },
            () =>
            {
                var menu = new GenericMenu();

                foreach (var actionType in Enum<AbilityActionType>.ToValues ())
                    menu.AddItem(new GUIContent(actionType.ToString()), false, CreateActionCallback, actionType);

                menu.ShowAsContext();
            },
            (x) =>
            {
                currentAbility = null;
            }

        );
    }

    public void Clear()
    {
        currentCartridge = null;
        currentAbility = null;
    }

    public void DrawAbilityList()
    {
        if (currentCartridge == null || abilityList == null)
            return;

        abilityList.DoLayoutList();
    }

    public void DrawCurrentAbility()
    {
        if (currentAbility == null)
            return;
        
        GUIStyle style = new GUIStyle();
        style.richText = true;

        var oldLabelWidth = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 100;

        GUILayout.BeginVertical(GUILayout.MaxWidth(380));

        GUILayout.BeginHorizontal();
        GUILayout.Label(currentAbility.Action.ActionType.ToString().SplitCamelCase(), EditorStyles.boldLabel);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        EditorGUILayout.PrefixLabel("Ability Type");

        currentAbility.Type = (AbilityType)EditorGUILayout.EnumPopup
            (currentAbility.Type, GUILayout.MaxWidth(150));

        GUILayout.EndHorizontal();

        DrawActionProperties();

        // Draw the trigger
        if (currentAbility.Type != AbilityType.PermanentEffect)
        {
            DrawAbilityTarget();

            GUILayout.BeginHorizontal();

            EditorGUILayout.PrefixLabel("Ability Trigger");

            currentAbility.Trigger = (AbilityTrigger)EditorGUILayout.EnumPopup
                (currentAbility.Trigger, GUILayout.MaxWidth(150));

            GUILayout.EndHorizontal();

            DrawAbilityTriggerConditions();

            DrawCurrentTriggerCondition();

            DrawAbilityTargetConditions();

            DrawCurrentTargetCondition();
        }

        GUILayout.EndVertical();

        EditorGUIUtility.labelWidth = oldLabelWidth;
    }
}
