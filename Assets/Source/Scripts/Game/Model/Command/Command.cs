﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Command
{
    public int boardId;
    public int gameId;

    public CommandType Type;
    public AbilityTrigger Trigger;
    public AbilityTrigger GlobalTrigger;
}

public class SummonCommand : Command
{
    public bool fromHand = false;

    public SummonCommand()
    {
        Type = CommandType.Summon;
        Trigger = AbilityTrigger.WhenThisCardIsPlayed;
        GlobalTrigger = AbilityTrigger.WhenAnyCardPlayed;
    }
}

public class DamageCommand : Command
{
    public DamageInfo damageInfo;
    public int damagerGameId;

    public DamageCommand()
    {
        Type = CommandType.Damage;
        Trigger = AbilityTrigger.WhenThisCardDamaged;
        GlobalTrigger = AbilityTrigger.WhenAnyCardDamaged;
    }
}

public class DrawCommand : Command
{
    public int targetPlayerId;
    public int numberOfCards;

    public DrawCommand()
    {
        Type = CommandType.Draw;
        Trigger = AbilityTrigger.WhenThisPlayerDraws;
        GlobalTrigger = AbilityTrigger.WhenAnyPlayerDraws;
    }
}

public class MoveCommand : Command
{
    public int moveToBoardId;

    public MoveCommand()
    {
        Type = CommandType.Move;
        Trigger = AbilityTrigger.WhenThisCardMoves;
        GlobalTrigger = AbilityTrigger.WhenAnyCardMoves;
    }
}

public class DamageEffectCommand : Command
{
    public DamageEffectCommand()
    {
        Type = CommandType.DamageEffect;
        Trigger = AbilityTrigger.None;
        GlobalTrigger = AbilityTrigger.None;
    }
}

public class CastCommand : Command
{
    public CastCommand()
    {
        Type = CommandType.Cast;
        Trigger = AbilityTrigger.WhenThisCardIsPlayed;
        GlobalTrigger = AbilityTrigger.WhenAnyCardPlayed;
    }
}

public class AttachCommand : Command
{
    public int attachToId;

    public AttachCommand()
    {
        Type = CommandType.Attach;
        Trigger = AbilityTrigger.WhenThisCardIsPlayed;
        GlobalTrigger = AbilityTrigger.WhenAnyCardPlayed;
    }
}

public class DestroyAttachmentCommand : Command
{
    public int attachToId;

    public DestroyAttachmentCommand()
    {
        Type = CommandType.DestroyAttachment;
        Trigger = AbilityTrigger.WhenThisCardIsDestroyed;
        GlobalTrigger = AbilityTrigger.WhenAnyCardIsDestroyed;
    }
}

public class TerraformCommand : Command
{
    public List < int > boardIds;
    public List < int > terraformIds;

    public TerraformCommand()
    {
        boardIds = new List<int>();
        terraformIds = new List < int >();
        Type = CommandType.Terraform;
        Trigger = AbilityTrigger.None; 
        GlobalTrigger = AbilityTrigger.WhenAnyTileTerraformed;
    }
}

public class MoveProjectileCommand : Command
{
    public MoveProjectileCommand()
    {
        Type = CommandType.MoveProjectile;
        Trigger = AbilityTrigger.None; 
        GlobalTrigger = AbilityTrigger.None;
    }
}

public class AttackBeginCommand : Command
{
    public AttackBeginCommand()
    {
        Type = CommandType.AttackBegin;
        Trigger = AbilityTrigger.WhenThisCardAttacks;
        GlobalTrigger = AbilityTrigger.WhenAnyCardAttacks;
    }
}

public class AttackEndCommand : Command
{
    public AttackEndCommand()
    {
        Type = CommandType.AttackEnd;
        Trigger = AbilityTrigger.None;
        GlobalTrigger = AbilityTrigger.None;
    }
}

public enum PlayerAction
{
    PlayCard,
    MoveCard,
    Attack,
    HeroPower,
    EndTurn,
    Emote
}

public enum GameAction
{
    CreateCard,
}

public enum CommandType
{
    Summon,
    Battlecry,
    Damage,
    GiveHealth,
    Draw,
    Move,
    Cast,
    Attach,
    DestroyAttachment,
    Terraform,
    AttackBegin,
    AttackEnd,
    MoveProjectile,
    DamageEffect,
    Destroy
}