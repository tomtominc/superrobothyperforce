﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Framework.Core;

public class CommandSystem
{
    public Queue < Command > currentCommands;
    public BoardState currentBoardState;

    public CommandSystem()
    {
        currentBoardState = Board.Instance.GetCurrentBoardState();
        currentCommands = new Queue < Command >();
    }

    #region PLAYER COMMAND ACTIONS -- Commands Initiated By the Player

    public void ExecutePlayerAction(CardState cardState, int targetBoardId, PlayerAction playerAction)
    {
        switch (playerAction)
        {
            case PlayerAction.PlayCard:
                PlayCard(cardState, targetBoardId);
                break;

            case PlayerAction.MoveCard:
                MoveCard(cardState, targetBoardId);
                break;

            case PlayerAction.Attack:
                Attack(cardState, targetBoardId);
                break;

            case PlayerAction.HeroPower:
                HeroPower(cardState, targetBoardId);
                break;

            case PlayerAction.EndTurn:
                EndTurn();
                break;
        }
    }

    public void PlayCard(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        if (cardState.Type == CardType.Hero || cardState.Type == CardType.Pet || cardState.Type == CardType.Building)
        {
            commands = CommandFactory.Summon(cardState, targetBoardId, true);
        }
        else if (cardState.Type == CardType.Spell || cardState.Type == CardType.Trick)
        {
            commands = CommandFactory.Cast(cardState, targetBoardId);
        }
        else if (cardState.Type == CardType.Weapon || cardState.Type == CardType.Body || cardState.Type == CardType.Cannon)
        {
            BoardState boardState = Board.Instance.GetCurrentBoardState();
            CellState cellState = boardState.GetCell(targetBoardId);

            if (cellState.occupyingCard != null && cellState.occupyingCard.Type == CardType.Hero)
            {
                commands = CommandFactory.Attach(cardState, targetBoardId);
            }
        }

        while (commands.Count > 0)
            currentCommands.Enqueue(commands.Dequeue());
    }

    public void MoveCard(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = CommandFactory.Move(cardState, targetBoardId);

        while (commands.Count > 0)
            currentCommands.Enqueue(commands.Dequeue());
    }

    public void Attack(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = CommandFactory.Attack(cardState, targetBoardId);

        while (commands.Count > 0)
            currentCommands.Enqueue(commands.Dequeue());
    }

    public void HeroPower(CardState CardState, int targetBoardId)
    {

    }

    public void EndTurn()
    {

    }

    public void Taunt(string message)
    {

    }

    #endregion

    #region GAME COMMAND ACTIONS -- Commands Initiated By the Game

    public void ExecuteGameAction(CardState cardState, int targetBoardId, GameAction gameAction)
    {
        switch (gameAction)
        {
            case GameAction.CreateCard:
                CreateCard(cardState, targetBoardId);
                break;
        }
    }

    public void CreateCard(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        if (cardState.Type == CardType.Hero || cardState.Type == CardType.Pet || cardState.Type == CardType.Building)
        {
            commands = CommandFactory.Summon(cardState, targetBoardId, false);
        }
        else if (cardState.Type == CardType.Spell || cardState.Type == CardType.Trick)
        {
            commands = CommandFactory.Cast(cardState, targetBoardId);
        }
        else if (cardState.Type == CardType.Weapon || cardState.Type == CardType.Body || cardState.Type == CardType.Cannon)
        {
            BoardState boardState = Board.Instance.GetCurrentBoardState();
            CellState cellState = boardState.GetCell(targetBoardId);

            if (cellState.occupyingCard != null && cellState.occupyingCard.Type == CardType.Hero)
            {
                commands = CommandFactory.Attach(cardState, targetBoardId);
            }
        }
        else if (cardState.Type == CardType.Tile)
        {
            commands = CommandFactory.Terraform(cardState, targetBoardId);
        }

        while (commands.Count > 0)
            currentCommands.Enqueue(commands.Dequeue());
    }

    #endregion

    public void Resolve()
    {
        for (int i = 0; i < currentCommands.Count; i++)
        {
            Command command = currentCommands.ElementAt(i);

            switch (command.Type)
            {
                case CommandType.Summon: 
                    SummonCommand summonCommand = ((SummonCommand)command);
                    CellState summonedCell = currentBoardState.GetCell(summonCommand.boardId);
                    CardState summonedCard = GameManager.Instance.GetCardState(summonCommand.gameId);
                    summonedCell.SetOccupied(summonedCard);
                    GameManager.Instance.MoveGameZone(summonedCard.GameId, GameZone.BoardZone);
                    break;
                case CommandType.Move:
                    MoveCommand moveCommand = ((MoveCommand)command);
                    CellState moveToCell = currentBoardState.GetCell(moveCommand.moveToBoardId);
                    CellState moveFromCell = currentBoardState.GetCell(moveCommand.boardId);
                    moveFromCell.SetOccupied(null);
                    moveToCell.SetOccupied(GameManager.Instance.GetCardState(moveCommand.gameId));
                    break;
                case CommandType.Cast:
                    CastCommand castCommand = ((CastCommand)command);
                    Debug.LogFormat("Casting card {0}", GameManager.Instance.GetCardState(castCommand.gameId).GetName());
                    break;
                case CommandType.Damage:
                    DamageCommand damageCommand = ((DamageCommand)command);
                    CardState state = GameManager.Instance.GetCardState(damageCommand.gameId);
                    state.Damage(damageCommand.damageInfo);
                    break;
                case CommandType.DamageEffect:
                    DamageEffectCommand damageEffect = ((DamageEffectCommand)command);
                    CardState damager = GameManager.Instance.GetCardState(damageEffect.gameId);
                    Debug.LogFormat("{0} is damaging at cell id {1}", damager.GetName(), damageEffect.boardId);
                    break;
                case CommandType.Draw:
                    DrawCommand drawCommand = ((DrawCommand)command);
                    PlayerState drawingPlayerState = GameManager.Instance.GetPlayer(drawCommand.targetPlayerId);
                    drawingPlayerState.Draw();
                    break;
                case CommandType.Attach:
                    AttachCommand attachCommand = ((AttachCommand)command);
                    CardState attachToCard = GameManager.Instance.GetCardState(attachCommand.attachToId);
                    CardState attachment = GameManager.Instance.GetCardState(attachCommand.gameId);
                    switch (attachment.Type)
                    {
                        case CardType.Weapon:
                            attachToCard.weaponAttachmentId = attachCommand.gameId;
                            break;
                        case CardType.Cannon:
                            attachToCard.cannonAttachmentId = attachCommand.gameId;
                            break;
                        case CardType.Body:
                            attachToCard.bodyAttachmentId = attachCommand.gameId;
                            break;
                    }
                    break;
                case CommandType.Terraform:
                    TerraformCommand terraformCommand = ((TerraformCommand)command);
                    CellState terraformCell = currentBoardState.GetCell(terraformCommand.boardId);
                    CardState tileState = GameManager.Instance.GetCardState(terraformCommand.gameId);
                    terraformCell.TransformTile(tileState);
                    break;
                case CommandType.Destroy:
                    break;
                default:
                    Debug.LogWarningFormat("Command of type {0} has not been implemented.", command.Type);
                    break;
            }
        }
    }

    public Command GetNext()
    {
        return currentCommands.Dequeue();
    }
}
