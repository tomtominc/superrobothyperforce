﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CommandFactory
{
    #region PLAYER ACTIONS -- Returns a Queue < Command >

    public static Queue < Command > Summon(CardState cardState, int targetBoardId, bool fromHand)
    {
        Queue < Command > commands = new Queue < Command >();

        Card summonedCard = cardState.GetCard();

        SummonCommand summonCommand = new SummonCommand();
        summonCommand.fromHand = fromHand;
        summonCommand.boardId = targetBoardId;
        summonCommand.gameId = cardState.GameId;

        commands.Enqueue(summonCommand);

        List < AbilityCardPair > triggeredAbilities = new List<AbilityCardPair>();

        if (fromHand)
        {
            triggeredAbilities.Add(TriggerAbilitiesSingle(cardState, targetBoardId, summonCommand.Trigger));
        }

        triggeredAbilities.AddRange(TriggerAbilitiesGlobal(targetBoardId, summonCommand.GlobalTrigger));

        var queuedCommands = ResolveCommands(triggeredAbilities, targetBoardId);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;

    }

    public static Queue < Command > Cast(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        Card castingCard = cardState.GetCard();

        CastCommand castCommand = new CastCommand();
        castCommand.gameId = cardState.GameId;

        commands.Enqueue(castCommand);

        var queuedCommands = ResolveAllAbilities(cardState, targetBoardId);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;
    }

    public static Queue < Command > Attach(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        Card card = cardState.GetCard();
        CardState attachToCard = Board.Instance.GetCurrentBoardState().GetCell(targetBoardId).occupyingCard;

        bool destroyAttachment = false;
        int attachmentId = -1;

        switch (cardState.Type)
        {
            case CardType.Weapon:
                if (attachToCard.weaponAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.weaponAttachmentId;
                }
                break;
            case CardType.Cannon:
                if (attachToCard.cannonAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.cannonAttachmentId;
                }
                break;
            case CardType.Body:
                if (attachToCard.bodyAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.bodyAttachmentId;
                }
                break;
        }

        if (destroyAttachment)
        {
            DestroyAttachmentCommand destroyAttachmentCommand = new DestroyAttachmentCommand();
            destroyAttachmentCommand.gameId = attachmentId;
            destroyAttachmentCommand.attachToId = attachToCard.GameId;
            destroyAttachmentCommand.boardId = -1;
            commands.Enqueue(destroyAttachmentCommand);
        }

        AttachCommand attachCommand = new AttachCommand();
        attachCommand.gameId = cardState.GameId;
        attachCommand.boardId = -1;
        attachCommand.attachToId = attachToCard.GameId;

        commands.Enqueue(attachCommand);

        return commands;

    }

    public static Queue < Command > Move(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        MoveCommand moveCommand = new MoveCommand();
        moveCommand.boardId = cardState.BoardId;
        moveCommand.moveToBoardId = targetBoardId;
        moveCommand.gameId = cardState.GameId;

        commands.Enqueue(moveCommand);

        //TODO check the tile: Lava tile will damage me. 

        List < AbilityCardPair > triggeredAbilities = new List < AbilityCardPair >();

        triggeredAbilities.Add(TriggerAbilitiesSingle(cardState, 
                targetBoardId, moveCommand.Trigger));

        triggeredAbilities.AddRange(TriggerAbilitiesGlobal(targetBoardId, 
                moveCommand.GlobalTrigger));

        var queuedCommands = ResolveCommands(triggeredAbilities, targetBoardId);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;
    }

    public static Queue < Command > Attack(CardState attacker, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();
        Queue < Command > attackQueue = new Queue < Command >();

        if (targetBoardId == -1)
            attackQueue = AttackSimple(attacker, out targetBoardId);
        else
            attackQueue = AttackTarget(attacker, targetBoardId);

        AttackBeginCommand attackBeginCommand = new AttackBeginCommand();
        attackBeginCommand.boardId = targetBoardId;
        attackBeginCommand.gameId = attacker.GameId;

        commands.Enqueue(attackBeginCommand);

        while (attackQueue.Count > 0)
            commands.Enqueue(attackQueue.Dequeue());

        AttackEndCommand attackEndCommand = new AttackEndCommand();
        attackEndCommand.boardId = targetBoardId;
        attackEndCommand.gameId = attacker.GameId;

        commands.Enqueue(attackEndCommand);

        List < AbilityCardPair > triggeredAbilities = new List < AbilityCardPair >();

        triggeredAbilities.Add(TriggerAbilitiesSingle(attacker, 
                targetBoardId, attackBeginCommand.Trigger));

        triggeredAbilities.AddRange(TriggerAbilitiesGlobal(targetBoardId, 
                attackBeginCommand.GlobalTrigger));

        var queuedCommands = ResolveCommands(triggeredAbilities, targetBoardId);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;
    }

    private static Queue < Command > AttackSimple(CardState attacker, out int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        int weaponGameId = attacker.GetWeapon();
        CardState weaponCardState = GameManager.Instance.GetCardState(weaponGameId);

        Debug.LogFormat("Weapon is of Type: {0}", weaponCardState.GetWeaponType());
        switch (weaponCardState.GetWeaponType())
        {
            case WeaponType.Shoot:
                
                var shootCommands = MoveProjectileStraight(weaponCardState, attacker.BoardId, out targetBoardId);

                while (shootCommands.Count > 0)
                    commands.Enqueue(shootCommands.Dequeue());
                
                break;
            default:
                targetBoardId = -1;
                break;
        }

        return commands;
    }

    private static Queue < Command > AttackTarget(CardState attacker, int targetBoardId)
    {
        return null;
    }

    private static Queue < Command > MoveProjectileStraight(CardState weaponCard, int startBoardId, out int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();
        Queue < Command > terraformCommandList = new Queue < Command >();

        BoardState boardState = Board.Instance.GetCurrentBoardState();
        int currentBoardId = startBoardId + 1;
        int currentRow = (int)(currentBoardId / Board.MAX_COLUMNS);

        Debug.LogFormat("Weapon card board id {0} current board id {1} current row {2} max tile: {3}", 
            startBoardId, currentBoardId, currentRow, Board.MAX_COLUMNS * (currentRow + 1));

        targetBoardId = -1;

        while (currentBoardId < Board.MAX_COLUMNS * (currentRow + 1))
        {
            CellState cell = boardState.GetCell(currentBoardId);

            Ability terraformAbility = weaponCard.GetCard().Abilities.
                Find(x => x.Action.ActionType == AbilityActionType.Terraform);

            if (terraformAbility != null)
            {
                var terraformCommands = AbilityResolver.GetCommands(null, terraformAbility, currentBoardId);

                while (terraformCommands.Count > 0)
                    terraformCommandList.Enqueue(terraformCommands.Dequeue());
            }

            if (cell.occupyingCard != null)
            {
                targetBoardId = cell.Id;

                var damageCommands = Damage(weaponCard, currentBoardId, 
                                         new DamageInfo(weaponCard.GetPower(), weaponCard.GetElement()));

                while (damageCommands.Count > 0)
                    commands.Enqueue(damageCommands.Dequeue());

                break;
            }

            currentBoardId++;
        }

        while (terraformCommandList.Count > 0)
            commands.Enqueue(terraformCommandList.Dequeue());

        return commands;
    }

    #endregion

    #region NON-ABILITY ACTIONS -- Commands Initiated By Other Means (Can still be abilites)

    public static Queue < Command > Draw(int numberOfCards, int targetPlayerId)
    {
        Queue < Command > commands = new Queue < Command >();

        DrawCommand drawCommand = new DrawCommand();
        drawCommand.numberOfCards = numberOfCards;
        drawCommand.targetPlayerId = targetPlayerId;

        commands.Enqueue(drawCommand);

        List < AbilityCardPair > triggeredAbilities = TriggerAbilitiesGlobal(-1, drawCommand.GlobalTrigger);

        var queuedCommands = ResolveCommands(triggeredAbilities, -1);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;

    }

    public static Queue < Command > Damage(CardState damager, int targetBoardId, DamageInfo damageInfo)
    {
        Queue < Command > commands = new Queue < Command >();

        CardState recepientOfDamage = Board.Instance.GetCurrentBoardState()
            .GetCell(targetBoardId).occupyingCard;

        if (recepientOfDamage == null)
        {
            Debug.LogWarningFormat("Occupying Card at {0} is null!", targetBoardId);
        }

        DamageEffectCommand damageEffect = new DamageEffectCommand();
        damageEffect.gameId = damager.GameId;
        damageEffect.boardId = targetBoardId;
        commands.Enqueue(damageEffect);

        DamageCommand damageCommand = new DamageCommand();
        damageCommand.boardId = targetBoardId;
        damageCommand.gameId = recepientOfDamage.GameId;
        damageCommand.damageInfo = damageInfo;
        commands.Enqueue(damageCommand);


        List < AbilityCardPair > triggeredAbilities = new List < AbilityCardPair >();

        triggeredAbilities.Add(TriggerAbilitiesSingle(recepientOfDamage, 
                targetBoardId, damageCommand.Trigger));

        triggeredAbilities.AddRange(TriggerAbilitiesGlobal(targetBoardId, 
                damageCommand.GlobalTrigger));

        var queuedCommands = ResolveCommands(triggeredAbilities, targetBoardId);

        while (queuedCommands.Count > 0)
            commands.Enqueue(queuedCommands.Dequeue());

        return commands;

    }

    public static Queue < Command > Terraform(CardState tile, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();
        TerraformCommand terraformCommand = new TerraformCommand();
        terraformCommand.boardId = targetBoardId;
        terraformCommand.gameId = tile.GameId;

        commands.Enqueue(terraformCommand);

        return commands;
    }



    #endregion

    #region COMMAND HELPERS --- Helpers that are generic to help with abilities and commands

    public static Queue < Command > ResolveAllAbilities(CardState cardState, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        Card resolvingCard = cardState.GetCard();

        foreach (var ability in resolvingCard.Abilities)
        {
            var queuedCommands = AbilityResolver.GetCommands(cardState, ability, targetBoardId);

            while (queuedCommands.Count > 0)
                commands.Enqueue(queuedCommands.Dequeue());
        }

        return commands;
       
    }

    public static Queue < Command > ResolveCommands(List < AbilityCardPair > triggeredAbilities, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        foreach (var pair in triggeredAbilities)
        {
            foreach (var ability in pair.abilities)
            {
                var queuedCommands = AbilityResolver.GetCommands(pair.cardState, ability, targetBoardId);

                while (queuedCommands.Count > 0)
                    commands.Enqueue(queuedCommands.Dequeue());
            }    
        }

        return commands;
    }

    public static AbilityCardPair TriggerAbilitiesSingle(CardState cardState, int targetBoardId, AbilityTrigger trigger)
    {
        AbilityCardPair pair = new AbilityCardPair();

        Card card = cardState.GetCard();
        pair.cardState = cardState;
        pair.abilities = card.Abilities.Where(x => x.Trigger == trigger).ToList();

        return pair;
    }

    public static List < AbilityCardPair > TriggerAbilitiesGlobal(int targetBoardId, AbilityTrigger trigger)
    {
        List < AbilityCardPair > triggeredAbilities = new List < AbilityCardPair >();

        foreach (var cell in Board.Instance.GetCurrentBoardState().GetCells ())
        {
            if (cell.occupyingCard != null)
                triggeredAbilities.Add(TriggerAbilitiesSingle(cell.occupyingCard, targetBoardId, trigger));        
        }

        return triggeredAbilities;
    }

    #endregion
}
