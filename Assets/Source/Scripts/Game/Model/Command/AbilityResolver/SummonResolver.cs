﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonResolver : AbilityResolver
{
    public override Queue<Command> Resolve(CardState cardState, Ability ability, int targetBoardId)
    {
        return null;
    }
}

public class AttachResolver : AbilityResolver
{
    public override Queue<Command> Resolve(CardState cardState, Ability ability, int targetBoardId)
    {
        Queue< Command > commands = new Queue<Command>();

        BoardState currentBoardState = Board.Instance.GetCurrentBoardState();
        Card card = cardState.GetCard();
        CardState attachToCard = currentBoardState.GetCell(targetBoardId).occupyingCard;

        bool destroyAttachment = false;
        int attachmentId = -1;

        switch (cardState.Type)
        {
            case CardType.Weapon:
                if (attachToCard.weaponAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.weaponAttachmentId;
                }
                break;

            case CardType.Cannon:
                if (attachToCard.cannonAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.cannonAttachmentId;
                }

                break;

            case CardType.Body:
                if (attachToCard.bodyAttachmentId > -1)
                {
                    destroyAttachment = true;
                    attachmentId = attachToCard.bodyAttachmentId;
                }

                break;
        }

        if (destroyAttachment)
        {
            DestroyAttachmentCommand destroyAttachmentCommand = new DestroyAttachmentCommand();
            destroyAttachmentCommand.gameId = attachmentId;
            destroyAttachmentCommand.attachToId = attachToCard.GameId;
            destroyAttachmentCommand.boardId = -1;
            commands.Enqueue(destroyAttachmentCommand);
        }

        AttachCommand attachCommand = new AttachCommand();
        attachCommand.boardId = -1;
        attachCommand.attachToId = attachToCard.GameId;

        commands.Enqueue(attachCommand);

        return commands;
        // List < Ability > battleCryAbilities = 

//        foreach (var a in card.Abilities)
//        {
//            if (ability.Trigger.TriggerType == AbilityTriggerType.Battlecry)
//                InvokeAbility(state, ability, targetBoardId);
//        }

    }
}

public class CastSpellResolver : AbilityResolver
{
    public override Queue<Command> Resolve(CardState cardState, Ability ability, int targetBoardId)
    {
        return null;   
    }
}
