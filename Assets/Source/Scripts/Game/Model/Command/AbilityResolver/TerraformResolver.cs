﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraformResolver :AbilityResolver
{
    public override Queue<Command> Resolve(CardState cardState, Ability ability, int targetBoardId)
    {
        TerraformAbilityAction terraformAction = ((TerraformAbilityAction)ability.Action);
        CardState tileState = GameManager.Instance.CreateTileCard(terraformAction.tileType);
        Queue < Command > commands = CommandFactory.Terraform(tileState, targetBoardId);
        return commands;
    }
}
