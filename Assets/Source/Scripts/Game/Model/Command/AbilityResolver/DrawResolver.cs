﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawResolver : AbilityResolver
{
    public override Queue <Command> Resolve(CardState cardState, Ability ability, int targetBoardId)
    {
        DrawAbilityAction drawAction = ((DrawAbilityAction)ability.Action);
        Queue < Command > commands = CommandFactory.Draw(drawAction.numberOfCards, cardState.OwnerId);
        return commands;
    }
}
