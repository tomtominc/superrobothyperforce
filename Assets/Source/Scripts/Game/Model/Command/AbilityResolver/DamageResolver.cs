﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DamageResolver : AbilityResolver
{
    public override Queue < Command > Resolve(CardState damager, Ability ability, int targetBoardId)
    {
        Queue < Command > commands = new Queue < Command >();

        DamageAbilityAction damageAction = ((DamageAbilityAction)ability.Action);

        switch (ability.TargetFilter.RandomType)
        {
            case RandomType.RandomSplit:
                for (int i = 0; i < damageAction.damageInfo.amount; i++)
                {
                    CellState boardCell = GetRandomBoardCell(ability, damager.OwnerId);

                    if (boardCell == null)
                        continue;

                    var queuedCommands = CommandFactory.Damage(damager, boardCell.Id, damageAction.damageInfo);

                    while (queuedCommands.Count > 0)
                        commands.Enqueue(queuedCommands.Dequeue());
                }
                break;

        }

        return commands;
    }
}
