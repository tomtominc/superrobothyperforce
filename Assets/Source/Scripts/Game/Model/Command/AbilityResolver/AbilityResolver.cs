﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The ability resolver is used to create actions
/// out of abilities triggered.
/// It also holds static universal methods for 
/// helping with trigger conditions / target conditions / and target filters
/// </summary>
public abstract class AbilityResolver
{
    public abstract Queue < Command > Resolve(CardState cardState, Ability ability, int targetBoardId);

    public static Queue < Command > GetCommands(CardState cardState, Ability ability, int targetBoardId)
    {
        switch (ability.Action.ActionType)
        {
            case AbilityActionType.Damage:
                return GetCommandQueue<DamageResolver>(cardState, ability, targetBoardId);
            case AbilityActionType.Draw:
                return GetCommandQueue<DrawResolver>(cardState, ability, targetBoardId);
            case AbilityActionType.Terraform:
                return GetCommandQueue<TerraformResolver>(cardState, ability, targetBoardId);
            default:
                Debug.LogWarningFormat("Command of type: {0} has not been created.", ability.Action.ActionType);
                return null;
        }
    }

    private static Queue < Command > GetCommandQueue < T >(CardState cardState, Ability ability, int targetBoardId)
        where T : AbilityResolver, new()
    {
        T resolver = new T();
        return resolver.Resolve(cardState, ability, targetBoardId);
    }

    public static List < int > GetPlayerTarget(TargetFilter targetFilter, int ownerId)
    {
        switch (targetFilter.TargetType)
        {
            case AbilityTargetType.Self:
                return new List < int >() { ownerId };
            case AbilityTargetType.Opponent:
                return new List<int>() { GameManager.Instance.GetOpponentPlayerId(ownerId) };
            default:
                return new List < int >() { ownerId };

        }
    }

    public CellState GetRandomBoardCell(Ability ability, int ownerId)
    {
        // check conditions 
        BoardState currentBoardState = Board.Instance.GetCurrentBoardState();
        AbilityTargetType targetType = ability.TargetFilter.TargetType;
        CardType cardType = ability.TargetFilter.CardType;

        List < CellState > targetCells = null;

        if (targetType == AbilityTargetType.Opponent)
        {
            List < CellState > states = currentBoardState.GetCells();

            targetCells = currentBoardState.GetCells().FilterByOwner(GameManager.Instance.GetOpponentPlayerId(ownerId))
                .FilterByCartridgeType(cardType);
        }
        else if (targetType == AbilityTargetType.Self)
        {
            targetCells = currentBoardState.GetCells().FilterByOwner(GameManager.Instance.GetPlayerId(ownerId))
                .FilterByCartridgeType(cardType);
        }
        else
        {
            targetCells = currentBoardState.GetCells().FilterByCartridgeType(cardType);
        }

        if (targetCells.Count == 0)
        {
            Debug.LogWarning("No Cells were found.");
            return null;
        }

        CellState targetCell = targetCells[Random.Range(0, targetCells.Count)];
        Debug.LogFormat("Picked target cell at: {0}", targetCell.Id);

        return targetCells[Random.Range(0, targetCells.Count)];
    }


    /// <summary>
    /// Tries to trigger an ability. use this when adding a command.
    /// </summary>
    /// <returns>The trigger ability.</returns>
    /// <param name="boardCell">Board cell.</param>
    /// <param name="type">Type.</param>
    public List < Ability > TryTriggerAbility(CellState cell, AbilityTrigger trigger)
    {
        List < Ability > queueAbilities = new List<Ability>();

        Card cartridge = cell.occupyingCard.GetCard();

        foreach (var ability in cartridge.Abilities)
        {
            if (ability.Trigger == trigger)
            {
                bool conditionMet = IsTriggerConditionsMet(ability.TriggerConditions);

                if (conditionMet)
                {
                    queueAbilities.Add(ability);
                }
            }
        }

        return queueAbilities;
    }


    /// <summary>
    /// Determines whether this instance is trigger conditions met the specified triggerConditions.
    /// </summary>
    /// <returns><c>true</c> if this instance is trigger conditions met the specified triggerConditions; otherwise, <c>false</c>.</returns>
    /// <param name="triggerConditions">Trigger conditions.</param>
    public bool IsTriggerConditionsMet(List < AbilityCondition > triggerConditions)
    {
        return true;
    }

   
}

public class AbilityCardPair
{
    public List < Ability > abilities;
    public CardState cardState;
}
