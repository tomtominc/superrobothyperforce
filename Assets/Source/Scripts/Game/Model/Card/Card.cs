﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Card
{
    /// <summary>
    /// The identifier for this cartridge,
    /// used to get the cartridge from a dictionary.
    /// </summary>
    public int Id;

    /// <summary>
    /// The name of the cartridge this is 
    /// used in gui.
    /// </summary>
    public string Name;

    /// <summary>
    /// The description text that's on
    /// the card.
    /// </summary>
    public string Description;

    /// <summary>
    /// The type of the cartridge.
    /// Pet / Weapon / Ability / Building / Trick 
    /// all have different behaviours
    /// </summary>
    public CardType CardType;
    /// <summary>
    /// The type of the weapon.
    /// </summary>
    public WeaponType WeaponType;

    /// <summary>
    /// How rare the cartridge is, determines 
    /// how often you get it in a pack.
    /// </summary>
    public Rarity Rarity;

    /// <summary>
    /// The card set of the catridge
    /// this is used to deprecate cards and
    /// for buying different packs.
    /// </summary>
    public CardSet CardSet;

    /// <summary>
    /// The class set is used to determine if
    /// a certain hero can use the card in their
    /// deck.
    /// </summary>
    public ClassSet ClassSet;

    /// <summary>
    /// The subsets are used to 
    /// determine some effects during the 
    /// game, e.g. heal all 'Undead' characters
    /// by 10 health.
    /// </summary>
    public Subset Subset;

    /// <summary>
    /// The level of the card, unused for now.
    /// </summary>
    public int Level;

    /// <summary>
    /// The cost of the cartridge, how much it
    /// costs to play it from your hand.
    /// </summary>
    public int Cost;

    /// <summary>
    /// The element of the card 
    /// is used to deal more / less damage
    /// and can be used for other effects.
    /// </summary>
    public Element Element;

    /// <summary>
    /// The power of the card, some 
    /// cartridge types don't use this.
    /// </summary>
    public int Power;

    /// <summary>
    /// The health of the card some cartridge types
    /// don't use this.
    /// </summary>
    public int Health;

    /// <summary>
    /// All of the associated effects for this card.
    /// </summary>
    public List < Ability > Abilities = new List< Ability >();
}

public enum TileType
{
    Normal,
    Lava,
    Grass,
    Water,
    Ice
}

[System.Flags]
public enum Rarity
{
    Common = 0,
    Rare = 1 << 1,
    Epic = 1 << 2,
    Legendary = 1 << 3,
    All = ~0
}

[System.Flags]
public enum CardSet
{
    Classic = 0,
    All = ~0
}

[System.Flags]
public enum ClassSet
{
    UniversalSet = 0,
    PyroSet = 1 << 1,
    ForestSet = 1 << 2,
    AquaSet = 1 << 3,
    ElectricSet = 1 << 4,
    All = ~0
}

[System.Flags]
public enum Subset
{
    None = 0,
    Worm = 1 << 1,
    Mech = 1 << 2,
    Beast = 1 << 3,
    Undead = 1 << 4,
    All = ~0

}

[System.Flags]
public enum CardType
{
    None = 0,
    Pet = 1 << 1,
    Weapon = 1 << 2,
    Spell = 1 << 3,
    Building = 1 << 4,
    Trick = 1 << 5,
    Hero = 1 << 6,
    Cannon = 1 << 7,
    Body = 1 << 8,
    Tile = 1 << 9,
    All = ~0

}

public enum WeaponType
{
    None,
    Sword,
    Shoot,
    Throw,
    Fling
}

public enum StatusEffect
{
    Normal,
    Burn,
    Paralyze,
    Frozen,
    Asleep,
    Poision
}

