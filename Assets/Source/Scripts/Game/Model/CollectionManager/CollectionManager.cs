﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core;
using System.Linq;

public sealed class CollectionManager
{
    private static CollectionManager instance;

    public static CollectionManager Instance
    {
        get
        {
            if (instance == null)
                instance = new CollectionManager();

            return instance;
        }
    }

    public Configuration config;

    public CollectionManager()
    {
        config = new Configuration();
        config.LoadGameConfiguration();
    }

    public Card GetCard(int id)
    {
        return config.CardCollection.Find(x => x.Id == id);
    }

    public Card GetCard(string name)
    {
        return config.CardCollection.Find(x => x.Name == name);
    }
}
