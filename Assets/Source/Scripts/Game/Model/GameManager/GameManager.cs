﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core;

public enum GameZone
{
    HandZone,
    BoardZone,
    GraveyardZone
}

public sealed class GameManager
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
                instance = new GameManager();

            return instance;
        }
    }

    private int currentId = -1;
    private Dictionary < int , CardState > registeredCardStates;
    private Dictionary < int , PlayerState > registeredPlayers;
    private Dictionary < int , CardView > registeredCardViews;

    public PlayerState CurrentPlayer;
    public PlayerState OpponentPlayer;
    public PlayerState ClientPlayer;

    public GameController.GameMode currentGameMode;

    public GameManager()
    {
        ClientPlayer = new PlayerState(0);
        OpponentPlayer = new PlayerState(1);

        registeredPlayers = new Dictionary<int, PlayerState>();
        registeredCardStates = new Dictionary<int, CardState>();
        registeredCardViews = new Dictionary < int , CardView >();

        RegisterPlayer(ClientPlayer);
        RegisterPlayer(OpponentPlayer);

    }

    public void SetGameMode(GameController.GameMode gameMode)
    {
        currentGameMode = gameMode;
    }

    public int GetOpponentPlayerId(int ownerId)
    {
        return ownerId == ClientPlayer.Id ? OpponentPlayer.Id : ClientPlayer.Id;
    }

    public int GetPlayerId(int ownerId)
    {
        return ownerId == ClientPlayer.Id ? ClientPlayer.Id : OpponentPlayer.Id;
    }

    public PlayerState GetPlayer(int Id)
    {
        return registeredPlayers[Id];   
    }

    public void RegisterPlayer(PlayerState state)
    {
        registeredPlayers.Add(state.Id, state);
    }

    public CardState GetCardState(int gameId)
    {
        return registeredCardStates[gameId];
    }

    public void RegisterCardState(CardState state)
    {
        registeredCardStates.Add(state.GameId, state);
    }

    public void RegisterCardView(CardView cardView)
    {
        registeredCardViews.Add(cardView.cardState.GameId, cardView);
    }

    public CardView GetCardView(int id)
    {
        return registeredCardViews[id];
    }

    public CardState CreateCardState(string name, int ownerId)
    {
        currentId++;

        CardState state = new CardState(name, currentId, ownerId);
        state.currentGameZone = GameZone.HandZone;
        RegisterCardState(state);

        return state;
    }

    public CardState CreateTileCard(TileType tileType)
    {
        CardState tileState = CreateCardState("Tile", -1);
        tileState.SetTileType(tileType);
        tileState.currentGameZone = GameZone.BoardZone;
        return tileState;
    }

    public CardState MoveGameZone(int gameId, GameZone gameZone)
    {
        CardState cardState = GetCardState(gameId);
        cardState.currentGameZone = gameZone;
        return cardState;
    }


}
