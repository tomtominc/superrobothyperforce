﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BoardState
{
    public List < CellState > cells = new List < CellState >();

    public BoardState()
    {
        for (int i = 0; i < Board.MAX_CELL_COUNT; i++)
        {
            CellState cell = new CellState(i);
            cells.Add(cell);
        }
    }

    public List < CellState > GetCells()
    {
        return new List < CellState >(cells);
    }

    public CellState GetCell(int id)
    {
        return cells.Find(x => x.Id == id);
    }

    public void SetCells(List < CellState > cells)
    {
        this.cells = new List < CellState >(cells);
    }

    public void SetCell(CellState cell)
    {
        CellState boardCell = this.cells.Find(x => x.Id == cell.Id);
        cells.Remove(boardCell);
        cells.Add(cell);
    }

    public void Print()
    {
        string board = string.Format("BoardSize: {0}\n", cells.Count);

        int currColumn = 0;

        for (int i = 0; i < Board.MAX_CELL_COUNT; i++)
        {
            currColumn++;

            if (currColumn > Board.MAX_COLUMNS)
            {
                board += "\n";
                currColumn = 1;
            }
                
            CellState cell = cells.Find(x => x.Id == i);

            if (cell.occupyingCard != null)
                board += string.Format("[{0} {1}/{2}]", cell.occupyingCard.GetName(), 
                    cell.occupyingCard.GetPower(), cell.occupyingCard.GetHealth());
            else
                board += "[ ]";
        }

        Debug.Log(board);
    }
}

