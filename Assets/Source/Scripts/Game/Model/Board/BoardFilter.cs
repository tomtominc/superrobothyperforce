﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class BoardFilter
{
    public static List < CellState > FilterByOccupied(this List < CellState > cells)
    {
        return cells.Where(x => x.occupyingCard != null && x.occupyingCard.GetHealth() > 0).ToList();
    }

    public static List < CellState > FilterByOwner(this List < CellState > cells, int ownerId)
    {
        return cells.FilterByOccupied().Where(x => x.occupyingCard.OwnerId == ownerId).ToList();
    }

    public static List < CellState > FilterByCartridgeType(this List < CellState > cells, CardType cartridgeType)
    {
        if (cartridgeType == CardType.All)
            return  cells.FilterByOccupied().ToList();
        
        return  cells.FilterByOccupied().Where(x => x.occupyingCard.Type == cartridgeType).ToList();
    }

    public static List < CellState > FilterByAbilityTrigger(this List < CellState > cells, AbilityTrigger trigger)
    {
        List < CellState > boardCells = cells.FilterByOccupied();

        List < CellState > filteredResults = new List<CellState>();    

        foreach (var boardCell in boardCells)
        {
            Card cartridge = CollectionManager.Instance.GetCard(boardCell.occupyingCard.CardId);

            foreach (var ability in cartridge.Abilities)
            {
                if (ability.Trigger == trigger)
                {
                    boardCell.ability = ability;
                    filteredResults.Add(boardCell);
                }
            }
        }

        return filteredResults;
    }
}
