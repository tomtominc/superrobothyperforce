﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Board
{
    private static Board instance;

    public static Board Instance
    {
        get
        {
            if (instance == null)
                instance = new Board();

            return instance;
        }
    }

    public const int MAX_ROWS = 3;
    public const int MAX_COLUMNS = 6;
    public const int MAX_CELL_COUNT = MAX_ROWS * MAX_COLUMNS;

    private BoardState currentBoardState;

    public Board()
    {
        currentBoardState = new BoardState();
    }

    public BoardState GetCurrentBoardState()
    {
        return currentBoardState;
    }

    public void SetCurrentBoardState(BoardState boardState)
    {
        currentBoardState.SetCells(boardState.GetCells());
    }

    /// <summary>
    /// Prints the board state.
    /// </summary>
    public void PrintBoardState()
    {
        currentBoardState.Print();
    }

}


