﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CellState
{
    public int Id;
    public CardState occupyingCard;
    public CardState tileCard;
    public int ownerId;
    public Vector2 point;

    public Ability ability;

    public void SetOccupied(CardState state)
    {
        occupyingCard = state;

        if (state != null)
            state.BoardId = Id;
    }

    public CellState()
    {
    }

    public CellState(int id)
    {
        Id = id;
        point = id.ToPoint(Board.MAX_COLUMNS);
        ownerId = point.x >= (Board.MAX_COLUMNS / 2) ? 1 : 0;
    }

    public void TransformTile(CardState tile)
    {
        tileCard = tile;

        if (tileCard != null)
            tileCard.BoardId = Id;
    }

    public override string ToString()
    {
        return string.Format("ID: {0} Card: {1} TileType: {2} Owner: {3} Point: {4}", Id,
            occupyingCard, tileCard, ownerId, point);
    }
}


