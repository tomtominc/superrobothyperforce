﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core.Animation;

public class ProjectileController : MonoBehaviour
{
    private SpriteAnimation animator;

    public void Start()
    {
        animator = GetComponent < SpriteAnimation >();    
    }

    public virtual IEnumerator Explode()
    {
        animator.PlayWithCallBack("Explosion", (x, y) =>
            {
                Destroy(gameObject);
            });

        yield break;
    }

    public virtual Transform Mover
    {
        get { return transform; }
    }
}
