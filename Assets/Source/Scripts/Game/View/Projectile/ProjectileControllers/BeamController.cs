﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamController : ProjectileController
{
    public enum Section
    {
        Start,
        Middle,
        Head
    }

    public BeamPart head;

    public Vector3 tileDistance;
    public Vector3 startDistance;
    public Transform startSection;
    public Transform middleSection;

    public override Transform Mover
    {
        get
        {
            return head.transform;
        }
    }

    private Vector3 startPosition = Vector3.zero;
    private int sectionCount;
    private float maxDistance;
    private Section _lastSection;

    private List < BeamPart > _sections;
    private bool _update = false;

    private Vector3 sectionPosition
    {
        get { return startDistance + (tileDistance * (sectionCount - 1)); }
    }

    public void Start()
    {
        head.Initialize(0);
        _sections = new List < BeamPart >();
        maxDistance = Vector3.Distance(startPosition, tileDistance);
        CreateBeamPart(Section.Start);

        _update = true;
    }

    public void CreateBeamPart(Section section)
    {
        Transform prefab = section == Section.Start ? startSection : middleSection;
        Transform startInstance = Instantiate < Transform >(prefab);

        startInstance.SetParent(transform);
        startInstance.localPosition = section == Section.Start ? Vector3.zero : sectionPosition;

        BeamPart part = startInstance.GetComponent < BeamPart >();

        _sections.Add(part);

        foreach (var s in _sections)
        {
            s.Initialize(head.CurrentFrame);
        }

        sectionCount++;

    }

    public void Update()
    {
        if (!_update)
            return;
        
        int sections = DetermineSectionCount();

        for (int i = 0; i < sections; i++)
        {
            CreateBeamPart(Section.Middle);
        }
    }

    public int DetermineSectionCount()
    {
        float totalDistance = Vector3.Distance(head.transform.localPosition, startPosition);
        float filledDistance = Vector3.Distance(startPosition, sectionPosition);

        float unFilledDistance = totalDistance - filledDistance;

        return Mathf.RoundToInt((unFilledDistance / maxDistance) + 0.5f);
    }

    public override IEnumerator Explode()
    {
        _update = false;

        StartCoroutine(head.Explode());

        foreach (var part in _sections)
        {
            StartCoroutine(part.Explode());
        }

        yield return new WaitForSeconds(0.5f);

    }

}
