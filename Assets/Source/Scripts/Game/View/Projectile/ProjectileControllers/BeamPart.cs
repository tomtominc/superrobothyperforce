﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Core.Animation;

public class BeamPart : MonoBehaviour
{
    public BeamController.Section section;
    private SpriteRenderer spriteRenderer;
    private SpriteAnimation animator;

    public int CurrentFrame
    {
        get { return animator.CurrentFrame; }
    }

   
    public void Initialize(int keyFrame)
    {
        animator = GetComponent < SpriteAnimation >();
        spriteRenderer = GetComponent < SpriteRenderer >();
        animator.playFrom = keyFrame;
        animator.Play(section);
    }

    public IEnumerator Explode()
    {
        yield return StartCoroutine(HSVUtility.Exit(spriteRenderer, 0.5f));

        Destroy(gameObject);
    }

}
