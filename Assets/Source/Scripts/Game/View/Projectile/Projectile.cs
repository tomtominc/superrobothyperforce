﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Framework.Core.Animation;

public class Projectile : MonoBehaviour
{
    public ProjectileController controller;
    public ProjectileType projectileType;
    public MovementType movementType;
    public Ease moveEase;
    public SpawnPoint spawnPoint;
    public Vector3 offsetPosition;
    public Vector3 targetOffset;
    public float duration = 1f;

    public Data data;

    protected SpriteAnimation animator;
    protected SpriteRenderer spriteRenderer;

    protected ProjectileMovement _movement;

    public IEnumerator Launch(Data data)
    {
        this.data = data;
        animator = GetComponentInChildren < SpriteAnimation >();
        spriteRenderer = GetComponentInChildren < SpriteRenderer >();
        spriteRenderer.flipX = this.data.ownerId == GameManager.Instance.OpponentPlayer.Id;

        CreateMovement();

        Tweener moveTween = _movement.GetMoveTween();
        yield return moveTween.WaitForCompletion();
    }

    public IEnumerator Explode()
    {
        yield return StartCoroutine(controller.Explode());
    }


    #region HELPER METHODS

    protected virtual void CreateMovement()
    {
        switch (movementType)
        {
            case Projectile.MovementType.Straight:
                _movement = new StraightMovement(this);
                break;
        }
    }

    public Transform GetController()
    {
        return controller.Mover;
    }

    #endregion

    #region DATA STRUCTURES -- Defines And Data To Change Projectile Behaviour

    public enum ProjectileType
    {
        Bullet,
        Beam
    }

    public enum SpawnPoint
    {
        BarrelPoint,
        OffsetPosition,
    }

    public enum MovementType
    {
        Straight
    }

    public class Data
    {
        public int ownerId;
        public Vector3 barrelPoint;
        public Vector3 targetPoint;
        public Action onFinished;
    }

    #endregion

}
