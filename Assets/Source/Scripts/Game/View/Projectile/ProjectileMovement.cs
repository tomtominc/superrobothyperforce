﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class ProjectileMovement
{
    protected Projectile _projectile;

    public ProjectileMovement(Projectile projectile)
    {
        _projectile = projectile;    
        _projectile.transform.position = GetInitialPosition();
    }

    public abstract Tweener GetMoveTween();

    public Vector3 GetInitialPosition()
    {
        switch (_projectile.spawnPoint)
        {
            case Projectile.SpawnPoint.BarrelPoint:
                return _projectile.data.barrelPoint + _projectile.offsetPosition;
            case Projectile.SpawnPoint.OffsetPosition:
                return GetOffsetPosition(_projectile.offsetPosition);
            default:
                return GetOffsetPosition(_projectile.offsetPosition);
        }
    }

    protected Vector3 GetOffsetPosition(Vector3 offset)
    {
        float xOffset = offset.x * (_projectile.data.ownerId == GameManager.Instance.OpponentPlayer.Id ? 1 : -1);
        return new Vector3(_projectile.data.targetPoint.x + xOffset, _projectile.data.targetPoint.y + offset.y);
    }

    protected Vector3 GetTargetPosition(Vector3 targetPosition)
    {
        return targetPosition + _projectile.targetOffset;
    }
}

public class StraightMovement : ProjectileMovement
{

    public StraightMovement(Projectile projectile)
        : base(projectile)
    {
        
    }

    public override Tweener GetMoveTween()
    {
        return _projectile.GetController().DOMove(GetTargetPosition(_projectile.data.targetPoint), _projectile.duration)
            .SetEase(_projectile.moveEase);
    }

}
