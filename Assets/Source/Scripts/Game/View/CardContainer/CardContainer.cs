﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardContainer : MonoBehaviour
{
    private CardState cardState;
    private bool hasBeenUsed = false;

    public UISpriteText cardName;


    public void Initialize(CardState cardState)
    {
        this.cardState = cardState;
        UpdateView();
    }


    public void UpdateView()
    {
        cardName.Text = cardState.GetName();
    }

    public void UseCard()
    {
        hasBeenUsed = true;

        switch (cardState.Type)
        {
            case CardType.Hero:
                Summon();
                break;

            case CardType.Pet:
                Summon();
                break;

            case CardType.Building:
                Summon();
                break;

            case CardType.Spell:
                CastSpell();
                break;

            case CardType.Weapon:
                Attach();
                break;

            case CardType.Body:
                Attach();
                break;

            case CardType.Cannon:
                Attach();
                break;

            case CardType.Trick:
                CastSpell();
                break;
        }
    }

    public void Summon()
    {

    }

    public void Attach()
    {
        
    }

    public void CastSpell()
    {
        
    }


}
