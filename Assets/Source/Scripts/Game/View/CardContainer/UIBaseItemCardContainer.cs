﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class UIBaseItemCardContainer  : UIBaseItem
{
    public Text nameLabel;

    public override void SetItem(Dictionary<string, object> values, Action<ActionEventArgs> onClicked)
    {
        base.SetItem(values, onClicked);

        nameLabel.text = (string)values["Name"];
        nameLabel.text += string.Format(" ({0})", values["Type"]);
    }
}
