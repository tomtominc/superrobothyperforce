﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gamelogic.Grids;
using Framework.Core.Animation;

public class CellView : SpriteCell
{
    public CellState cellState;
    private SpriteAnimation _cellSprite;

    public void Initialize()
    {
        _cellSprite = transform.FindChild("Sprite").GetComponent < SpriteAnimation >();
    }

    public void UpdateView(CellState cellState)
    {
        this.cellState = cellState;

        _cellSprite.PlayFormat("{0}_{1}", this.cellState.ownerId, this.cellState.point.y);
    }

    public void PrintState()
    {
        Debug.Log(this.cellState);
    }
}
