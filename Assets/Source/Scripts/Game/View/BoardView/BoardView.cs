﻿using UnityEngine;
using Gamelogic.Grids;
using System.Collections.Generic;

/// <summary>
/// The board view has a list of grid points and 
/// updates things like cells in each grid. 
/// </summary>
public class BoardView : GridBehaviour<RectPoint>
{
    private static BoardView instance;

    public static BoardView Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < BoardView >();
            return instance;
        }
    }

    public IGrid < CellView , RectPoint > _grid;

    public override void InitGrid()
    {
        _grid = Grid.CastValues<CellView, RectPoint>();
        _grid.Apply(x => x.Initialize());

        UpdateView();
    }

    public void UpdateView()
    {
        BoardState boardState = Board.Instance.GetCurrentBoardState();

        foreach (var cell in boardState.cells)
        {
            CellView cellView = GetCell(cell.point);
            cellView.UpdateView(cell);
        }
    }

    public CellView GetCell(Vector2 point)
    {
        RectPoint rectPoint = GetPoint(point);
        return GetCell(rectPoint);
    }

    public CellView GetCell(RectPoint point)
    {
        if (_grid.Contains(point))
            return _grid[point];
        return null;
    }

    public CellView GetCellByWorldPosition(Vector2 worldPosition)
    {
        RectPoint point = Map[worldPosition];
        return GetCell(point);
    }

    public CellView GetCell(int boardId)
    {
        Vector2 point = boardId.ToPoint(Board.MAX_COLUMNS);
        return GetCell(point);
    }

    public RectPoint GetPoint(Vector2 point)
    {
        return new RectPoint((int)point.x, (int)point.y);
    }

    public void Move(CardView cardView, int boardId)
    {
        CellView cellView = GetCell(boardId);
        cardView.transform.position = cellView.transform.position;
    }
}
