﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Remoting.Contexts;

public class CommandTask
{
    protected ViewResolver _context;
    protected Command _command;


    public virtual void Initialize(ViewResolver context, Command command)
    {
        _context = context;
        _command = command;
    }

    public virtual IEnumerator Resolve()
    {
        Debug.LogFormat("Resolving task: {0}", _command.Type);
        yield return new WaitForSeconds(1f);
    }
}

public class SummonCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CellView cellView = BoardView.Instance.GetCell(_command.boardId);
        CardView cardView = PrefabManager.Instance.GetInstance(_command.gameId);
        cardView.transform.position = cellView.transform.position;

        yield return _context.StartCoroutine(cardView.Summon(_command));

    }
}

public class CastCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CardView cardView = PrefabManager.Instance.GetInstance(_command.gameId);
        cardView.transform.position = Vector3.zero;
        yield break;
    }
}

public class AttachCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        AttachCommand attachCommand = (AttachCommand)_command;
        AttachmentCardView attachmentCardView = (AttachmentCardView)PrefabManager.Instance.GetInstance(attachCommand.gameId);
        EntityCardView entityCardView = (EntityCardView)GameManager.Instance.GetCardView(attachCommand.attachToId);

        entityCardView.Attach(attachmentCardView);

        yield return _context.StartCoroutine(attachmentCardView.Summon(_command));
    }
}

public class MoveCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CardView cardView = GameManager.Instance.GetCardView(_command.gameId);

        yield return _context.StartCoroutine(cardView.Move(_command));
    }
}

public class AttackBeginCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CardView cardView = GameManager.Instance.GetCardView(_command.gameId);
       
        yield return _context.StartCoroutine(cardView.AttackBegin(_command));
    }
}

public class AttackEndCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CardView cardView = GameManager.Instance.GetCardView(_command.gameId);

        yield return _context.StartCoroutine(cardView.AttackEnd(_command));
    }
}

public class DamageCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        DamageCommand damageCommand = (DamageCommand)_command;
        CardView targetCardView = GameManager.Instance.GetCardView(damageCommand.gameId);
        yield return _context.StartCoroutine(targetCardView.Hurt(_command));
    }
}

public class DamageEffectCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        DamageEffectCommand effectCommand = (DamageEffectCommand)_command;
        CardView effectVisual = GameManager.Instance.GetCardView(effectCommand.gameId);

        yield return _context.StartCoroutine(effectVisual.DoDamageEffect(effectCommand));
    }
}



public class TerraformCommandTask : CommandTask
{
    public override IEnumerator Resolve()
    {
        CellView cellView = BoardView.Instance.GetCell(_command.boardId);
        CardView cardView = PrefabManager.Instance.GetInstance(_command.gameId);
        cardView.transform.position = cellView.transform.position;

        _context.StartCoroutine(cardView.Summon(_command));
        yield break;
    }
}