﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ViewResolver : MonoBehaviour
{
    private static ViewResolver instance;

    public static ViewResolver Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType < ViewResolver >();
                instance.queuedSystems = new Queue<CommandSystem>();
            }

            return instance;
        }
    }

    private Queue < CommandSystem > queuedSystems;
    private bool _isResolving = false;

    public void AddToCommandQueue(CommandSystem system)
    {
        system.Resolve();
        queuedSystems.Enqueue(system);

        if (!_isResolving)
        {
            StartCoroutine(ResolveRoutine());
        }
    }

    private IEnumerator ResolveRoutine()
    {
        _isResolving = true;

        while (queuedSystems.Count > 0)
        {
            CommandSystem system = queuedSystems.Dequeue();

            while (system.currentCommands.Count > 0)
            {
                Command command = system.currentCommands.Dequeue();

                yield return StartCoroutine(ResolveCommand(command));
            }
        }

        _isResolving = false;
    }

    private IEnumerator ResolveCommand(Command command)
    {
        switch (command.Type)
        {
            case CommandType.Summon: 
                yield return StartCoroutine(ResolveCommandTask < SummonCommandTask >(command));
                yield break;
            case CommandType.Cast:
                yield return StartCoroutine(ResolveCommandTask < CastCommandTask >(command));
                yield break;
            case CommandType.Attach:
                yield return StartCoroutine(ResolveCommandTask < AttachCommandTask >(command));
                yield break;
            case CommandType.Move:
                yield return StartCoroutine(ResolveCommandTask < MoveCommandTask >(command));
                yield break;
            case CommandType.AttackBegin:
                yield return StartCoroutine(ResolveCommandTask < AttackBeginCommandTask >(command));
                yield break;
            case CommandType.AttackEnd:
                yield return StartCoroutine(ResolveCommandTask < AttackEndCommandTask >(command));
                yield break;
            case CommandType.Damage:
                yield return StartCoroutine(ResolveCommandTask < DamageCommandTask >(command));
                yield break;
            case CommandType.DamageEffect:
                yield return StartCoroutine(ResolveCommandTask < DamageEffectCommandTask >(command));
                yield break;
            case CommandType.Terraform:
                yield return StartCoroutine(ResolveCommandTask < TerraformCommandTask >(command));
                yield break;
            default:
                Debug.LogWarningFormat("Command of Type {0} has not been implemented.", command.Type);
                yield return new WaitForSeconds(1f);
                yield break;
        }

        yield break;

    }

    public IEnumerator ResolveCommandTask < T >(Command command)  where T : CommandTask, new()
    {
        T task = new T();
        task.Initialize(this, command);
        yield return StartCoroutine(task.Resolve());
    }
}
