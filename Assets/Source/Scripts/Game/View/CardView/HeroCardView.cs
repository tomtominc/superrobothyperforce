﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeroCardView : EntityCardView
{
    protected CellView currentCellView;
    public float minimumDragActionDistance = 1f;

    public void SetupHero()
    {
        // set up the hero with weapon and body
    }

    public override IEnumerator AttackBegin(Command command)
    {
        if (weaponAttachment == null)
            yield break;

        string attackAnimation = string.Format("{0}Attack", weaponAttachment.cardState.GetWeaponType());
        animator.Play(attackAnimation);

        yield return StartCoroutine(weaponAttachment.AttackBegin(command));
    }

    public override IEnumerator AttackEnd(Command command)
    {
        if (weaponAttachment == null)
            yield break;

        yield return StartCoroutine(weaponAttachment.AttackEnd(command));
        animator.Play(State.Idle);
    }

    public override void OnBeginTouch(Vector2 startPosition)
    {
    }

    public override void OnDrag(Vector2 dragPosition)
    {
        currentCellView = BoardView.Instance.GetCellByWorldPosition(dragPosition);
    }

    public override void OnEndTouch()
    {
        if (currentCellView != null && currentCellView.cellState.Id != cardState.BoardId)
        {
            CommandSystem commandSystem = new CommandSystem();
            commandSystem.MoveCard(cardState, currentCellView.cellState.Id);
            ViewResolver.Instance.AddToCommandQueue(commandSystem);
        }
    }
}
