﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TileCardView : CardView
{
    public Material lavaMaterial;

    protected string tileAnimation
    {
        get
        {
            return string.Format("{0}_{1}", cardState.GetTileType(),
                cardState.BoardId.ToPoint(Board.MAX_COLUMNS).y);
        }
    }

    public override void Initialize(CardState cardState)
    {
        base.Initialize(cardState);
        animator.Play(tileAnimation);
        StartTileEffect();
    }

    private void StartTileEffect()
    {
        switch (cardState.GetTileType())
        {
            case TileType.Lava:
                LavaEffect();
                break;
        }
    }

    public static bool isLavaEffect = false;
    public static float lavaGlowValue = 0.5f;
    public static List < SpriteRenderer > lavaMaterials = new List<SpriteRenderer>();

    public void LavaEffect()
    {
        spriteRenderer.material.SetFloat("_Val", lavaGlowValue);
        lavaMaterials.Add(spriteRenderer);

        if (isLavaEffect)
            return;

        isLavaEffect = true;
        spriteRenderer.material.SetFloat("_Val", lavaGlowValue);

        DOTween.To(() => lavaGlowValue, x => lavaGlowValue = x, 1.5f, 2f).
        SetLoops(-1, LoopType.Yoyo).
        OnUpdate(() =>
            {
                List < SpriteRenderer > removals = new List<SpriteRenderer >();

                foreach (var sr in lavaMaterials)
                {
                    if (sr == null)
                    {
                        removals.Add(sr);
                        continue;
                    }

                    sr.material.SetFloat("_Val", lavaGlowValue);
                }

                foreach (var sr in removals)
                    lavaMaterials.Remove(sr);
                    
            });


    }

}
