﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatView : MonoBehaviour
{
    public UISpriteText label;

    private int _value;
    private int _maxValue;

    public int CurrentValue
    {
        get { return _value; }
    }

    public int CurrentMaxValue
    {
        get { return _maxValue; }
    }

    public void Initialize(int value, int maxValue)
    {
        SetMaxValue(maxValue);
        SetValue(value);
    }

    public void SetMaxValue(int maxValue)
    {
        _maxValue = maxValue;
    }

    public void SetValue(int value)
    {
        if (value > _maxValue)
            value = _maxValue;

        _value = value;

        UpdateView();
    }

    private void UpdateView()
    {
        label.Text = _value.ToString();    
    }
}
