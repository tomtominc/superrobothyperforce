﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    public StatView healthView;
    public StatView powerView;

    private CardState _cardState;

    public void Initialize(CardState cardState)
    {
        _cardState = cardState;

        if (_cardState.GetPower() <= 0)
            powerView.gameObject.SetActive(false);


        Debug.Log(_cardState.GetHealth());
        healthView.Initialize(_cardState.GetHealth(), _cardState.GetHealth());
        powerView.Initialize(_cardState.GetPower(), _cardState.GetPower());
    }

    public void UpdateView()
    {
        if (healthView.CurrentValue != _cardState.GetHealth())
            healthView.SetValue(_cardState.GetHealth());

        if (powerView.CurrentValue != _cardState.GetPower())
            powerView.SetValue(_cardState.GetPower());
    }
}
