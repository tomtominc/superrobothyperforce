﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A card view that resides and occupies a cell
/// like hero cards, pet cards, and building cards.
/// </summary>
public class EntityCardView : CardView, ISelectionHandler
{
    private InputController inputController;

    [HideInInspector]
    public WeaponCardView weaponAttachment;

    public virtual void Attach(AttachmentCardView attachment)
    {
        switch (attachment.cardState.Type)
        {
            case CardType.Weapon:
                SetWeaponAttachment((WeaponCardView)attachment);
                break;
        }
    }

    public virtual void SetWeaponAttachment(WeaponCardView attachment)
    {
        attachment.transform.SetParent(transform, true);
        attachment.transform.localPosition = Vector3.zero;
        weaponAttachment = attachment;
    }

    public virtual void SetController(InputController inputController)
    {
        this.inputController = inputController;
    }

    public virtual void OnBeginTouch(Vector2 startPosition)
    {
        //Debug.LogFormat("OnBeginTouch started on: {0}", name);
    }

    public virtual void OnDrag(Vector2 dragPosition)
    {
        //Debug.LogFormat("OnDrag started on: {0}", name);
    }

    public virtual void OnEndTouch()
    {
        //Debug.LogFormat("OnEndTouch started on: {0}", name);
    }
}
