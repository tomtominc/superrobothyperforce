﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCardView : CardView
{
    public Transform effectPrefab;

    public override IEnumerator DoDamageEffect(Command command)
    {
        DamageEffectCommand damageEffect = ((DamageEffectCommand)command);
        CardState spellCard = GameManager.Instance.GetCardState(damageEffect.gameId);
        CellView targetCell = BoardView.Instance.GetCell(damageEffect.boardId);

        Transform effectInstance = Instantiate < Transform >(effectPrefab);
        Projectile effectMover = effectInstance.GetComponent < Projectile >();

        Projectile.Data data = new Projectile.Data();
        data.ownerId = cardState.GameId;
        data.targetPoint = targetCell.transform.position;

        yield return StartCoroutine(effectMover.Launch(data));
    }
}
