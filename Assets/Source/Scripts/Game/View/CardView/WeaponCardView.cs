﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCardView : AttachmentCardView
{
    public Transform effectPrefab;
    public Transform barrelAnchor;

    private Projectile activeProjectile;

    public override IEnumerator AttackBegin(Command command)
    {
        animator.Play(State.Attack);

        switch (cardState.GetWeaponType())
        {
            case WeaponType.Shoot:
                yield return StartCoroutine(DoShootableAttack(command));
                yield break;
            default:
                Debug.LogWarningFormat("Weapon of type {0} is not supported.", cardState.GetWeaponType());
                yield break;
        }
    }

    public override IEnumerator AttackEnd(Command command)
    {
        if (activeProjectile != null)
        {
            yield return StartCoroutine(activeProjectile.Explode());
        }

        animator.Play(State.Idle);
        yield break;
    }

    public IEnumerator DoShootableAttack(Command command)
    {
        AttackBeginCommand attackBegin = ((AttackBeginCommand)command);

        CellView targetCell = BoardView.Instance.GetCell(attackBegin.boardId);

        if (targetCell == null)
        {
            Debug.LogWarningFormat("Target cell {0} is null.", attackBegin.boardId);
            yield break;
        }
        
        Transform effectInstance = Instantiate < Transform >(effectPrefab);

        activeProjectile = effectInstance.GetComponent < Projectile >();

        Projectile.Data data = new Projectile.Data();
        data.ownerId = cardState.GameId;
        data.barrelPoint = barrelAnchor.position;
        data.targetPoint = targetCell.transform.position;

        data.targetPoint.y = barrelAnchor.position.y;

        yield return StartCoroutine(activeProjectile.Launch(data));
    }
}
