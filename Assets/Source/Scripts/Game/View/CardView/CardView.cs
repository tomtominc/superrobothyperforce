﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using TaskManager;
using System;
using Framework.Core.Animation;

public class CardView : MonoBehaviour
{
    public enum State
    {
        Idle,
        Summon,
        Hurt,
        Move,
        Attack,
        SwordAttack,
        FlingAttack,
        ShootAttack,
        CannonAttack
    }

    protected internal CardState cardState;

    protected internal SpriteAnimation animator;
    protected internal SpriteRenderer spriteRenderer;
    protected internal StatsController statsController;

    public virtual void Initialize(CardState cardState)
    {
        this.cardState = cardState;

        animator = GetComponentInChildren < SpriteAnimation >();
        spriteRenderer = GetComponentInChildren < SpriteRenderer >();
        statsController = GetComponentInChildren < StatsController >();

        if (spriteRenderer)
            spriteRenderer.flipX = this.cardState.OwnerId == GameManager.Instance.OpponentPlayer.Id;

        if (statsController)
            statsController.Initialize(this.cardState);
    }

    public virtual void DoAnimation(State state)
    {
        animator.PlayWithCallBack(state, (x, y) =>
            {
                animator.Play(State.Idle);
            });
    }


    public virtual IEnumerator Summon(Command command)
    {
        yield return StartCoroutine(HSVUtility.Damage(spriteRenderer, 0.5f));
    }

    public virtual IEnumerator Move(Command command)
    {
        DoAnimation(State.Move);
        MoveCommand moveCommand = (MoveCommand)command;
        BoardView.Instance.Move(this, moveCommand.moveToBoardId);
        yield return new WaitForEndOfFrame();
    }

    public virtual IEnumerator AttackBegin(Command command)
    {
        yield break;
    }

    public virtual IEnumerator AttackEnd(Command command)
    {
        yield break;
    }

    public virtual IEnumerator Hurt(Command command)
    {
        yield return StartCoroutine(HSVUtility.Damage(spriteRenderer, 0.5f));

        statsController.UpdateView();
    }

    public virtual IEnumerator DoDamageEffect(Command command)
    {
        yield break;
    }

   
}

