﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelectionHandler
{
    void SetController(InputController inputController);

    void OnBeginTouch(Vector2 startPosition);

    void OnDrag(Vector2 dragPosition);

    void OnEndTouch();
}
