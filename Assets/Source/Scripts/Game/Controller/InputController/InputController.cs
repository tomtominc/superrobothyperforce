﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the input, this is controlled by the gamestate
/// the game state is resposible for updating this class
/// so the user can have input.
/// This allows the game controller to update only certain things
/// in certain game states. 
/// 
/// The controller gets any iselectable object and updates it based on
/// the updates.
/// </summary>
public class InputController
{
    public GameController gameController;
    public ISelectionHandler currentInstance;

    public Vector2 mousePosition
    {
        get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); }
    }

    public InputController(GameController gameController)
    {
        this.gameController = gameController;    
    }

    public void Update()
    {
        #if UNITY_STANDALONE || UNITY_EDITOR

        if (Input.GetMouseButton(0))
        {
            ISelectionHandler instance = GetHandler();

            if (currentInstance == null && instance != null)
            {
                currentInstance = instance;
                currentInstance.OnBeginTouch(mousePosition);
            }
            else if (currentInstance != null)
            {
                currentInstance.OnDrag(mousePosition);
            }
        }

        if (Input.GetMouseButtonUp(0) && currentInstance != null)
        {
            currentInstance.OnEndTouch();
            currentInstance = null;
        }

        #endif
    }

    public ISelectionHandler GetHandler()
    {
        RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero);

        if (hit.transform != null && hit.transform.GetComponent < ISelectionHandler >() != null)
        {
            return hit.transform.GetComponent < ISelectionHandler >();
        }

        return null;
    }
}
