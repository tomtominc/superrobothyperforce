﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DoozyUI;

public class InitializePhase : GamePhase
{
    public override void StateStart()
    {
        UIManager.ShowUiElement("CardDebugMenu");

        context.ChangeState(GameController.Phase.Main);
    }
}
