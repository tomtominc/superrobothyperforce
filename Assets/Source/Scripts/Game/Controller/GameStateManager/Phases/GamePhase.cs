﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;

/// <summary>
/// An abstract gamephase, to make more
/// phases you will need to derive off of this class.
/// </summary>
public class GamePhase : State < GameController >
{
    
}
