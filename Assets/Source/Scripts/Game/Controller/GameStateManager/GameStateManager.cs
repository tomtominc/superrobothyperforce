﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    private static GameStateManager instance;

    public static GameStateManager Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType < GameStateManager >();
            return instance;
        }
    }

    private GameController _gameController;

    public void Awake()
    {
        //_gameController = new GameController(this);
    }

    public void Update()
    {
        //_gameController.Update();
    }
}
