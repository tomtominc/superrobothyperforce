﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using TaskManager;

public class GameController : MonoBehaviour
{
    public enum Phase
    {
        /// <summary>
        /// Initializes the game.
        /// Starts the game visuals by showing versus
        /// Shuffles player decks, gives players their
        /// starting hand.
        /// Picks who will go first.
        /// Spawns both the heros.
        /// Sets commands for hero battlecrys etc.
        /// Hands the game off to the 1st player and starts them.
        /// </summary>
        Initialize,
        /// <summary>
        /// The start of the turn.
        /// checks to see if any cardstates have the 
        /// onstartturn condition to active affects
        /// goes to the draw phase after.
        /// </summary>
        Start,
        /// <summary>
        /// Handles the card draw, checks to see if
        /// anything has a condition onplayerdraw
        /// goes to the main phase after.
        /// </summary>
        Draw,
        /// <summary>
        /// the main phase starts by activating all applicable 
        /// cardviews and handcards. Handcards are active when
        /// their cost is less than or equal to the oil that you have
        /// the cardviews are active when a player can do an action with 
        /// them.  
        /// </summary>
        Main,
        /// <summary>
        /// The end phase happens right before the start phase of the 
        /// other players turn. the end phase allows for clean up
        /// and for effects that happens when a players turn is over.
        /// </summary>
        End,
        /// <summary>
        /// The wait phase is when it's your opponents turn.
        /// waiting happens after the end phase when control is
        /// given to your opponent. 
        /// </summary>
        Wait
    }

    public enum GameMode
    {
        /// <summary>
        /// In local mode you play against a 
        /// computer player.
        /// </summary>
        Local,
        /// <summary>
        /// This mode allows you to simulate 
        /// a local game by playing against yourself.
        /// It's for debug purposes only.
        /// </summary>
        LocalSimulation,
        /// <summary>
        /// In online mode you play
        /// against someone else on a 
        /// server using Photon
        /// </summary>
        Online,
        /// <summary>
        /// Debug mode allows for 
        /// testing of cards through the 
        /// Debug GUI.
        /// You can simulate another player by
        /// switching who plays what. In
        /// debug mode there are no phases.
        /// </summary>
        Debug
    }

    public StateManager < GameController > stateManager;
    public TaskManager < GameController > taskManager;
    public InputController inputController;

    public Phase currentPhase;
    public GameMode gameMode;

    private bool initialized = false;

    public IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        inputController = new InputController(this);
        stateManager = new StateManager<GameController>(this);
        taskManager = new TaskManager < GameController >(this, gameObject);

        ConfigureStateManager();
        ChangeState(currentPhase);

        initialized = true;
    }

    public void Update()
    {
        if (!initialized)
            return;
        
        inputController.Update();
        stateManager.Update(Time.deltaTime);
    }

    public virtual void ConfigureStateManager()
    {
        stateManager.AddState(new InitializePhase());
        stateManager.AddState(new StartPhaseController());
        stateManager.AddState(new DrawPhaseController());
        stateManager.AddState(new MainPhaseController());
        stateManager.AddState(new EndPhaseController());
    }

    public virtual void ChangeState(Phase phase)
    {
        switch (phase)
        {
            case Phase.Initialize:
                stateManager.ChangeState < InitializePhase >();
                break;
            case Phase.Start: 
                stateManager.ChangeState < StartPhaseController >();
                break;
            case Phase.Draw:
                stateManager.ChangeState < DrawPhaseController >();
                break;
            case Phase.Main:
                stateManager.ChangeState < MainPhaseController >();
                break;
            case Phase.End:
                stateManager.ChangeState< EndPhaseController >();
                break;
        }
    }
}
