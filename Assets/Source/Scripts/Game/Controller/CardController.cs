﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DoozyUI;

public class CardController : MonoBehaviour
{
    [SerializeField] private Transform _baseItem;
    [SerializeField] private Toggle player0Toggle;

    private int gameId = -1;

    private ToggleGroup _toggleGroup;

    public event EventHandler < ActionEventArgs > ButtonClicked;

    private ToggleGroup ToggleGroup
    {
        get
        {
            if (_toggleGroup == null)
                _toggleGroup = GetComponent < ToggleGroup >();

            return _toggleGroup; 
        }
    }

    public Vector2 mousePosition
    {
        get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); }
    }

    public class ActionInQueue
    {
        public string cardName;
        public int boardId;
    }

    public ActionInQueue currentAction;

    public void Start()
    {
        SetDisplay();
    }

    public void SetDisplay()
    {
        ResetScrollLayout();

        foreach (var card in CollectionManager.Instance.config.CardCollection)
        {
            Transform item = Instantiate < Transform >(_baseItem);
            item.SetParent(transform, false);

            UIBaseItem itemComponent = item.GetComponent < UIBaseItem >();

            itemComponent.Initialize(card.Name, ToggleGroup);

            Dictionary < string , object > values;
            values = new Dictionary < string , object >();

            values.Add("Name", card.Name);
            values.Add("Description", card.Description);
            values.Add("Type", card.CardType);

            itemComponent.SetItem(values, OnClickedAction);
        }

    }

    public void Update()
    {
        if (currentAction == null)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            CellView view = BoardView.Instance.GetCellByWorldPosition(mousePosition);

            if (view != null)
            {
                currentAction.boardId = view.cellState.Id;
                DoAction();
            }
            else
                Debug.Log("cell view is null");

            currentAction = null;
        }
    }

    public void DoAction()
    {
        int ownerId = player0Toggle.isOn ? 0 : 1;

        CardState cardState = GameManager.Instance.CreateCardState(currentAction.cardName, ownerId);
        CommandSystem commandSystem = new CommandSystem();
        commandSystem.ExecutePlayerAction(cardState, currentAction.boardId, PlayerAction.PlayCard);

        ViewResolver.Instance.AddToCommandQueue(commandSystem);
    }

    private void ResetScrollLayout()
    {
        transform.DestroyChildren();
    }

    private void OnClickedAction(ActionEventArgs e)
    {
        currentAction = new ActionInQueue();
        currentAction.cardName = e.Property;

        //UIManager.HideUiElement("CardDebugMenu");
        
        if (ButtonClicked != null)
            ButtonClicked.Invoke(this, e);
    }
}
