﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEntryPoint : MonoBehaviour
{
    private int gameId = -1;

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
        CommandSystem commandSystem = new CommandSystem();

        CardState rein = GameManager.Instance.CreateCardState("Rein Bleu", 0);
        commandSystem.ExecutePlayerAction(rein, 7, PlayerAction.PlayCard);

        CardState earthTower = GameManager.Instance.CreateCardState("Earth Temple", 1);
        commandSystem.ExecutePlayerAction(earthTower, 10, PlayerAction.PlayCard);

        ViewResolver.Instance.AddToCommandQueue(commandSystem);

        yield return new WaitForSeconds(2f);

        CardState tikiCannon = GameManager.Instance.CreateCardState("Tiki Cannon", 0);
        commandSystem.ExecutePlayerAction(tikiCannon, 7, PlayerAction.PlayCard);

        ViewResolver.Instance.AddToCommandQueue(commandSystem);

        commandSystem.ExecutePlayerAction(rein, -1, PlayerAction.Attack);

        ViewResolver.Instance.AddToCommandQueue(commandSystem);

    }
}
