﻿using UnityEngine;
using System.Collections;

public class CharacterLoader : MonoBehaviour
{
    private CharacterEntity _characterEntity;

    private void Awake()
    {
        _characterEntity = GetComponent < CharacterEntity >();
    }

    public void Load(CharacterData characterData)
    {
        _characterEntity.SetCustomData(characterData);
    }
}
