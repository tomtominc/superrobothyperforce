﻿using UnityEngine;
using Framework.Core;
using System.Collections.Generic;
using Framework.Core.EventSystem;
using System.Linq;
using Framework.Core.Animation;
using System;

public class CharacterEntity : UnitEntity, IInputBehaviour
{
    [SerializeField] private SpriteAnimation _bodyAnimator;

    public override SpriteAnimation BodyAnimator { get { return _bodyAnimator; } }

    [SerializeField] private CharacterData _characterData;

    public Dictionary < SwipeDirection , Vector2 > _directionMap = new Dictionary<SwipeDirection, Vector2>()
    {
        { SwipeDirection.Down , Vector2.down },
        { SwipeDirection.Up , Vector2.up },
        { SwipeDirection.Right , Vector2.right },
        { SwipeDirection.Left , Vector2.left },
        { SwipeDirection.Touch , Vector2.zero }
    };

    public bool InverseControls { get { return false; } }

    public CharacterData CharacterData
    {
        get { return _characterData; }
    }

    public override void Initialize(IWorld world, string key)
    {
        base.Initialize(world, key);

        SetStatus(Status.Normal);

        EventManager.Publish(this, InputEvent.RegisterBehaviour, this, -1);

    }

    public void SetCustomData(CharacterData characterData)
    {
        _characterData = characterData;

        PlayAnimationByState(_characterData.animatorState);
    }

    public override void PlayAnimationByState(AnimatorState state, Action OnComplete = null)
    {
        if (_characterData == null)
            return;

        _characterData.animatorState = state;

        if (BodyAnimator.currentAnimationName == null ||
            !BodyAnimator.currentAnimationName.Equals(state.ToString()))
        {
            if (state != AnimatorState.OnIdle)
            {
                BodyAnimator.PlayWithCallBack(state.ToString(), (x, y) =>
                    {
                        PlayAnimationByState(AnimatorState.OnIdle);
                    });
            }
            else
            {
                BodyAnimator.Play(state.ToString());
            }
           
        }
            
    }


    protected override void SetData(string key)
    {
        _health = 200;

        if (_healthGUI)
            _healthGUI.Initialize(_health);
    }

    private void OnDisable()
    {
        EventManager.Publish(this, InputEvent.DeregisterBehaviour, this);
    }

    protected override void OnBattleStart(IMessage message)
    {
        _update = true;
    }

    public void HandleMobileInput(SwipeDirection direction)
    {
        if (_characterData == null || _characterData.state != CharacterData.State.Battle || !_update)
            return;
        
        Vector2 offset = _directionMap[direction];

        PlayAnimationByState(AnimatorState.OnMove);

        Move(_coordinate + offset, 0f, MoveTweenType.Linear, null);
    }


    protected override void DoUpdate()
    {
        base.DoUpdate();
    }

    public override void RemoveFromWorld()
    {
        EventManager.Publish(this, BattleEvent.OnCharacterDestroyed, null);
        base.RemoveFromWorld();
    }
}
