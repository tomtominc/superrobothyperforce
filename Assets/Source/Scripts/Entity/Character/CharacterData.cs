﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterData
{
    public enum State
    {
        View,
        Battle
    }

    public State state;
    public UnitEntity.AnimatorState animatorState;

    public CharacterData()
    {
        
    }

    public CharacterData(State state, UnitEntity.AnimatorState animatorState)
    {
        this.state = state;
        this.animatorState = animatorState;
    }
}
