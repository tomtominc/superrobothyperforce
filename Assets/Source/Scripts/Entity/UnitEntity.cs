﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor.Sprites;
using System;

public enum Status
{
    Normal,
    Spawn,
    Hit,
    Blink,
    Stun
}

// should seperate damage from component
public class UnitEntity : Entity2D, IDamagable
{
    public enum AnimatorState
    {
        OnIdle,
        OnSpawn,
        OnDamage,
        OnAttack,
        OnSwordAttack,
        OnFlingAttack,
        OnShootAttack,
        OnCannonAttack,
        OnMove,

    }

    public bool Invincible
    { 
        get { return _invincible; }
    }

    protected bool _invincible = false;
    protected Status _status;
    protected Status _lastStatus;

    protected int _health;

    public virtual int Health
    {
        get { return _health; }
        set { _health = value; }
    }

    public override void Initialize(IWorld world, string key)
    {
        InitializeHealth();
        base.Initialize(world, key);
       

    }

    public virtual void InitializeHealth()
    {
        _healthGUI = GetComponentInChildren < HealthGUI >(true);
    }

    public virtual void Damage(DamageInfo damageInfo)
    {
        Health = Health - damageInfo.amount;

        if (Health <= 0)
            Health = 0;

        SetHealth(Health);

        //UpdateStatus(damageInfo.status); 
    }


    public virtual void SetHealth(int health)
    {
        _health = health;

        EventManager.Publish(this, EntityEvent.OnChangedHealth, this);

        if (health <= 0)
        {
            EventManager.Publish(this, EntityEvent.OnDeath, this);
            RemoveFromWorld();
        }

        if (_healthGUI)
            _healthGUI.SetHealth(_health);
    }

    public virtual void UpdateStatus(Status status)
    {
        _lastStatus = status;

        SetStatus(status);

        var table = new Dictionary < string, object >()
        {
            { "Status", _status },
            { "LastStatus", _lastStatus },
            { "Entity", this    }
        };

        EventManager.Publish(this, EntityEvent.OnUpdateStatus, table);
    }

    protected virtual void SetStatus(Status status)
    {
        _status = status;

        switch (_status)
        {
            case Status.Spawn:
                DoSpawnUpdate();
                break;
            case Status.Normal:
                DoNormalUpdate();
                break;
            case Status.Hit:
                DoHitUpdate();
                break;
            case Status.Blink:
                DoBlinkUpdate();
                break;
            case Status.Stun:
                DoStunUpdate();
                break;
        }
    }

    public virtual bool ContainsAnimation(AnimatorState state)
    {
        return BodyAnimator.animationsByName.ContainsKey(state.ToString());
    }

    public virtual void PlayAnimationByState(AnimatorState state, Action OnComplete = null)
    {
        string animation = string.Format("{0}", state);

        if (!BodyAnimator.currentAnimationName.Equals(animation))
        {
            BodyAnimator.PlayWithCallBack(animation, (x, y) =>
                {
                    if (OnComplete != null)
                        OnComplete();
                });
        }
    }

    public virtual void PlayAnimationByName(object arg, Action callback)
    {
        string animation = string.Format("{0}_{1}", name, arg);

        BodyAnimator.PlayWithCallBack(animation, (x, y) =>
            {
                if (callback != null)
                    callback();
            });
    }

    protected virtual void DoSpawnUpdate()
    {
        
    }

    protected virtual void DoNormalUpdate()
    {
        PlayAnimationByState(AnimatorState.OnIdle);
    }

    protected virtual void DoHitUpdate()
    {
        _update = false;
        _invincible = true;
        PlayAnimationByState(AnimatorState.OnDamage);

        HSVUtility.Damage(_renderer, 0.5f, () =>
            {
                _update = true;
                _invincible = false;

                SetStatus(Status.Normal);
            });
    }

    protected virtual void DoBlinkUpdate()
    {
        _update = false;
        _invincible = true;
        PlayAnimationByState(AnimatorState.OnDamage);


        HSVUtility.DamageWithInvincibility(_renderer, 0.5f, 10, () =>
            {
                PlayAnimationByState(AnimatorState.OnIdle);
                _update = true;
            }, () =>
            {
                _invincible = false;
                SetStatus(Status.Normal);
            });
      
    }

    protected virtual void DoStunUpdate()
    {
        
    }
}
