﻿using UnityEngine;
using System;

public class WeaponStats
{
    public Vector2 direction;
    public LayerMask collision;
    public LayerMask trigger;

    public WeaponStats(Vector2 direction, LayerMask collision, LayerMask trigger)
    {
        this.direction = direction;
        this.collision = collision;
        this.trigger = trigger;
    }

    public WeaponStats()
    {
        
    }
}

public class CollisionInfo
{
    public IEntity hitTarget;
    public Vector2 point;
    public IEntity bullet;
    public DamageInfo damageInfo;
    public bool countered;
    public bool triggered;

    public CollisionInfo(IEntity hitTarget, IEntity bullet, Vector2 point, DamageInfo damageInfo, bool countered, bool triggered)
    {
        this.hitTarget = hitTarget;
        this.bullet = bullet;
        this.point = point;
        this.damageInfo = damageInfo;
        this.countered = countered;
        this.triggered = triggered;
    }
}

public class Weapon
{
    protected IWorld _world;

    public Weapon(IWorld world, object data)
    {
        _world = world;
    }

    public virtual void Shoot(EntityData data, BulletInfo damage, Action < CollisionInfo > onCollide)
    {
        IEntity entity = _world.SpawnEntity(data);
        
    }

}