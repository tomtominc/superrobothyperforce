﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Framework.Core.EventSystem;

public enum FactoryType
{
    Single,
    Continuous,
}

public class Factory
{
    protected FactoryType _factoryType;
    protected int _offset = 1;
    protected float _delay = 0.5f;

    protected float _currentTime;
    protected int _current;
    protected List<BulletInfo> _datas;
    protected Action<Factory> _onFinishedHandler;
    protected IEntity _entity;

    public Factory(IEntity entity, List <BulletInfo> datas, FactoryType factoryType, 
                   float delay, Action<Factory> onFinish)
    {
        _entity = entity;
        _datas = new List<BulletInfo>(datas);
        _factoryType = factoryType;
        _offset = 1;
        _delay = delay;
        _onFinishedHandler = onFinish;
    }

    public virtual void Create()
    {
        List<BulletInfo> removals = new List<BulletInfo>();

        foreach (var data in _datas)
        {
            Vector2 cell = data.creationCoordinate + (data.direction * (_offset + _current));

            if (!_entity.World.IsInWorldBounds(cell))
            {
                removals.Add(data);
                continue;
            }

            EntityData entityData = new EntityData(data.key, EntityType.Bullet, cell);
            IEntity entity = _entity.World.SpawnEntity(entityData);
        }

        foreach (var data in removals)
        {
            _datas.Remove(data);
        }

        if (_datas.Count <= 0 || _factoryType == FactoryType.Single)
        {
            RemoveFromWorld();
        }

        _current++;
    }

    public virtual void Update(float deltaTime)
    {
        _currentTime += deltaTime;

        if (_currentTime < _delay)
            return;

        _currentTime = 0;

        Create();
    }

    public virtual void RemoveFromWorld()
    {
        if (_onFinishedHandler != null)
            _onFinishedHandler.Invoke(this);
    }

    public virtual void OnCollide(CollisionInfo info)
    {

    }
}
