﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Runtime.InteropServices;

[CustomPropertyDrawer(typeof(FSMAttackState))]
public class FSMAttackStateEditor : PropertyDrawer
{
    Rect _position;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int propNumber = 20;
        var customProjectileData = property.FindPropertyRelative("customProjectileData");

        if (customProjectileData.boolValue)
        {
            propNumber += 2;
        }

        var spawnBehaviour = property.FindPropertyRelative("spawnBehaviour");

        if (spawnBehaviour.enumValueIndex == 0)
        {
            propNumber += 2;
        }

        return base.GetPropertyHeight(property, label) * propNumber;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var initialState = property.FindPropertyRelative("initialState");
        var attackStyle = property.FindPropertyRelative("attackStyle");
        var customProjectileData = property.FindPropertyRelative("customProjectileData");
        var projectileKey = property.FindPropertyRelative("projectileKey");
        var damage = property.FindPropertyRelative("damage");
        var element = property.FindPropertyRelative("element");
        var spawnBehaviour = property.FindPropertyRelative("spawnBehaviour");
        var gridOffset = property.FindPropertyRelative("gridOffset");
        var pixelOffset = property.FindPropertyRelative("pixelOffset");
        var attackCount = property.FindPropertyRelative("attackCount");
        var attackDelay = property.FindPropertyRelative("attackDelay");
        var attackFrame = property.FindPropertyRelative("attackFrame");
        var nextState = property.FindPropertyRelative("nextState");     

        // Get the initial rect
        _position = position;
        _position.height = 16f;

        // general properties ===================
        EditorGUI.LabelField(_position, "Attack Properties", new GUIStyle("OL Title"));

        _position.y += 32f;
        EditorGUI.PropertyField(_position, initialState);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, attackStyle);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, projectileKey);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, customProjectileData);

        if (customProjectileData.boolValue)
        {
            EditorGUI.indentLevel++;

            _position.y += 16f;
            EditorGUI.PropertyField(_position, damage);
            _position.y += 16f;
            EditorGUI.PropertyField(_position, element);

            EditorGUI.indentLevel--;

        }

        _position.y += 16f;
        EditorGUI.PropertyField(_position, attackDelay);

        _position.y += 32f;
        EditorGUI.LabelField(_position, "Spawn Properties", new GUIStyle("OL Title"));
        _position.y += 32f;

        EditorGUI.PropertyField(_position, spawnBehaviour);

        if (spawnBehaviour.enumValueIndex == 0)
        {
            EditorGUI.indentLevel++;

            _position.y += 16f;
            EditorGUI.PropertyField(_position, gridOffset);

            _position.y += 16f;
            EditorGUI.PropertyField(_position, pixelOffset);

            EditorGUI.indentLevel--;
        }


        _position.y += 32f;
        // wait properties ===================
        EditorGUI.LabelField(_position, "Animation Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, attackFrame);

        _position.y += 32f;
        // exit properties ===================
        EditorGUI.LabelField(_position, "Exit State Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, nextState);

        EditorGUI.EndProperty();

    }
}