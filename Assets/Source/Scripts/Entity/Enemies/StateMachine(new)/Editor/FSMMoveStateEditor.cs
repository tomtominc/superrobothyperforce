﻿
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(FSMMoveState))]
public class FSMMoveStateEditor : PropertyDrawer
{
    Rect _position;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var movementStyle = property.FindPropertyRelative("movementStyle");

        int propNumber = 21;

        if (movementStyle.enumValueIndex == 3)
        {
            propNumber += 1;
        }

        return base.GetPropertyHeight(property, label) * propNumber;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        var initialState = property.FindPropertyRelative("initialState");
        var movementStyle = property.FindPropertyRelative("movementStyle");
        var pathType = property.FindPropertyRelative("pathType");
        var movementAnimation = property.FindPropertyRelative("movementAnimation");
        var moveCount = property.FindPropertyRelative("moveCount");
        var moveDuration = property.FindPropertyRelative("moveDuration");
        var moveDelay = property.FindPropertyRelative("moveDelay");
        var stepStyle = property.FindPropertyRelative("stepStyle");
        var stepLimit = property.FindPropertyRelative("stepLimit");
        var gridLimit = property.FindPropertyRelative("gridLimit");
        var restrictions = property.FindPropertyRelative("restrictions");
        var onSuccess = property.FindPropertyRelative("onSuccess");
        var onFailure = property.FindPropertyRelative("onFailure");       

        // Get the initial rect
        _position = position;
        _position.height = 16f;

        // general properties ===================
        EditorGUI.LabelField(_position, "Movement Properties", new GUIStyle("OL Title"));

        _position.y += 32f;
        EditorGUI.PropertyField(_position, initialState);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, movementStyle);

        if (movementStyle.enumValueIndex == 3)
        {
            EditorGUI.indentLevel++;

            _position.y += 16f;
            EditorGUI.PropertyField(_position, pathType);

            EditorGUI.indentLevel--;
        }

        _position.y += 16f;
        EditorGUI.PropertyField(_position, movementAnimation);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, moveCount);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, moveDuration);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, moveDelay);

        _position.y += 32f;
        // wait properties ===================
        EditorGUI.LabelField(_position, "Step Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, stepStyle);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, stepLimit);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, gridLimit);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, restrictions);

        _position.y += 32f;
        // exit properties ===================
        EditorGUI.LabelField(_position, "Exit State Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, onSuccess);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, onFailure);

        EditorGUI.EndProperty();

    }
}