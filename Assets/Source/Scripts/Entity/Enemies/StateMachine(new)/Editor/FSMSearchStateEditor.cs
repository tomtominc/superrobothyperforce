﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(FSMSearchState))]
public class FSMSearchStateEditor : PropertyDrawer
{
    Rect _position;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int propNumber = 16;
        return base.GetPropertyHeight(property, label) * propNumber;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

 

        var initialState = property.FindPropertyRelative("initialState");
        var searchStyle = property.FindPropertyRelative("searchStyle");
        var searchDuration = property.FindPropertyRelative("searchDuration");
        var numberOfTargets = property.FindPropertyRelative("numberOfTargets");
        var waitDuration = property.FindPropertyRelative("waitDuration");
        var onSuccess = property.FindPropertyRelative("onSuccess");
        var onFailure = property.FindPropertyRelative("onFailure");       

        // Get the initial rect
        _position = position;
        _position.height = 16f;

        // general properties ===================
        EditorGUI.LabelField(_position, "General Properties", new GUIStyle("OL Title"));

        _position.y += 32f;
        EditorGUI.PropertyField(_position, initialState);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, searchStyle);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, searchDuration);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, numberOfTargets);

        _position.y += 32f;
        // wait properties ===================
        EditorGUI.LabelField(_position, "Wait Task Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, waitDuration);

        _position.y += 32f;
        // exit properties ===================
        EditorGUI.LabelField(_position, "Exit State Properties", new GUIStyle("OL Title"));
        _position.y += 32f;
        EditorGUI.PropertyField(_position, onSuccess);
        _position.y += 16f;
        EditorGUI.PropertyField(_position, onFailure);

        EditorGUI.EndProperty();

    }
}
