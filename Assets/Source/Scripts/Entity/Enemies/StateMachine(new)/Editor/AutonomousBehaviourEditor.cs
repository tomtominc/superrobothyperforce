﻿using UnityEditor;
using UnityEngine;
using RutCreate.LightningDatabase;
using System.Collections.Generic;
using Framework.Core;

[CustomEditor(typeof(AutonomousBehaviour))]
public class AutonomousBehaviourEditor : Editor
{
    AutonomousBehaviour _behaviour;

    private void OnEnable()
    {
        _behaviour = (AutonomousBehaviour)target;  
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        int selected = EditorGUIExtensions.DrawSelectableHeader("AutonomousBehaviour_MainHeader", EditorColor.greenyellow, "General", "State Machine");

        EditorGUIExtensions.BeginContent(Color.white);

        switch (selected)
        {
            case 0:
                DrawGeneralProperties();
                break;

            case 1:
                DrawStateMachineProperties();
                break;
        }

        EditorGUIExtensions.EndContent();

        serializedObject.ApplyModifiedProperties();
    }

    public SerializedProperty GetProp(string field)
    {
        return serializedObject.FindProperty(field);
    }


    public void DrawGeneralProperties()
    {
        EditorGUILayout.Space();

        EditorGUIExtensions.DrawTitle("Autonomous Definition");

        EditorGUILayout.Space();

        SerializedProperty autonomousType = GetProp("autonomousType");

        EditorGUILayout.PropertyField(autonomousType);
        EditorGUILayout.PropertyField(GetProp("animationBehaviour"));

        switch (autonomousType.enumValueIndex)
        {
            case 0:
                DrawAutonoumousProperties();
                break;

            case 1:
                DrawProjectileProperties();
                break;

            case 2:
                break;
        }
    }

    public void DrawAutonoumousProperties()
    {
        EditorGUILayout.PropertyField(GetProp("autonomousKey"));
    }

    public void DrawProjectileProperties()
    {
        EditorGUILayout.PropertyField(GetProp("projectileKey"));

        EditorGUILayout.Space();

        EditorGUIExtensions.DrawTitle("Projectile Properties");

        EditorGUILayout.Space();

        var collisionUpdate = GetProp("collisionUpdate");
        EditorGUILayout.PropertyField(collisionUpdate);

        if (collisionUpdate.enumValueIndex == 0)
            return;

        var limitCollisionsToFrames = GetProp("limitCollisionsToFrames");
        EditorGUILayout.PropertyField(limitCollisionsToFrames);

        if (limitCollisionsToFrames.boolValue)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(GetProp("collisionFrames"));
            EditorGUI.indentLevel--;
        }

        var isCounterable = GetProp("isCounterable");
        EditorGUILayout.PropertyField(isCounterable);

        if (isCounterable.boolValue)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(GetProp("counterFrames"));
            EditorGUI.indentLevel--;
        }

        var onCollisionEffect = GetProp("onCollisionEffect");
        EditorGUILayout.PropertyField(onCollisionEffect);

        if (onCollisionEffect.enumValueIndex == 1)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(GetProp("damagableKey"));
            EditorGUI.indentLevel--;
        }

        EditorGUILayout.PropertyField(GetProp("despawnBehaviour"));
    }


    public void DrawStateMachineProperties()
    {
        EditorGUILayout.Space();

        var initialStateValue = serializedObject.FindProperty("initialStateValue");

        EditorGUILayout.PropertyField(initialStateValue);

        int selected = EditorGUIExtensions.DrawSelectableHeader("AutonomousBehaviour_StateMachineHeader", EditorColor.greenyellow, "Search", "Movement", "Attack");

        EditorGUIExtensions.BeginContent(Color.white);

        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

        int index = EditorPrefs.GetInt(string.Format("State_{0}", selected));

        EditorGUILayout.LabelField(string.Format("[{0}]", index), EditorStyles.miniLabel);


        if (GUILayout.Button("<", EditorStyles.toolbarButton))
        {
            index = index.Wrap(-1, GetCurrentCount(selected));
        }

        if (GUILayout.Button(">", EditorStyles.toolbarButton))
        {
            index = index.Wrap(1, GetCurrentCount(selected));
        }

        if (GUILayout.Button("-", EditorStyles.toolbarButton))
        {
            RemoveByIndex(selected, index);
        }

        if (GUILayout.Button("+", EditorStyles.toolbarButton))
        {
            AddToStates(selected);
        }

        EditorGUILayout.EndHorizontal();

        if (index >= GetCurrentCount(selected))
        {
            index = 0;
        }

        EditorGUILayout.Space();

        switch (selected)
        {
            case 0:
                DrawSearchStates(index);
                break;
            case 1:
                DrawMovementStates(index);
                break;
            case 2: 
                DrawAttackStates(index);
                break;
        }

        EditorPrefs.SetInt(string.Format("State_{0}", selected), index);

        EditorGUIExtensions.EndContent();
    }

    public void DrawSearchStates(int index)
    {
        SerializedProperty propArray = serializedObject.FindProperty("searchStates");

        if (propArray.arraySize < 0 || propArray.arraySize <= index)
        {
            EditorGUILayout.HelpBox("No valid states", MessageType.Warning);
            return;
        }

        EditorGUILayout.PropertyField(propArray.GetArrayElementAtIndex(index));
    }

    public void DrawMovementStates(int index)
    {
        SerializedProperty propArray = serializedObject.FindProperty("moveStates");

        if (propArray.arraySize < 0 || propArray.arraySize <= index)
        {
            EditorGUILayout.HelpBox("No valid states", MessageType.Warning);
            return;
        }

        EditorGUILayout.PropertyField(propArray.GetArrayElementAtIndex(index));
    }

    public void DrawAttackStates(int index)
    {
        SerializedProperty propArray = serializedObject.FindProperty("attackStates");

        if (propArray.arraySize < 0 || propArray.arraySize <= index)
        {
            EditorGUILayout.HelpBox("No valid states", MessageType.Warning);
            return;
        }

        EditorGUILayout.PropertyField(propArray.GetArrayElementAtIndex(index));
    }

    public int GetCurrentCount(int selected)
    {
        int count = 0;

        switch (selected)
        {
            case 0:
                count = _behaviour.searchStates.Count;
                break;

            case 1:
                count = _behaviour.moveStates.Count;
                break;

            case 2:
                count = _behaviour.attackStates.Count;
                break;
        }

        return count;
    }

    public void RemoveByIndex(int selected, int index)
    {
        if (GetCurrentCount(selected) <= 0)
        {
            return;
        }

        switch (selected)
        {
            case 0:
                _behaviour.searchStates.RemoveAt(index);
                break;

            case 1:
                _behaviour.moveStates.RemoveAt(index);
                break;

            case 2:
                _behaviour.attackStates.RemoveAt(index);
                break;
        }
    }

    public int AddToStates(int selected)
    {
        int returnIndex = 0;

        switch (selected)
        {
            case 0:
                _behaviour.searchStates.Add(new FSMSearchState());
                returnIndex = _behaviour.searchStates.Count - 1;
                break;

            case 1:
                _behaviour.moveStates.Add(new FSMMoveState());
                returnIndex = _behaviour.moveStates.Count - 1;
                break;

            case 2:
                _behaviour.attackStates.Add(new FSMAttackState());
                returnIndex = _behaviour.attackStates.Count - 1;
                break;
        }

        return returnIndex;
    }
}
