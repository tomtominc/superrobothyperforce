﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TaskManager;

public class WaitTask : Task<AutonomousBehaviour>
{
    private float _waitTime;
    private float _currentTime = 0;

    public WaitTask(float delay)
    {
        _waitTime = delay;    
    }

    public override void TaskStart()
    {
        _currentTime = 0;
    }

    public override void TaskUpdate()
    {
        _currentTime += Time.deltaTime;

        if (_currentTime >= _waitTime)
            Complete();

    }

}
