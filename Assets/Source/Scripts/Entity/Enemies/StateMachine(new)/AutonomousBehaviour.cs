﻿using StateManager;
using TaskManager;
using System.Collections.Generic;
using System;
using UnityEngine;
using Framework.Core.EventSystem;
using Gamelogic.Grids;

public class AutonomousBehaviour : UnitEntity
{
    public enum AutonomousType
    {
        Enemy,
        Projectile,
        Other
    }

    public enum CollisionUpdate
    {
        Disabled,
        Collide,
        Trigger,
        CollideAndTrigger
    }

    public enum OnCollisionEffect
    {
        UseEffectManager,
        SpawnDamagable,
        FinishAnimation,
        UseHitAnimation
    }

    public enum DespawnBehaviour
    {
        OnCollision,
        AfterAnimation
    }

    public enum AnimationBehaviour
    {
        AlwaysStartStateInIdle,
        UseNameAndElementOnly
    }

    protected internal StateManager < AutonomousBehaviour > _stateManager;
    protected internal TaskManager < AutonomousBehaviour > _taskManger;


    /// <summary>
    /// Holds things like Name, Description, Element, Health, Power, Speed 
    /// This keeps everything consistent throughout the game and can be used
    /// generically for UI purposes as well as changed on a server
    /// for balancing. This does not hold behaviour and if behaviour is changed 
    /// this would be through an update not on the server.
    /// </summary>
    public AutonomousKey autonomousKey;

    /// <summary>
    /// Holds things like Name, Description, Element, Health, Power, Speed 
    /// This keeps everything consistent throughout the game and can be used
    /// generically for UI purposes as well as changed on a server
    /// for balancing. This does not hold behaviour and if behaviour is changed 
    /// this would be through an update not on the server.
    /// </summary>
    public ProjectileKey projectileKey;

    public StateValue initialStateValue;
    public AutonomousType autonomousType;
    public AnimationBehaviour animationBehaviour;


    public List < FSMSearchState > searchStates = new List < FSMSearchState >();
    public List < FSMMoveState > moveStates = new List < FSMMoveState >();
    public List < FSMAttackState > attackStates = new List < FSMAttackState >();

    // For the AI Manager to grab a callback when finished ====
    protected Action < AutonomousBehaviour > _finished;

    // Properties for FSM Behaviours ==========================

    // the current state of the autonomous behaviour
    protected StateValue _currentStateValue;
    // this list gets filled in the search state and used in the attack state
    // for projectiles to find their target
    protected internal List < Vector2 > _searchTargets;

    // For Projectile autonomous types only ===================
    // what kind of collisions need to be updated on this object
    public CollisionUpdate collisionUpdate;

    // if the collision ray is 0 it will just collide when it's on the same
    // tile as the player / enemy
    public float collisionRayLength = 0f;

    // can the bullet be countered?
    public bool isCounterable = false;
    // hide this in the inspector if isCounterable is false
    public Vector2 counterFrames = new Vector2(-1f, -1f);

    // animation collision properties
    public bool limitCollisionsToFrames = false;
    public Vector2 collisionFrames = new Vector2(-1f, -1f);

    public OnCollisionEffect onCollisionEffect;

    // collision behaviour spawn damagable
    public DatabaseKey damagableKey;

    public DespawnBehaviour despawnBehaviour;

    public ProjectileData projectileData;

    protected CollisionBehaviour _collisionBehaviour;

    protected bool _alreadyDidCollide;

  
    public override void Initialize(IWorld world, string key)
    {
        base.Initialize(world, key);

        _update = false;

        if (_stateManager == null)
        {
            _taskManger = new TaskManager<AutonomousBehaviour>(this, gameObject);
            _stateManager = new StateManager<AutonomousBehaviour>(this);
        }

        if (autonomousType == AutonomousType.Enemy)
            EventManager.Publish(this, AutonomousEvent.Register, this);
    }

    public void InitializeAnimation(AnimationBehaviour behaviour)
    {
        switch (behaviour)
        {
            case AnimationBehaviour.AlwaysStartStateInIdle:
                PlayAnimationByState(AnimatorState.OnIdle);
                break;
            case AnimationBehaviour.UseNameAndElementOnly:
                PlayAnimationByName(projectileData.element, () =>
                    {
                        if (despawnBehaviour == DespawnBehaviour.AfterAnimation)
                            RemoveFromWorld();
                    });
                break;
        }
    }

    public virtual void StartAgent(Action < AutonomousBehaviour > finished)
    {
        _finished = finished;
        _update = true;

        ChangeState(initialStateValue);
    }

    public virtual void StopAgent()
    {
        _finished = null;
        _update = false;
    }

    public virtual void StartProjectile(ProjectileData projectileData)
    {
        this.projectileData = projectileData;

        InitializeAnimation(animationBehaviour);

        _update = true;

        _searchTargets = new List<Vector2>(projectileData.targets);

        _collisionBehaviour = new CollisionBehaviour(transform, Collide);

        ChangeState(initialStateValue);
    }

    protected override void DoUpdate()
    {
        if (!_update || _currentStateValue == null)
            return;


        if (_currentStateValue.type != StateType.None)
        {
            _stateManager.Update(Time.deltaTime);
        }

        if (autonomousType == AutonomousType.Projectile)
        {
            ProjectileCollisionUpdate();
        }
       
    }

    public virtual void ProjectileCollisionUpdate()
    {
        if (collisionUpdate == CollisionUpdate.Disabled || _collisionBehaviour == null)
            return;
        
        _collisionBehaviour.DoUpdate(Time.deltaTime);
    }

    public virtual void Collide(CollisionInfo collisionInfo)
    {
        if (_alreadyDidCollide)
            return;

        _alreadyDidCollide = true;

        if (collisionInfo.triggered)
        {
            TriggerTarget(collisionInfo.hitTarget.Transform, collisionInfo);
        }

        if (collisionInfo.countered && isCounterable)
        {
            // not supported
        }

        IDamagable damagable = collisionInfo.hitTarget.Transform.GetComponent < IDamagable >();

        if (damagable == null || damagable.Invincible)
            return;

        _update = false;

        damagable.Damage(collisionInfo.damageInfo);

        DoCollisionEffect(collisionInfo.point);

    }

    public virtual void DoCollisionEffect(Vector2 point)
    {
        if (!gameObject.activeSelf)
            return;

        switch (onCollisionEffect)
        {
            case OnCollisionEffect.UseEffectManager:
                break;

            case OnCollisionEffect.UseHitAnimation:
                break;
        }
    }

    public virtual void TriggerTarget(Transform target, CollisionInfo collisionInfo)
    {
        IDamagable damagable = target.GetComponent < IDamagable >();
        damagable.Damage(collisionInfo.damageInfo);

        ITrigger trigger = target.GetComponent < ITrigger >();
        trigger.Trigger();
    }


    public virtual void ChangeState(StateValue value)
    {
        _currentStateValue = new StateValue(value.type, value.index);

        if (_currentStateValue.type == StateType.Finish)
        {
            Finish();
            return;
        }

        switch (_currentStateValue.type)
        {
            case StateType.Search:
                _stateManager.ChangeState<FSMSearchState>(searchStates[_currentStateValue.index]);
                break;
            
            case StateType.Move: 
                _stateManager.ChangeState<FSMMoveState>(moveStates[_currentStateValue.index]);
                break;

            case StateType.Attack:
                _stateManager.ChangeState<FSMAttackState>(attackStates[_currentStateValue.index]);
                break;
        }
    }

   

    public virtual void Finish()
    {
        if (_finished != null)
            _finished.Invoke(this);

        if (autonomousType == AutonomousType.Projectile)
        {
            RemoveFromWorld();
        }
           
    }

    public override void RemoveFromWorld()
    {
        if (autonomousType == AutonomousType.Enemy)
            EventManager.Publish(this, AutonomousEvent.Deregister, this);

        base.RemoveFromWorld();
        
    }
   
}

public class ProjectileData
{
    public int damage;
    public Element element;
    public List < Vector2 > targets;
    public LayerMask collisionLayer;
    public LayerMask triggerLayer;

    public ProjectileData()
    {
        damage = 0;
        element = Element.Normal;
        targets = new List < Vector2 >();
    }

    public ProjectileData(int damage, Element element, List < Vector2 > targets, 
                          LayerMask collisionLayer, LayerMask triggerLayer)
    {
        this.damage = damage;
        this.element = element;
        this.targets = targets;
        this.collisionLayer = collisionLayer;
        this.triggerLayer = triggerLayer;
    }

    public ProjectileData(ProjectileData copy)
    {
        this.damage = copy.damage;
        this.element = copy.element;
        this.targets = new List < Vector2 >(copy.targets);
        this.collisionLayer = copy.collisionLayer;
        this.triggerLayer = copy.triggerLayer;
    }
}
