﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TaskManager;

public class FleeMoveTask : MoveTask
{
    public FleeMoveTask(FSMMoveState behaviour)
        : base(behaviour)
    {
        currentMoveState = MoveState.Moving;   
    }

    public override void TaskStart()
    {
        Vector2 target = Flee();

        _behaviour.context.Move(target, _behaviour.moveDuration, _behaviour.movementAnimation, OnFinished);    
    }

    private Vector2 Flee()
    {
        List<Vector2> points = GetGridLimits();

        points.Remove(_behaviour.context.Coordinate);

        points.RemoveAll(vector => vector.y == _behaviour.context.CharacterCoordinates.y);

        if (points.Count <= 0)
        {
            Debug.LogWarningFormat("Returning no valid points when trying to move: {0}", _behaviour.context.name);

            return _behaviour.context.Coordinate;
        }
           
        Vector2 point = points[UnityEngine.Random.Range(0, points.Count)];

        return point;
    }

   
}
