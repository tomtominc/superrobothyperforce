﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TaskManager;

public class MoveTask : Task<AutonomousBehaviour>
{
    public enum MoveState
    {
        Moving,
        Finish
    }

    protected FSMMoveState _behaviour;

    public bool isSuccessful;

    public MoveState currentMoveState;

    public MoveTask(FSMMoveState  behaviour)
    {
        _behaviour = behaviour;
    }

    public override void TaskUpdate()
    {
        if (currentMoveState == MoveState.Finish)
            Complete();
    }

    protected List<Vector2> GetGridLimits()
    {
        Rect limits = _behaviour.context.World.MaxGrid;

        switch (_behaviour.gridLimit)
        {
            case FSMMoveState.GridLimit.EnemySpace:
                limits = _behaviour.context.World.GetBounds(LayerManager.AutonomousLayer);
                break;
            case FSMMoveState.GridLimit.PlayerSpace:
                limits = _behaviour.context.World.GetBounds(LayerManager.CharacterLayer);
                break;
        }

        List<Vector2> points = limits.Points();

        switch (_behaviour.stepLimit)
        {
            case FSMMoveState.StepLimit.Column:
                points.RemoveAll(vector => vector.x != _behaviour.context.Coordinate.x);
                break;
            case FSMMoveState.StepLimit.Row:
                points.RemoveAll(vector => vector.y != _behaviour.context.Coordinate.y);
                break;
        }

        //FIX: something about this is giving a null reference exception
        switch (_behaviour.restrictions)
        {
            case FSMMoveState.Restrictions.OccupiedSpaces:
                //points.RemoveAll(vector => !_behaviour.context.World.IsMoveValid(_behaviour.context, vector));
                break;
        }

        switch (_behaviour.stepStyle)
        {
            case FSMMoveState.StepStyle.Step:
                points.RemoveAll(vector => Vector2.Distance(_behaviour.context.Coordinate, vector) > 1f);
                break;
        }

        return points;
    }

    protected Vector2 GetCharacterOffset()
    {
        return (_behaviour.context.CharacterCoordinates - _behaviour.context.Coordinate);
    }

    protected virtual void OnFinished(bool moved)
    {
        isSuccessful = moved;
        currentMoveState = MoveState.Finish;

    }

}
