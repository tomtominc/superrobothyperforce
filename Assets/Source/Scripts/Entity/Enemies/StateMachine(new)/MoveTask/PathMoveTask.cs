﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathMoveTask : MoveTask
{
    public PathMoveTask(FSMMoveState behaviour)
        : base(behaviour)
    {
        currentMoveState = MoveState.Moving;   
    }


    public override void TaskStart()
    {
        if (context.autonomousType == AutonomousBehaviour.AutonomousType.Projectile)
        {
            Debug.Log(context._searchTargets[0]);

            context.World.SetTargetCell(context._searchTargets[0], true);
        }

        context.Move(context._searchTargets[0], _behaviour.moveDuration, _behaviour.movementAnimation, OnFinished);
    }

    protected override void OnFinished(bool moved)
    {
        context.World.SetTargetCell(context._searchTargets[0], false);
        base.OnFinished(moved);
    }
}
