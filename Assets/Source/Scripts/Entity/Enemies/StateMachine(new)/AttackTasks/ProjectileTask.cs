﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RutCreate.LightningDatabase;

public class ProjectileTask : AttackTask
{
    private bool _waitForEnd = false;

    public ProjectileTask(FSMAttackState behaviour)
        : base(behaviour)
    {
        
    }

    public override void TaskStart()
    {
        if (_behaviour.context.ContainsAnimation(UnitEntity.AnimatorState.OnAttack))
        {
            _waitForEnd = true;

            _behaviour.context.PlayAnimationByState(UnitEntity.AnimatorState.OnAttack, () =>
                {
                    _currentState = State.Finish;
                });
        }
       
    }

    public override void TaskUpdate()
    {
        if (_behaviour.context.CurrentFrame < _behaviour.attackFrame)
            return;

        if (_currentState == State.Wait)
        {
            ProjectileData projectileData = GetProjectileData();
            Vector2 projectileSpawnPoint = GetProjectileSpawnPoint();

            string projectileName = _behaviour.projectileKey.value;

            EntityData entityData = new EntityData(projectileName, EntityType.Projectile, projectileSpawnPoint);

            AutonomousBehaviour projectile = (AutonomousBehaviour)_behaviour.context.World.SpawnEntity(entityData);

            projectile.StartProjectile(projectileData);


            _currentState = _waitForEnd ? State.WaitForAnimationEnd : State.Finish;
        }
        else if (_currentState == State.Finish)
        {
            Complete();
        }
    }
}
