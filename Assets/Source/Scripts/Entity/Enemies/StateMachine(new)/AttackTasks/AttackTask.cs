﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TaskManager;
using RutCreate.LightningDatabase;

public class AttackTask : Task < AutonomousBehaviour >
{
    public enum State
    {
        Wait,
        WaitForAnimationEnd,
        Finish
    }

    protected FSMAttackState _behaviour;

    protected State _currentState;

    public AttackTask(FSMAttackState behaviour)
    {
        _behaviour = behaviour;    
        _currentState = State.Wait;
    }

    public ProjectileData GetProjectileData()
    {
        ProjectileData projectileData = null;

        if (_behaviour.customProjectileData)
        {
            projectileData = new ProjectileData(_behaviour.damage, _behaviour.element, 
                _behaviour.context._searchTargets, LayerManager.CharacterLayer, LayerManager.CharacterLayer);
        }
        else if (_behaviour.context.autonomousType == AutonomousBehaviour.AutonomousType.Enemy)
        {
            AutonomousBlueprint blueprint = _behaviour.context.autonomousKey.Value;
            projectileData = new ProjectileData(blueprint.BasePower, blueprint.Element, 
                _behaviour.context._searchTargets, LayerManager.CharacterLayer, 
                LayerManager.CharacterLayer);
        }
        else if (_behaviour.context.autonomousType == AutonomousBehaviour.AutonomousType.Projectile)
        {
            projectileData = new ProjectileData(_behaviour.context.projectileData);
        }

        return projectileData;
    }

    public Vector2 GetProjectileSpawnPoint()
    {
        Vector2 point = Vector2.zero;

        switch (_behaviour.spawnBehaviour)
        {
            case FSMAttackState.SpawnBehaviour.CurrentPoint:
                point = _behaviour.context.Coordinate;
                point += _behaviour.gridOffset;
                break;
        }

        return point;
    }


}
