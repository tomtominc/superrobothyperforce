﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using Framework.Core;

[System.Serializable]
public class FSMAttackState : State < AutonomousBehaviour >
{
   
    public State initialState;
    public AttackStyle attackStyle;
    public bool customProjectileData;
    public ProjectileKey projectileKey;
    public int damage;
    public Element element;
    public SpawnBehaviour spawnBehaviour;

    /// <summary>
    /// The grid offset for spawning the 
    /// projectile. 
    /// </summary>
    public Vector2 gridOffset;

    /// <summary>
    /// the pixel offset for spawning the
    /// projectile. This should be used to offset slightly 
    /// to e.g. get a grenade to be spawned in the 'hand' of an object.
    /// </summary>
    public Vector2 pixelOffset;

    public int attackCount;
    public float attackDelay;
    public int attackFrame;

    public StateValue nextState;

    private State _currentState;
    private int _currentAttackCount;
    protected float _currentTime;

    #region Overrides

    public override void StateStart()
    {
        if (context.animationBehaviour == AutonomousBehaviour.AnimationBehaviour.AlwaysStartStateInIdle)
            context.PlayAnimationByState(UnitEntity.AnimatorState.OnIdle);
        
        _currentAttackCount = 0; 

        ChangeTask(initialState);
    }

    public override void StateUpdate(float deltaTime)
    {

    }

    public override void StateEnd()
    {

    }

    #endregion

    #region Task Update

    public void ChangeTask(State state)
    {
        _currentState = state;

        switch (_currentState)
        {
            case State.Wait:
                DoWaitTask();
                break;
            case State.Attack:
                DoAttackTask();
                break;
        }
    }

    #endregion

    #region Do Tasks

    public void DoWaitTask()
    {
        WaitTask waitTask = new WaitTask(attackDelay);
        waitTask.compledHandler += (task) =>
        {
            ChangeTask(State.Attack);       
        };

        context._taskManger.AddTask(waitTask);
    }

    public void DoAttackTask()
    {
        switch (attackStyle)
        {
            case AttackStyle.Projectile:
                DoProjectileAttack();
                break;
        }
    }

    #endregion

    #region Attack Styles

    public void DoProjectileAttack()
    {
        ProjectileTask projectileTask = new ProjectileTask(this);

        projectileTask.compledHandler += (task) =>
        {
            _currentAttackCount++;

            if (_currentAttackCount >= attackCount)
            {
                context.ChangeState(nextState);
            }
            else
            {
                ChangeTask(State.Wait);
            }
        };

        context._taskManger.AddTask(projectileTask);

    }

    #endregion

    #region Enum

    public enum State
    {
        Wait,
        Attack
    }

    public enum AttackStyle
    {
        Projectile,
        Factory
    }

    public enum SpawnBehaviour
    {
        /// <summary>
        /// Spawns at the current point of the autonomous 
        /// behaviour. Uses a 'grid offset' and 'pixel offset'
        /// for correct spawn location.
        /// </summary>
        CurrentPoint,
    }

    #endregion

}
