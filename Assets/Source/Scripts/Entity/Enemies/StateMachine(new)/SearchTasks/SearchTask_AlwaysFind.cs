﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TaskManager;

public class SearchTask : Task<AutonomousBehaviour>
{
    public enum SearchState
    {
        Search,
        Finish
    }

    public SearchState currentSearchState;
    public bool isSuccessful;
}


public class SimpleSearch : SearchTask
{
    
    private int _findCount;

    public SimpleSearch(int findCount)
    {
        currentSearchState = SearchState.Search;
        _findCount = findCount; 
        isSuccessful = true;
    }

    public override void TaskStart()
    {
        context._searchTargets = new List < Vector2 >();
        context._searchTargets.Add(context.CharacterCoordinates);

        Vector2 lastPoint = new Vector2(-1f, -1f);
        Vector2 point = lastPoint;

        for (int i = 1; i < _findCount; i++)
        {
            while (point.Equals(lastPoint))
            {
                point = context.World.GetRandomWorldPoint(0, 9);
            }

            lastPoint = point;

            context._searchTargets.Add(point);
        }

        currentSearchState = SearchState.Finish;
    }

    public override void TaskUpdate()
    {
        if (currentSearchState == SearchState.Finish)
            Complete();
    }
}
