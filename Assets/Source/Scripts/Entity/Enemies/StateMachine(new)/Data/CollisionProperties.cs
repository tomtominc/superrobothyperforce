﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CollisionProperties
{
    public LayerMask CollisionLayer;
    public LayerMask TriggerLayer;
}
