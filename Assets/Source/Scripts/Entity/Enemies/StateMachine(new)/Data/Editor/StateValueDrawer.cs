﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(StateValue))]
public class StateValueDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        float enumWidth = 0.9f;
        float indexWidth = 1f - enumWidth;
        EditorGUI.PropertyField(new Rect(position.x + position.width * 0, position.y, position.width * enumWidth, position.height), property.FindPropertyRelative("type"), label);
        EditorGUI.PropertyField(new Rect(position.x + position.width * enumWidth, position.y, position.width * indexWidth, position.height), property.FindPropertyRelative("index"), GUIContent.none);

        EditorGUI.EndProperty();
    }
}
