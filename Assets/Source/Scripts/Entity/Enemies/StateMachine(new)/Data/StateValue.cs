﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class StateValue
{
    public StateType type;
    public int index = 0;

    public StateValue()
    {
    }

    public StateValue(StateType type, int index)
    {
        this.type = type;
        this.index = index;
    }

    public StateValue Clone()
    {
        return new StateValue(type, index);
    }
}

public enum StateType
{
    None,
    Wait,
    Search,
    Move,
    Attack,
    Finish
}

