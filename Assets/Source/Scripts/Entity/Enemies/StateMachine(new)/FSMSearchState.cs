﻿using UnityEngine;
using System.Collections;
using StateManager;
using Framework.Core;

public enum SearchStyle
{
    Disabled,
    Simple,
    Raycast,
}

public enum SearchDuration
{
    Once,
    Timed,
    UntilFound
}


[System.Serializable]
public class FSMSearchState : State<AutonomousBehaviour>
{
    public enum State
    {
        Wait,
        Search
    }

    public State initialState;
    public SearchStyle searchStyle;
    public SearchDuration searchDuration;
    public int numberOfTargets = 1;

    // wait state property
    public float waitDuration = 0f;
   
    public StateValue onSuccess;
    public StateValue onFailure;

    private State _currentState;

    #region Overrides

    public override void StateStart()
    {
        if (context.animationBehaviour == AutonomousBehaviour.AnimationBehaviour.AlwaysStartStateInIdle)
            context.PlayAnimationByState(UnitEntity.AnimatorState.OnIdle);

        ChangeTask(initialState);
    }

    public override void StateUpdate(float deltaTime)
    {
        
    }

    public override void StateEnd()
    {
        
    }

    #endregion

    #region Task Update

    public virtual void ChangeTask(State state)
    {
        _currentState = state;

        switch (_currentState)
        {
            case State.Wait: 
                DoWaitTask();
                break;
            case State.Search:
                DoSearchTask();
                break;
        }
    }

    #endregion

    #region Do Tasks

    public virtual void DoWaitTask()
    {
        WaitTask waitTask = new WaitTask(waitDuration);
        waitTask.compledHandler += (task) =>
        {
            ChangeTask(State.Search);
        };

        context._taskManger.AddTask(waitTask);
    }

    public virtual void DoSearchTask()
    {
        switch (searchStyle)
        {
            case SearchStyle.Simple:
                DoSimpleSearch();
                break;
        }
    }

    #endregion

    #region Search Styles

    public virtual void DoSimpleSearch()
    {
        SimpleSearch simpleSearch = new SimpleSearch(numberOfTargets);
        simpleSearch.compledHandler += (task) =>
        {
            if (simpleSearch.isSuccessful)
            {
                context.ChangeState(onSuccess);
            }
            else
            {
                context.ChangeState(onFailure);
            }
        };

        context._taskManger.AddTask(simpleSearch);
    }

    #endregion
}
    
