﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BlueprintData
{
    public string Name;

    public string Description;

    public Element Element;

    public Sprite IconSprite;
}
