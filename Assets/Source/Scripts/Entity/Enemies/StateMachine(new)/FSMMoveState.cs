﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateManager;
using Framework.Core;

[System.Serializable]
public class FSMMoveState : State<AutonomousBehaviour>
{
   
    public State initialState;
    public MovementStyle movementStyle;
    public MoveTweenType movementAnimation;
    public int moveCount;
    public float moveDuration;
    public float moveDelay;
    public StepStyle stepStyle;
    public StepLimit stepLimit;
    public GridLimit gridLimit;
    public Restrictions restrictions;

    // path movement style
    public PathType pathType;

    public StateValue onSuccess;
    public StateValue onFailure;

    private State _currentState;

    private int _currentMovementCount;

    #region Overrides

    public override void StateStart()
    {
        if (context.animationBehaviour == AutonomousBehaviour.AnimationBehaviour.AlwaysStartStateInIdle)
            context.PlayAnimationByState(UnitEntity.AnimatorState.OnIdle);
        
        _currentMovementCount = 0; 

        ChangeTask(initialState);
    }

    public override void StateUpdate(float deltaTime)
    {
        
    }

    public override void StateEnd()
    {
        
    }

    #endregion

    #region Task Update

    public void ChangeTask(State state)
    {
        _currentState = state;

        switch (_currentState)
        {
            case State.Wait:
                DoWaitTask();
                break;
            case State.Move:
                DoMoveTask();
                break;
        }
    }

    #endregion

    #region Do Tasks

    public void DoWaitTask()
    {
        WaitTask waitTask = new WaitTask(moveDelay);
        waitTask.compledHandler += (task) =>
        {
            ChangeTask(State.Move);       
        };

        context._taskManger.AddTask(waitTask);

    }

    public void DoMoveTask()
    {
        switch (movementStyle)
        {
            case MovementStyle.Flee:
                DoFleeMove();
                break;
            case MovementStyle.Path:
                DoPathMove();
                break;
        }
    }

    #endregion

    #region Move Styles

    public void DoFleeMove()
    {
        FleeMoveTask fleeMoveTask = new FleeMoveTask(this);

        fleeMoveTask.compledHandler += (task) =>
        {
            if (fleeMoveTask.isSuccessful)
            {
                _currentMovementCount++;
            }

            if (_currentMovementCount >= moveCount)
            {
                context.ChangeState(onSuccess);
            }
            else
            {
                ChangeTask(State.Wait);
            }
        };

        context._taskManger.AddTask(fleeMoveTask);
            
    }

    public void DoPathMove()
    {
        PathMoveTask pathMoveTask = new PathMoveTask(this);

        pathMoveTask.compledHandler += (task) =>
        {
            if (pathMoveTask.isSuccessful)
            {
                _currentMovementCount++;
            }

            if (_currentMovementCount >= moveCount)
            {
                context.ChangeState(onSuccess);
            }
            else
            {
                ChangeTask(State.Wait);
            }

        };

        context._taskManger.AddTask(pathMoveTask);
    }

    #endregion

    #region Enum

    public enum State
    {
        Wait,
        Move
    }

    public enum MovementStyle
    {
        Disabled,
        Flee,
        Follow,
        Path,
        Direction
    }

    public enum MovementAnimation
    {
        None,
        SimulateHeight
    }

    public enum StepStyle
    {
        Disabled,
        Step,
        Teleport,
        Continuous,
        Ariel
    }

    public enum PathType
    {
        Targets
    }

    public enum StepLimit
    {
        Unrestricted,
        Row,
        Column
    }

    public enum GridLimit
    {
        Unrestricted,
        PlayerSpace,
        EnemySpace
    }

    public enum Restrictions
    {
        Unrestricted,
        OccupiedSpaces
    }

    #endregion

}
