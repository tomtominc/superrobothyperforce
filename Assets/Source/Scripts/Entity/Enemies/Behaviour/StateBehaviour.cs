﻿using UnityEngine;

public class StateBehaviour
{
    protected Transform _transform;

    public StateBehaviour()
    {

    }

    public StateBehaviour(Transform transform)
    {
        _transform = transform;
    }

    public virtual void DoStart()
    {

    }

    public virtual void DoUpdate(float deltaTime)
    {

    }
}
