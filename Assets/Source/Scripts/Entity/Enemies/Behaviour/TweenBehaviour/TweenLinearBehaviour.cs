﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class TweenLinearBehaviour : TweenBehaviour
{
    public TweenLinearBehaviour(Transform transform, Vector3 target, float duration, Action <Vector3,Vector3> onUpdate, Action onComplete)
        : base(transform, target, duration, onUpdate, onComplete)
    {
    }

    public override void DoStart()
    {
        Vector3 lastPosition = _transform.position;

        _transform.DOMove(_target, _duration).SetEase(Ease.Linear).OnUpdate(() =>
            {
                if (_onUpdate != null)
                    _onUpdate.Invoke(_transform.position, lastPosition);

                lastPosition = _transform.position;

            }).OnComplete(() =>
            {
                if (_onComplete != null)
                    _onComplete.Invoke();
            });
    }


}
