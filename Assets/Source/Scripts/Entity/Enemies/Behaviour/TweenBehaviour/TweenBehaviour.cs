﻿using UnityEngine;
using System.Collections;
using System;

public class TweenBehaviour : StateBehaviour
{

    protected Action<Vector3,Vector3> _onUpdate;
    protected Action _onComplete;
    protected Vector3 _target;
    protected float _duration;

    public TweenBehaviour(Transform transform, Vector3 target, float duration, Action <Vector3,Vector3> onUpdate, Action onComplete)
        : base(transform)
    {
        _target = target;
        _duration = duration;
        _onUpdate = onUpdate;
        _onComplete = onComplete;

    }
}
