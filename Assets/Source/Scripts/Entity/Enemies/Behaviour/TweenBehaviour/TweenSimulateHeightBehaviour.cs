﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class TweenSimulateHeightBehaviour : TweenBehaviour
{
    public float firingAngle = 60f;
    public float gravity = 9.8f;

    public TweenSimulateHeightBehaviour(Transform transform, Vector3 target, float duration, Action <Vector3,Vector3> onUpdate, Action onComplete)
        : base(transform, target, duration, onUpdate, onComplete)
    {
    }

    public override void DoStart()
    {
        _transform.DOJump(_target, 1f, 1, _duration).SetEase(Ease.Linear).OnComplete 
        (
            () =>
            { 
                if (_onComplete != null)
                    _onComplete();
            }
        );
    }
}
