﻿using UnityEngine;
using System;

public class CollisionBehaviour : StateBehaviour
{
    protected AutonomousBehaviour _projectile;
    protected Action<CollisionInfo> _onCollide;

    public CollisionBehaviour(Transform transform, Action<CollisionInfo> onCollide)
        : base(transform)
    {
        _onCollide = onCollide;
        _projectile = _transform.GetComponent<AutonomousBehaviour>();
    }

    public override void DoUpdate(float deltaTime)
    {
        if (_projectile.limitCollisionsToFrames && !_projectile.collisionFrames.InBetween(_projectile.CurrentFrame))
            return;

        RaycastHit2D collisionInfo = Physics2D.Raycast(_transform.position, Vector2.zero, 0, _projectile.projectileData.collisionLayer);

        RaycastHit2D triggerInfo = Physics2D.Raycast(_transform.position, Vector2.zero, 0, _projectile.projectileData.triggerLayer);

        if (collisionInfo.transform && collisionInfo.transform != _projectile.transform
            && (_projectile.collisionUpdate == AutonomousBehaviour.CollisionUpdate.CollideAndTrigger ||
            _projectile.collisionUpdate == AutonomousBehaviour.CollisionUpdate.Collide))
        {
            bool counter = _projectile.isCounterable && _projectile.counterFrames.InBetween(_projectile.CurrentFrame);

            CollisionInfo info = new CollisionInfo(collisionInfo.transform.GetComponent<IEntity>(),
                                     _transform.GetComponent<IEntity>(), collisionInfo.transform.position,
                                     new DamageInfo(_projectile.projectileData.damage, _projectile.projectileData.element), 
                                     counter, false);

            if (_onCollide != null)
                _onCollide.Invoke(info);
        }
        else if (triggerInfo.transform && triggerInfo.transform != _projectile.transform
                 && (_projectile.collisionUpdate == AutonomousBehaviour.CollisionUpdate.CollideAndTrigger ||
                 _projectile.collisionUpdate == AutonomousBehaviour.CollisionUpdate.Trigger))
        {
            CollisionInfo info = new CollisionInfo(triggerInfo.transform.GetComponent<IEntity>(),
                                     _transform.GetComponent<IEntity>(), triggerInfo.transform.position, 
                                     new DamageInfo(_projectile.projectileData.damage, _projectile.projectileData.element), false, true);

            if (_onCollide != null)
                _onCollide.Invoke(info);
        }
    }

}
