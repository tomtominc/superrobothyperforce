﻿using UnityEngine;
using System.Collections;



public interface IDamagable
{
    bool Invincible { get; }

    void Damage(DamageInfo damageInfo);
}

[System.Serializable]
public class DamageInfo
{
    public int amount;
    public Element element;

    public DamageInfo()
    {
    }

    public DamageInfo(DamageInfo damageInfo)
    {
        amount = damageInfo.amount;
        element = damageInfo.element;
    }

    public DamageInfo(int amount, Element element)
    {
        this.amount = amount;
        this.element = element;
    }
}

[System.Serializable]
public class BulletInfo
{
    public enum PointType
    {
        TransformPoint,
        SearchPoint,
        CharacterPoint,
        RandomPoint
    }

    public BulletInfo()
    {

    }

    public BulletInfo(string key, float speed, DamageInfo damageInfo, Vector2 creationCoordinate, Vector2 targetCoordinate, 
                      Vector2 direction, float rayLength, LayerMask collision, LayerMask trigger)
    {
        //General Properties
        this.key = key;
        this.speed = speed;

        //Collision Properties
        this.damageInfo = new DamageInfo(damageInfo);
        this.creationCoordinate = creationCoordinate;
        this.targetCoordinate = targetCoordinate;
        this.direction = direction;
        this.rayLength = rayLength;

        //Spawn Properties
        this.collision = collision;
        this.trigger = trigger;
        this.flipX = false;
    }

    public BulletInfo(BulletInfo data)
    {
        //General Properties
        this.key = data.key;
        this.speed = data.speed;

        //Collision Properties
        this.damageInfo = new DamageInfo(data.damageInfo);
        this.direction = data.direction;
        this.rayLength = data.rayLength;
        this.collision = data.collision;
        this.trigger = data.trigger;

        //Spawn Properties
        this.creationCoordinate = data.creationCoordinate;
        this.targetCoordinate = data.targetCoordinate;
        this.flipX = data.flipX;
    }


    [Header("General Properties")]
    public string key;
    public float speed;

    [Header("Collision Properties")]
    public DamageInfo damageInfo;
    public Vector2 direction;
    public float rayLength;
    public LayerMask collision, trigger;

    public bool flipX = false;

    [HideInInspector]
    public Vector2 creationCoordinate, targetCoordinate;

    [HideInInspector]
    public PointType creationPoint, targetPoint;

}


