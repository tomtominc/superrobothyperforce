﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;
using System.Collections.Generic;
using DG.Tweening;
using System;
using Framework.Core.Animation;
using Framework.Core;

public enum Element
{
    Normal,
    Fire,
    Water,
    Electric,
    Grass
}



public interface ITrigger
{
    void Trigger();
}

public enum EntityEvent
{
    OnSpawned,
    OnChangedHealth,
    OnUpdateStatus,
    OnDeath
}

// entity 2d exists in the world as something that can move
// this is a gameobject that has some extra properties.
public class Entity2D : MonoBehaviour , IEntity , ITrigger
{
    protected IWorld _world;
    protected Vector2 _coordinate;
    protected HealthGUI _healthGUI;
    protected SpriteRenderer _renderer;
    protected SpriteAnimation _animator;

    protected bool _update = true;
    protected Material _material;
    protected Bounds _cameraBounds;

    public Transform Transform { get { return transform; } }

    public Vector2 Coordinate { get { return _coordinate; } }

    public IWorld World { get { return _world; } }

    public int CurrentFrame { get { return BodyAnimator.CurrentFrame; } }

    public virtual SpriteAnimation BodyAnimator { get { return _animator; } }

    private CharacterEntity _character;

    private Vector2 lastCharacterCoords;

    private CharacterEntity character
    {
        get
        { 
            if (_character == null)
                _character = GameObject.FindObjectOfType < CharacterEntity >();


            return _character; 
        }

    }

    public Vector2 CharacterCoordinates
    {
        get
        { 
            if (character == null)
            {
                return lastCharacterCoords;
            }
            else
            {
                lastCharacterCoords = character.Coordinate;
                return lastCharacterCoords;
            }
        }
    }


    public Vector2 LastSearchedPoint { get; set; }

    public virtual void Initialize(IWorld world, string key)
    {
        name = key;

        _world = world;

        _renderer = GetComponentInChildren<SpriteRenderer>(true);
        _animator = GetComponentInChildren<SpriteAnimation>(true);

        _cameraBounds = Camera.main.OrthographicBounds();
       
        if (_renderer)
            _material = _renderer.material;

        SetData(key);

        EventManager.Subscribe(BattleEvent.OnBattleStart, OnBattleStart);
        EventManager.Subscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Subscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Subscribe(BattleEvent.OnBattleTogglePause, OnTogglePause);

        EventManager.Publish(this, EntityEvent.OnSpawned, this);
    }

    protected virtual void OnTogglePause(IMessage message)
    {
        _update = !_update;
    }

    protected virtual void SetData(string key)
    {
        
    }

    public bool Move(Vector2 point, float duration, MoveTweenType moveTweenType, Action<bool> onfinished)
    {
        if (_world == null || !_world.GetBounds(1 << gameObject.layer).Contains(point))
        {
            if (onfinished != null)
                onfinished.Invoke(false);

            return false;
        }

        return _world.Move(this, point, duration, moveTweenType, onfinished);
    }

    public void SetPosition(Vector2 position)
    {
        transform.position = position;
       
        _coordinate = _world.GetCoordinates(position);

    }

    public void SetPositionByPoint(Vector2 point)
    {
        SetPosition(_world.GetWorldPoint(point));
    }

    public virtual void RemoveFromWorld()
    {
        EventManager.Unsubscribe(BattleEvent.OnBattleStart, OnBattleStart);
        EventManager.Unsubscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Unsubscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
        EventManager.Unsubscribe(BattleEvent.OnBattleTogglePause, OnTogglePause);

        if (transform.gameObject.activeSelf)
        {
            _world.RemoveFromWorld(transform);
        }
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(BattleEvent.OnBattleStart, OnBattleStart);
        EventManager.Unsubscribe(BattleEvent.OnFormationDestroyed, OnFormationDestroyed);
        EventManager.Unsubscribe(BattleEvent.OnCharacterDestroyed, OnCharacterDestroyed);
    }

    public virtual void Trigger()
    {
    }

    private void Update()
    {
        UpdateRenderer();

        if (_update)
        {
            CheckBounds();
            DoUpdate();
        }
    }

    protected virtual void CheckBounds()
    {
        if (!Camera.main.InBounds(transform.position))
        {
            OnIsInvisible();
        }
    }

    protected virtual void OnIsInvisible()
    {
        RemoveFromWorld();
    }

    protected virtual void UpdateRenderer()
    {
        if (_renderer)
            _renderer.sortingOrder = -((int)_coordinate.y);
    }

    protected virtual void DoUpdate()
    {
       
    }

    public Vector2 GetCoordinates(BulletInfo.PointType pointType)
    {
        Vector2 point = Vector2.zero;

        switch (pointType)
        {
            case BulletInfo.PointType.TransformPoint:
                point = Coordinate;
                break;
            case BulletInfo.PointType.CharacterPoint:
                point = CharacterCoordinates;
                break;
            case BulletInfo.PointType.SearchPoint:
                point = LastSearchedPoint;
                break;
            case BulletInfo.PointType.RandomPoint:
                point = World.GetBounds(LayerManager.CharacterLayer).GetRandomPoint();
                break;

        }

        return point;
    }

    protected virtual void OnBattleStart(IMessage message)
    {
        
    }

    protected virtual void OnFormationDestroyed(IMessage message)
    {
        
    }

    protected virtual void OnCharacterDestroyed(IMessage message)
    {
        
    }

}

// -- Super Robot Hyper Force
// combat exists on a 3x6 grid

// -- initialize a battle
// BattleManager will grab a random formation based on factors
// it will then call ONBATTLEINITIALIZE event
// BattleGrid listens to the event and creates entities based on the formation passed
// BattleGrid handles all visualization including
// - background
// - grid cells
// - grid entities
// - other forms of visualization
// loading screen will turn on if the enviornment is changed, otherwise all entities are just spawned into the grid

// -- things that happen when ONBATTLEINITIALIZE event is called
// 1. overlay showing battle # will be displayed across the screen (middle banner)
// 2. all enemies will be spawned (depending on formation)
// 3. characters controls are turned off
// 4. ONBATTLESTART is called

// -- things that happen when ONBATTLESTART event is called
// 1. AIManager will begin allowing autonomous entities run their behaviour
// 2. Players controls will be turned on

// -- things that happen when ONPLAYERDIED event is called
// 1. AIManager will stop allowing autonomous entities to run their behaviour
// 2. TimeManager will pause everything!
// 3. overlay asking if you'd like to continue, either with gems or ads (if available)
// 	a. says "no"
//  	I. go back to the screen and have the player run a death animation.
//		II. overlay pops up asking to retry or go back to main menu
// 	b. says "yes"
// 		I. run ad or collect gems
//		II. BattleManager re-inits the BattleGrid with another battle keeping the same score as before.

// -- How does the character work?
// character is made up of several "parts" these parts will help define it's stats and visual
// when spawned in the character blueprint will have indexs to each part so [arm = 2, legs = 4, head = 1, cannon = 5]
// those numbers will then coorespond to some stat object which will then grab a real object to place on it.

// -- things that happen when ONBATTLEFINISH event is called
// 1. ONBATTLEINITIALIZE is called

