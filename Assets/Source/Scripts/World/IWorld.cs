﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum MoveTweenType
{
    Linear,
    SimulateHeight
}

public interface IEntity
{
    IWorld World { get; }

    Transform Transform { get; }

    void SetPosition(Vector2 position);

    void Initialize(IWorld world, string key);

    bool Move(Vector2 point, float duration, MoveTweenType moveTweenType, Action<bool> onfinished);

    Vector2 Coordinate { get; }

}

public interface IWorld
{
    Rect MaxGrid { get; }

    bool IsInWorldBounds(Vector2 point);

    IEntity AddToWorld(string name);

    void RemoveFromWorld(Transform entity);

    bool Move(IEntity entity, Vector2 point, float duration, MoveTweenType moveTweenType, Action<bool> onfinished = null);

    Vector2 GetCoordinates(Vector2 point);

    bool IsMoveValid(IEntity entity, Vector2 point);

    Vector2 GetWorldPoint(Vector2 point);

    Rect GetBounds(LayerMask layer);

    IEntity SpawnEntity(EntityData data);

    void SetOccupiedCell(IEntity entity, Vector2 targetPoint, Vector2 lastPoint);

    void SetTargetCell(Vector2 targetPoint, bool isTargeted);

    void SetDamagableCell(Vector2 targetPoint, bool isDamagable);

    Vector2 GetRandomWorldPoint(int min, int max);
}
