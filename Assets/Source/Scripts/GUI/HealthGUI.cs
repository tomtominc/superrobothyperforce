﻿using UnityEngine;
using System.Collections;

public class HealthGUI : MonoBehaviour
{
    private UISpriteText healthLabel;

    public void Initialize(int health)
    {
        healthLabel = GetComponentInChildren < UISpriteText >(true);
        SetHealth(health);     
    }

    public void SetHealth(int health)
    {
        healthLabel.Text = health.ToString(); 
    }
}
