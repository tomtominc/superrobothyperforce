﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RutCreate.LightningDatabase;
using UnityEngine.UI;
using System;

public enum PartType
{
    Body,
    Buster,
    Card
}

public class BlueprintDisplay : MonoBehaviour
{
    [SerializeField] private Transform _baseItem;

    private ToggleGroup _toggleGroup;
    private PartType _currentPartType;

    public event EventHandler < ActionEventArgs > ButtonClicked;

    private ToggleGroup ToggleGroup
    {
        get
        {
            if (_toggleGroup == null)
                _toggleGroup = GetComponent < ToggleGroup >();
            
            return _toggleGroup; 
        }
    }

    public void SetDisplay(PartType partType)
    {
        ResetScrollLayout();

        _currentPartType = partType;

        switch (_currentPartType)
        {
            case PartType.Buster:
                SetupBusterScrollLayout();
                break;
        }
    }

    private void ResetScrollLayout()
    {
        transform.DestroyChildren();
    }

    private void SetupBusterScrollLayout()
    {
//        List < BusterBlueprint > busters = DatabaseManager.GetAllBusters();
//
//        foreach (var buster in busters)
//        {
//            Transform item = Instantiate < Transform >(_baseItem);
//            item.SetParent(transform, false);
//
//            UIBaseItem itemComponent = item.GetComponent < UIBaseItem >();
//
//            itemComponent.Initialize(ToggleGroup);
//            itemComponent.SetItem(buster.Name, buster.Description, buster.Icon, DatabaseManager.GetElementSprite(buster.Element), OnClickedAction);
//        }
    }

    private void OnClickedAction(ActionEventArgs e)
    {
        if (ButtonClicked != null)
            ButtonClicked.Invoke(this, e);
    }
}
