﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class Countdown : MonoBehaviour
{
    [SerializeField]private List < Sprite > _countDownSprites;

    private Image _image;
    private int _current;
    private int _max;
    private Sequence _countDownSequence;

    private void OnEnable()
    {
        _image = GetComponent < Image >();
        _current = 0;
        _max = _countDownSprites.Count;
        _image.sprite = _countDownSprites[_current];

        DoCountdown();
    }

    private void OnDisable()
    {
        _countDownSequence.Complete();
    }

    public void DoCountdown()
    {
        _countDownSequence = DOTween.Sequence();
        _countDownSequence.SetUpdate(true);

        while (_current < _max)
        {
            if (_current >= _max)
                break;

            int current = _current;

            _countDownSequence.AppendInterval(0.75f);

            _countDownSequence.AppendCallback(() =>
                { 
                    _image.sprite = _countDownSprites[current]; 
                });
            _current++;
        }

        _countDownSequence.Play(); 
    }
}
