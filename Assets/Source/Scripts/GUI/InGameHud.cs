﻿using UnityEngine;
using System.Collections;
using Framework.Core.EventSystem;

public class InGameHud : MonoBehaviour
{
    public UISpriteText scoreUIText;

    private void OnEnable()
    {
        EventManager.Subscribe(BattleEvent.OnBattleFinish, OnBattleFinish);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe(BattleEvent.OnBattleFinish, OnBattleFinish);
    }

    private void OnBattleFinish(IMessage message)
    {
        BattleData data = (BattleData)message.Data;

        if (data != null)
        {
            scoreUIText.Text = data.score.ToString();
        }
    }
}
