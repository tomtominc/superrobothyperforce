﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class UIBaseItem : MonoBehaviour
{

    [SerializeField] private RectTransform _buttonGroup;
    [SerializeField] private List < Button > _buttons;

    [SerializeField] private float _offHeight = 96f;
    [SerializeField] private float _onHeight = 144f;

    [SerializeField] private float _tweenDuration = 0.5f;
    [SerializeField] private float _buttonPunchDuration = 0.5f;

    protected string _id;
    protected ToggleGroup _toggleGroup;
    protected Toggle _toggle;
    protected LayoutElement _layoutElement;
    protected Action < ActionEventArgs > _onClickedAction;

    protected event EventHandler < ActionEventArgs > ButtonClicked;

    public void Initialize(string id, ToggleGroup toggleGroup)
    {
        _id = id;
        _toggleGroup = toggleGroup;
        _toggle = GetComponent < Toggle >();
        _layoutElement = GetComponent < LayoutElement >();

        _toggle.onValueChanged.AddListener(OnValueChanged);

        _toggle.group = _toggleGroup;
            
        _toggleGroup.RegisterToggle(_toggle);

        foreach (Button button in _buttons)
        {
            button.onClick.AddListener(OnClicked);
        }
    }

    public virtual void SetItem(Dictionary < string , object > values, Action < ActionEventArgs > onClicked)
    {
        _onClickedAction = onClicked;
    }

    public virtual void OnValueChanged(bool isOn)
    {
        OnClicked();

        if (_buttonGroup == null)
            return;
        
        float height = isOn ? _onHeight : _offHeight;

        _buttonGroup.gameObject.SetActive(isOn);

        _layoutElement.DOPreferredSize(new Vector2(0f, height), _tweenDuration);

        if (isOn)
        {
            _buttonGroup.DOPunchScale(new Vector3(0.1f, 0.1f), _buttonPunchDuration);
        }
    }

    private void OnClicked()
    {
        GameObject go = EventSystem.current.currentSelectedGameObject;

        if (_onClickedAction != null)
            _onClickedAction.Invoke(new ActionEventArgs(_id, go.name, go));
        
        if (ButtonClicked != null)
            ButtonClicked.Invoke(this, new ActionEventArgs(_id, go.name, go));
    }
}

public class ActionEventArgs : EventArgs
{
    public string Property { get; private set; }

    public string Action { get; private set; }

    public GameObject ClickedObject { get; private set; }

    public ActionEventArgs(string Property, string Action, GameObject ClickedObject)
    {
        this.Property = Property;
        this.Action = Action;
        this.ClickedObject = ClickedObject;
    }

    public ActionEventArgs(string ActionPath)
    {
        string[] split = ActionPath.Split('/');

        this.Property = split[0];
        this.Action = split[1];
    }
}
